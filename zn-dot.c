/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

struct zn
DN_M(zn_dotu)(const int N,
	      const struct zn * restrict X, const int incX,
	      const struct zn * restrict Y, const int incY)
{
  if (incX == 1 && incY == 1)
    return zn_sumprod_(N,X,Y);
  else
    return zn_sumprod_strided_(N,X,incX,Y,incY);
}

struct zn
DN_M(zn_dotc)(const int N,
	      const struct zn * restrict X, const int incX,
	      const struct zn * restrict Y, const int incY)
{
  if (incX == 1 && incY == 1)
    return zn_sumprod_nc_(N,X,Y);
  else
    return zn_sumprod_strided_nc_(N,X,incX,Y,incY);
}

struct zn
DN_M(zn_dotu_f)(const int *N,
		const struct zn *X, const int *incX,
		const struct zn *Y, const int *incY)
{
  return DN_M(zn_dotu)(*N, X, *incX, Y, *incY);
}

struct zn
DN_M(zn_dotc_f)(const int *N,
		const struct zn *X, const int *incX,
		const struct zn *Y, const int *incY)
{
  return DN_M(zn_dotc)(*N, X, *incX, Y, *incY);
}
