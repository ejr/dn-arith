/* -*- C -*- Copyright (c) 2008 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(DN_OPS_INTERNAL_H_)
#define DN_OPS_INTERNAL_H_

#if defined(__cplusplus)
extern "C" {
#endif

struct dn dn_running_sum_wrapped_ (const struct dn x, const struct dn y)
  DN_ATTRS_CONST_;

#if defined(__cplusplus)
} // extern "C"
#endif

#endif // DN_OPS_INTERNAL_H_
