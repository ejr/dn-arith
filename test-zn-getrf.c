/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"
#include "zn-lapack.h"

#include "dn-helpers.h"

static int nerrs = 0;

static struct dn epserr;

static void
test_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct zn *A;
  int i, j, info;
  int *ipiv;

  A = calloc(ldA*N,sizeof(*A));
  ipiv = calloc(N, sizeof(*ipiv));

  for (j = 0; j < N; ++j)
    AREF(A,j,j) = zn_one();

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf ident utterly failed! info %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j)
    if (ipiv[j] != j) {
      printf("getrf ident ipiv wrong: %d should be %d\n",
	     ipiv[j], j);
      ++nerrs;
    }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("getrf ident diag/lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("getrf ident upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(ipiv);
  free(A);
}

static void
test_upsidedown_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct zn *A;
  int i, j, info;
  int *ipiv;

  A = calloc(ldA*N,sizeof(*A));
  ipiv = calloc(N, sizeof(*ipiv));

  for (j = 0; j < N; ++j)
    AREF(A,N-j-1,j) = zn_one();

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf upsidedown ident utterly failed! info %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j) {
    if (j < N/2) {
      if (ipiv[j] != N-j-1) {
	printf("getrf upsidedown ident ipiv wrong: %d should be %d\n",
	       ipiv[j], N-j-1);
	++nerrs;
      }
    }
    else {
      if (ipiv[j] != j) {
	printf("getrf upsidedown ident ipiv wrong: %d should be %d\n",
	       ipiv[j], j);
	++nerrs;
      }
    }
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("getrf upsidedown ident diag/lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("getrf upsidedown ident upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(ipiv);
  free(A);
}

static void
test_with_gemm (void)
{
  const int N = 17, M = N, ldA = N, ldAsav = N, ldB = N, ldC = N;
  struct zn *A, *Asav, *B, *C;
  int i, j, info, *ipiv, *permed;

  A = malloc(ldA*N*sizeof(*A));
  B = calloc(ldB*N, sizeof(*B));
  C = malloc(ldC*N*sizeof(*C));
  Asav = malloc(ldAsav*N*sizeof(*Asav));
  ipiv = malloc(N*sizeof(*ipiv));
  permed = malloc(N*sizeof(*permed));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      AREF(Asav,i,j) = AREF(A,i,j) = zn_make(dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()),
					     dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()));
    }

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf failed: %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j) {
    for (i = 0; i <= j; ++i) {
      AREF(B,i,j) = AREF(A,i,j);
      AREF(A,i,j) = zn_zero();
    }
    AREF(A,j,j) = zn_one();
  }

  /* compute C = L*U */
  zn_gemm("No", "No", N, N, N, zn_one(), A, ldA, B, ldB,
	  zn_zero(), C, ldC);

  for (i = 0; i < M; ++i)
    permed[i] = i;
  for (i = 0; i < N; ++i) {
    const int tmp = permed[i];
    permed[i] = permed[ipiv[i]];
    permed[ipiv[i]] = tmp;
  }
  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = AREF(Asav,permed[i],j);

  /* compute P*A in Asav */
  zn_laswp(N, Asav, ldAsav, 0, N-1, ipiv, 1);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      const struct zn tmp = zn_sub(AREF(Asav,i,j), AREF(C,i,j));
      const struct zn thr = zn_mul_dn(AREF(Asav,i,j), epserr);
      if (dn_gt(zn_abs(tmp), zn_abs(thr))) {
	  printf("getrf reform failed (%d %d) %g > %g\n", i, j,
		 (double)dn_to_n(zn_abs(tmp)), (double)dn_to_n(zn_abs(thr)));
	  ++nerrs;
      }
    }

  free(Asav);
  free(B);
  free(A);
}

static void
test_tallskinny_with_gemm (void)
{
  const int M = 31, N = 17, ldA = M, ldAsav = M, ldB = N, ldC = M;
  struct zn *A, *Asav, *B, *C;
  int i, j, info, *ipiv, *permed;

  A = malloc(ldA*N*sizeof(*A));
  B = calloc(ldB*N, sizeof(*B));
  C = calloc(ldC*N, sizeof(*C));
  Asav = malloc(ldAsav*N*sizeof(*Asav));
  ipiv = malloc(N*sizeof(*ipiv));
  permed = malloc(M*sizeof(*permed));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      AREF(Asav,i,j) = AREF(A,i,j) = zn_make(dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()),
					     dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()));
    }

  zn_getrf(M, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf failed: %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j) {
    for (i = 0; i <= j; ++i) {
      AREF(B,i,j) = AREF(A,i,j);
      AREF(A,i,j) = zn_zero();
    }
    AREF(A,j,j) = zn_one();
  }

  /* compute C = L*U */
  zn_gemm("No", "No", M, N, N, zn_one(), A, ldA, B, ldB,
	  zn_zero(), C, ldC);

  for (i = 0; i < M; ++i)
    permed[i] = i;
  for (i = 0; i < N; ++i) {
    const int tmp = permed[i];
    permed[i] = permed[ipiv[i]];
    permed[ipiv[i]] = tmp;
  }
  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = AREF(Asav,permed[i],j);

  /* compute P*A in Asav */
  zn_laswp(N, Asav, ldAsav, 0, N-1, ipiv, 1);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      const struct zn tmp = zn_sub(AREF(Asav,i,j), AREF(C,i,j));
      const struct zn thr = zn_mul_dn(AREF(Asav,i,j), epserr);
      if (dn_gt(zn_abs(tmp), zn_abs(thr))) {
	  printf("getrf reform failed (%d %d) %g > %g\n", i, j,
		 (double)dn_to_n(zn_abs(tmp)), (double)dn_to_n(zn_abs(thr)));
	  ++nerrs;
      }
    }

  free(Asav);
  free(B);
  free(A);
}

static void
test_shortfat_with_gemm (void)
{
  const int M = 11, N = 29, ldA = M, ldAsav = M, ldB = M, ldC = M;
  struct zn *A, *Asav, *B, *C;
  int i, j, info, *ipiv, *permed;

  A = malloc(ldA*N*sizeof(*A));
  B = calloc(ldB*N, sizeof(*B));
  C = calloc(ldC*N, sizeof(*C));
  Asav = malloc(ldAsav*N*sizeof(*Asav));
  ipiv = malloc(M*sizeof(*ipiv));
  permed = malloc(M*sizeof(*permed));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      AREF(Asav,i,j) = AREF(A,i,j) = zn_make(dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()),
					     dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()));
    }

  zn_getrf(M, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf failed: %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j) {
    for (i = 0; i <= j && i < M; ++i) {
      AREF(B,i,j) = AREF(A,i,j);
      AREF(A,i,j) = zn_zero();
    }
    if (j < M)
      AREF(A,j,j) = zn_one();
  }

  /* compute C = L*U */
  zn_gemm("No", "No", M, N, M, zn_one(), A, ldA, B, ldB,
	  zn_zero(), C, ldC);

  /* compute P*A in A by forming the permutation. */
  for (i = 0; i < M; ++i)
    permed[i] = i;
  for (i = 0; i < M; ++i) {
    const int tmp = permed[i];
    permed[i] = permed[ipiv[i]];
    permed[ipiv[i]] = tmp;
  }
  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = AREF(Asav,permed[i],j);

  /* compute P*A in Asav by swapping */
  zn_laswp(N, Asav, ldAsav, 0, M-1, ipiv, 1);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      const struct zn tmp = zn_sub(AREF(Asav,i,j), AREF(C,i,j));
      const struct zn thr = zn_mul_dn(AREF(Asav,i,j), epserr);
      if (dn_gt(zn_abs(tmp), zn_abs(thr))) {
	  printf("getrf reform failed (%d %d) %g > %g\n", i, j,
		 (double)dn_to_n(zn_abs(tmp)), (double)dn_to_n(zn_abs(thr)));
	  ++nerrs;
      }
    }

  free(Asav);
  free(B);
  free(A);
}

static void
test_with_trs (void)
{
  const int N = 15, M = N, ldA = N, ldAsav = N, ldB = N, ldC = N;
  struct zn *A, *Asav;
  int i, j, info, *ipiv, *permed;

  A = malloc(ldA*N*sizeof(*A));
  Asav = malloc(ldAsav*N*sizeof(*Asav));
  ipiv = malloc(N*sizeof(*ipiv));
  permed = malloc(N*sizeof(*permed));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      AREF(Asav,i,j) = AREF(A,i,j) = zn_make(dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()),
					     dn_make(1.0-2.0*drand48(),
						     DBL_EPSILON*drand48()));
    }

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getrf failed: %d\n", info);
    ++nerrs;
    return;
  }

  zn_getrs("No trans", N, N, A, ldA, ipiv, Asav, ldAsav, &info);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      if (i == j) {
	const struct zn tmp = zn_sub(AREF(Asav,i,j), zn_one());
	if (dn_gt(zn_abs(tmp), epserr)) {
	  printf("getrs/getrf failed on diag %d, %g\n",
		 i, (double)dn_to_n(zn_abs(tmp)));
	  ++nerrs;
	}
      }
      else {
	const struct zn tmp = AREF(Asav,i,j);
	if (dn_gt(zn_abs(tmp), epserr)) {
	  printf("getrs/getrf failed on off-diag (%d,%d), %g\n",
		 i, j, (double)dn_to_n(zn_abs(tmp)));
	  ++nerrs;
	}
      }
    }

  free(permed);
  free(ipiv);
  free(Asav);
  free(A);
}

int
main (void)
{
  epserr = dn_mul_n_n(1000*DBL_EPSILON, DBL_EPSILON);

  test_ident();
  test_upsidedown_ident();
  test_with_gemm();
  test_tallskinny_with_gemm();
  test_shortfat_with_gemm();
  test_with_trs();
  return nerrs;
}
