/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <string.h>

#include <assert.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

#define dn_laswp DN_M(dn_laswp)
void dn_laswp (int, struct dn * restrict, int, int, int,
	       const int * restrict, int);

void
DN_M(dn_getrs) (const char *trans, const int n, const int nrhs,
		const struct dn * restrict LU, const int ldLU,
		const int * restrict ipiv,
		struct dn * restrict B, const int ldB,
		int *info)
{
    *info = 0; /* XXX: should check args. */
    if (tolower(*trans) == 'n') {
	dn_laswp(nrhs, B, ldB, 0, n-1, ipiv, 1);
	dn_trsm("Left", "Lower", "No trans", "Unit", n, nrhs,
		dn_one(), LU, ldLU, B, ldB);
	dn_trsm("Left", "Upper", "No trans", "Non-unit", n, nrhs,
		dn_one(), LU, ldLU, B, ldB);
    }
    else {
	dn_trsm("Left", "Upper", "Transpose", "Non-unit", n, nrhs,
		dn_one(), LU, ldLU, B, ldB);
	dn_trsm("Left", "Lower", "Transpose", "Unit", n, nrhs,
		dn_one(), LU, ldLU, B, ldB);
	dn_laswp(nrhs, B, ldB, 0, n-1, ipiv, -1);
    }
}

void
DN_M(dn_getrs_f) (const char *trans, const int *n, const int *nrhs,
		  const struct dn * restrict LU, const int *ldLU,
		  const int * restrict ipiv,
		  struct dn * restrict B, const int *ldB,
		  int *info)
{
    dn_getrs(trans, *n, *nrhs, LU, *ldLU, ipiv, B, *ldB, info);
}
