/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

void
DN_M(dn_laswp) (int N, struct dn * restrict A, int ldA, int k1, int k2,
		const int * restrict ipiv, int incx)
{
    /* Interchange row I with row IPIV(I) for each of rows K1 through K2. */
    int i1, i2, ix0, inc;
    if (incx > 0) {
	ix0 = k1;
	i1 = k1;
	i2 = k2;
	inc = 1;
    }
    else if (incx < 0) {
	ix0 = (1-k2)*incx;
	i1 = k2;
	i2 = k1;
	inc = -1;
    }
    else
	return;
    int j;
    for (j = 0; j < N; ++j) {
	int i;
	int ix = ix0;
	for (i = i1; i <= i2; i += inc) {
	    const int ip = ipiv[ix];
	    if (ip != i) {
		const struct dn tmp = AREF(A,i,j);
		AREF(A,i,j) = AREF(A,ip,j);
		AREF(A,ip,j) = tmp;
	    }
	    ix += incx;
	}
    }
}

void
DN_M(dn_laswp_f) (int *N, struct dn * restrict A, int *ldA, int *k1, int *k2,
		  int * restrict ipiv, int *incx)
{
    int i1, i2, ix, inc;
    if (*incx > 0) {
	ix = *k1;
	i1 = *k1;
	i2 = *k2;
    }
    else if (*incx < 0) {
	ix = (1-*k2) * *incx;
	i1 = *k2;
	i2 = *k1;
    }
    else
	return;
    int i;
    for (i = i1; i <= i2; i += inc, ix += *incx ) --ipiv[ix];
    dn_laswp(*N, A, *ldA, *k1, *k2, ipiv, *incx);
    for (i = i1; i <= i2; i += inc, ix += *incx ) ++ipiv[ix];
}
