/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

void
DN_M(zn_gemv)(const char *trans, const int M, const int N,
	      const struct zn alpha,
	      const struct zn * restrict A, const int ldA,
	      const struct zn * restrict X, const int incX,
	      const struct zn beta,
	      struct zn * restrict Y, const int incY)
{
  const _Bool do_trans = tolower(*trans) != 'n';
  const _Bool do_conj = tolower(*trans) == 'c';
  const int lenY = (do_trans? M : N);

#pragma omp parallel
  {
    if (zn_iszero(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = zn_zero();
    else if (zn_isnegone(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = zn_negate(VREF(Y,j));
    else if (!zn_isone(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = zn_mul(beta, VREF(Y,j));

    if (!do_trans) {
#pragma omp for nowait
      for (int i = 0; i < M; ++i) {
	struct zn tmp = zn_sumprod_strided_(N, &AREF(A,i,0), ldA, X, incX);
	VREF(Y,i) = zn_macc(alpha, tmp, VREF(Y,i));
      }
    }
    else if (do_conj) {
#pragma omp for nowait
      for (int j = 0; j < N; ++j) {
	struct zn tmp;
	if (incX == 1)
	  tmp = zn_sumprod_nc_(M, X, &AREF(A,0,j));
	else
	  tmp = zn_sumprod_strided_nc_(M, X, incX, &AREF(A,0,j), 1);
	VREF(Y,j) = zn_macc(alpha, tmp, VREF(Y,j));
      }
    }
    else {
#pragma omp for nowait
      for (int j = 0; j < N; ++j) {
	struct zn tmp;
	if (incX == 1)
	  tmp = zn_sumprod_(M, &AREF(A,0,j), X);
	else
	  tmp = zn_sumprod_strided_(M, &AREF(A,0,j), 1, X, incX);
	VREF(Y,j) = zn_macc(alpha, tmp, VREF(Y,j));
      }
    }
  }
}

void
DN_M(zn_gemv_f)(const char *trans, const int *M, const int *N,
		const struct zn *alpha,
		const struct zn *A, const int *ldA,
		const struct zn *X, const int *incX,
		const struct zn *beta,
		struct zn *Y, const int *incY)
{
  DN_M(zn_gemv)(trans, *M, *N, *alpha, A, *ldA, X, *incX, *beta, Y, *incY);
}
