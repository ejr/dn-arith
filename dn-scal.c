/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

void
DN_M(dn_scal)(const int N,
	      const struct dn alpha,
	      struct dn * restrict X, const int incX)
{
  int i;

#pragma omp parallel for
  for (i = 0; i < N; ++i)
    VREF(X,i) = dn_mul(alpha, VREF(X,i));
}

void
DN_M(dn_scal_f)(const int *N,
		const struct dn *alpha,
		struct dn *X, const int *incX)
{
    DN_M(dn_scal)(*N, *alpha, X, *incX);
}
