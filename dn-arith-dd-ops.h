/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/

#define dn_head_ DN_M(dn_head_)
DN_INLINE dn_native_t dn_head_(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_head_(const struct dn a)
{
  return a.x[0];
}

#define dn_tail_ DN_M(dn_tail_)
DN_INLINE dn_native_t dn_tail_(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_tail_(const struct dn a)
{
  return a.x[1];
}

#define dn_make_ DN_M(dn_make_)
DN_INLINE struct dn dn_make_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_make_(const dn_native_t a, const dn_native_t b)
{
#if !defined(__cplusplus)
  return  ((struct dn){ .x = { a, b } });
#else
  struct dn outval;
  outval.x[0] = a;
  outval.x[1] = b;
  return outval;
#endif
}

#define dn_make_n DN_M(dn_make_n)
DN_INLINE struct dn dn_make_n(const dn_native_t a)
  DN_ATTRS_CONST_;
struct dn
dn_make_n(const dn_native_t a)
{
  return dn_make_(a, 0);
}

#define dn_to_n DN_M(dn_to_n)
DN_INLINE dn_native_t dn_to_n(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_to_n(const struct dn a)
{
  return (dn_native_t)a.x[0];
}

#define dn_to_dd DN_M(dn_to_dd)
DN_INLINE void dn_to_dd(const struct dn a, double*, double*);
void
dn_to_dd(const struct dn a, double *d1, double *d2)
{
#if defined(DN_NATIVE_IS_NOT_DOUBLE)
  *d1 = dn_head_(a);
  *d2 = dn_head_(a) - *d1;
  *d2 += dn_tail_(a);
#else
  *d1 = dn_head_(a);
  *d2 = dn_tail_(a);
#endif
}

#define dn_zero DN_M(dn_zero)
DN_INLINE struct dn dn_zero(void) DN_ATTRS_CONST_;
struct dn
dn_zero(void)
{
  return dn_make_n(0.0);
}

#define dn_one DN_M(dn_one)
DN_INLINE struct dn dn_one(void) DN_ATTRS_CONST_;
struct dn
dn_one(void)
{
  return dn_make_n(1.0);
}

#define dn_negone DN_M(dn_negone)
DN_INLINE struct dn dn_negone(void) DN_ATTRS_CONST_;
struct dn
dn_negone(void)
{
  return dn_make_n(-1.0);
}

#define dn_eps DN_M(dn_eps)
DN_INLINE struct dn dn_eps(void) DN_ATTRS_CONST_;
struct dn
dn_eps(void)
{
  return dn_make_n(DN_N_EPS_*DN_N_EPS_);
}

#define dn_nan DN_M(dn_nan)
DN_INLINE struct dn dn_nan(void) DN_ATTRS_CONST_;
struct dn
dn_nan(void)
{
#if !defined(__cplusplus)
  return dn_make_n(NAN);
#else
  return dn_make_n(std::numeric_limits<dn_native_t>::quiet_NaN());
#endif
}

#define dn_isnan DN_M(dn_isnan)
DN_INLINE DN_BOOL dn_isnan(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isnan(const struct dn a)
{
  return DN_N_ISNAN_(dn_head_(a));
}

#define dn_inf DN_M(dn_inf)
DN_INLINE struct dn dn_inf(void) DN_ATTRS_CONST_;
struct dn
dn_inf(void)
{
#if !defined(__cplusplus)
  return dn_make_n(INFINITY);
#else
  return dn_make_n(std::numeric_limits<dn_native_t>::infinity());
#endif
}

#define dn_isinf DN_M(dn_isinf)
DN_INLINE DN_BOOL dn_isinf(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isinf(const struct dn a)
{
  return DN_N_ISINF_(dn_head_(a));
}

#define dn_isfinite DN_M(dn_isfinite)
DN_INLINE DN_BOOL dn_isfinite(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isfinite(const struct dn a)
{
#if !defined(__cplusplus)
  return DN_N_ISFINITE_(dn_head_(a)) && DN_N_ISFINITE_(dn_tail_(a));
#else
  return !dn_isnan(a) && !dn_isinf(a);
#endif
}

#define dn_negate DN_M(dn_negate)
DN_INLINE struct dn dn_negate(const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_negate(const struct dn a)
{
  return dn_make_(-dn_head_(a), -dn_tail_(a));
}

/* Internal. */

/* Computes fl(a+b) and err(a+b).  Assumes |a| >= |b|. */
#define dn_quick_two_sum_ DN_M(dn_quick_two_sum_)
DN_INLINE struct dn dn_quick_two_sum_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_quick_two_sum_(const dn_native_t a, const dn_native_t b)
{
  const dn_native_t s = a + b;
  const dn_native_t err = b - (s - a);
  return dn_make_(s, err);
}

/* Computes fl(a-b) and err(a-b).  Assumes |a| >= |b| */
#define dn_quick_two_diff_ DN_M(dn_quick_two_diff_)
DN_INLINE struct dn dn_quick_two_diff_(const dn_native_t a,
				       const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_quick_two_diff_(const dn_native_t a, const dn_native_t b)
{
  const dn_native_t s = a - b;
  const dn_native_t err = (a - s) - b;
  return dn_make_(s, err);
}

/* Computes fl(a+b) and err(a+b).  */
#define dn_two_sum_ DN_M(dn_two_sum_)
DN_INLINE struct dn dn_two_sum_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_two_sum_(const dn_native_t a, const dn_native_t b)
{
  const dn_native_t s = a + b;
  const dn_native_t bb = s - a;
  const dn_native_t err = (a - (s - bb)) + (b - bb);
  return dn_make_(s, err);
}

/* Computes fl(a-b) and err(a-b).  */
#define dn_two_diff_ DN_M(dn_two_diff_)
DN_INLINE struct dn dn_two_diff_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_two_diff_(const dn_native_t a, const dn_native_t b)
{
  const dn_native_t s = a - b;
  const dn_native_t bb = s - a;
  const dn_native_t err = (a - (s - bb)) - (b + bb);
  return dn_make_(s, err);
}

#ifndef DN_N_FMA_

/* Computes high word and lo word of a */
#define dn_split_ DN_M(dn_split_)
DN_INLINE struct dn dn_split_(dn_native_t a) DN_ATTRS_CONST_;
struct dn
dn_split_(dn_native_t a)
{
  dn_native_t hi, lo;
  dn_native_t temp;
  if (DN_UNLIKELY_(a > DN_SPLIT_THRESH_ || a < -DN_SPLIT_THRESH_)) {
    a *= DN_SPLIT_INV_SCALE_;
    temp = DN_SPLITTER_ * a;
    hi = temp - (temp - a);
    lo = a - hi;
    hi *= DN_SPLIT_SCALE_;
    lo *= DN_SPLIT_SCALE_;
  } else {
    temp = DN_SPLITTER_ * a;
    hi = temp - (temp - a);
    lo = a - hi;
  }
  return dn_make_(hi, lo);
}
#endif /* DN_N_FMA_ */

/* Computes fl(a*b) and err(a*b). */
#define dn_two_prod_ DN_M(dn_two_prod_)
DN_INLINE struct dn dn_two_prod_(dn_native_t a, dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_two_prod_(dn_native_t a, dn_native_t b)
{
  const dn_native_t p = a * b;
#ifdef DN_N_FMA_
  const dn_native_t err = DN_N_FMA_(a, b, -p);
#else
  const struct dn ad = dn_split_(a);
  const struct dn bd = dn_split_(b);
  const dn_native_t err = ((dn_head_(ad) * dn_head_(bd) - p)
			   + dn_head_(ad) * dn_tail_(bd)
			   + dn_tail_(ad) * dn_head_(bd))
    + dn_tail_(ad) * dn_tail_(bd);
#endif
  return dn_make_(p, err);
}

/* Computes fl(a*a) and err(a*a).  Faster than the above method. */
#define dn_two_sqr_ DN_M(dn_two_sqr_)
DN_INLINE struct dn dn_two_sqr_(dn_native_t a) DN_ATTRS_CONST_;
struct dn
dn_two_sqr_(dn_native_t a)
{
  const dn_native_t p = a * a;
#ifdef DN_N_FMA_
  const dn_native_t err = DN_N_FMA_(a, a, -p);
#else
  const struct dn ad = dn_split_(a);
  const dn_native_t err = ((dn_head_(ad) * dn_head_(ad) - p)
			   + 2.0 * dn_head_(ad) * dn_tail_(ad))
    + dn_tail_(ad) * dn_tail_(ad);
#endif
  return dn_make_(p, err);
}

/* Computes the nearest integer to d. */
#define dn_nint_n_ DN_M(dn_nint_n_)
DN_INLINE dn_native_t dn_nint_n_(dn_native_t d) DN_ATTRS_CONST_;
dn_native_t
dn_nint_n_(dn_native_t d)
{
  if (d == DN_N_FLOOR_(d))
    return d;
  return DN_N_FLOOR_(d + 0.5);
}

#define dn_make DN_M(dn_make)
DN_INLINE struct dn dn_make(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_make(const dn_native_t a, const dn_native_t b)
{
  return dn_quick_two_sum_(a, b);
}

#define dn_add DN_M(dn_add)
DN_INLINE struct dn dn_add(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_add(const struct dn a, const struct dn b)
{
  /* This satisfies an IEEE754-style error bound, due to K. Briggs and
     W. Kahan.
  */
  struct dn s, t;

  s = dn_two_sum_(dn_head_(a), dn_head_(b));
  t = dn_two_sum_(dn_tail_(a), dn_tail_(b));
  s.x[1] += t.x[0];
  s = dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
  s.x[1] += t.x[1];
  return dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
}

/* double-double + double */
#define dn_add_n DN_M(dn_add_n)
DN_INLINE struct dn dn_add_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_add_n(const struct dn a, const dn_native_t b)
{
  struct dn s;
  s = dn_two_sum_(dn_head_(a), b);
  s.x[1] += dn_tail_(a);
  return dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
}

/* double-double - double-double */
#define dn_sub DN_M(dn_sub)
DN_INLINE struct dn dn_sub(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sub(const struct dn a, const struct dn b)
{
  struct dn s, t;
  s = dn_two_diff_(dn_head_(a), dn_head_(b));
  t = dn_two_diff_(dn_tail_(a), dn_tail_(b));
  s.x[1] += t.x[0];
  s = dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
  s.x[1] += t.x[1];
  return dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
}

#define dn_sub_n DN_M(dn_sub_n)
DN_INLINE struct dn dn_sub_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_sub_n(const struct dn a, const dn_native_t b)
{
  struct dn s;
  s = dn_two_diff_(dn_head_(a), b);
  s.x[1] += dn_tail_(a);
  return dn_quick_two_sum_(dn_head_(s), dn_tail_(s));
}

/* double-double * double-double */
#define dn_mul DN_M(dn_mul)
DN_INLINE struct dn dn_mul(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_mul(const struct dn a, const struct dn b)
{
  struct dn p;
  p = dn_two_prod_(dn_head_(a), dn_head_(b));
  p.x[1] += dn_head_(a)*dn_tail_(b);
  p.x[1] += dn_tail_(a)*dn_head_(b);
  p = dn_quick_two_sum_(dn_head_(p), dn_tail_(p));
  return p;
}

#define dn_sqr DN_M(dn_sqr)
DN_INLINE struct dn dn_sqr(const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_sqr(const struct dn a)
{
  struct dn p;
  p = dn_two_sqr_(dn_head_(a));
  p.x[1] += 2.0 * dn_head_(a)*dn_tail_(a);
  p.x[1] += dn_tail_(a)*dn_tail_(a);
  p = dn_quick_two_sum_(dn_head_(p), dn_tail_(p));
  return p;
}

/* double-double * double */
#define dn_mul_n DN_M(dn_mul_n)
DN_INLINE struct dn dn_mul_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_n(const struct dn a, const dn_native_t b)
{
  struct dn p;

  p = dn_two_prod_(dn_head_(a), b);
  p.x[1] += dn_tail_(a)*b;
  return dn_quick_two_sum_(dn_head_(p), dn_tail_(p));
}

#define dn_mul_n_n DN_M(dn_mul_n_n)
DN_INLINE struct dn dn_mul_n_n(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_n_n(const dn_native_t a, const dn_native_t b)
{
  return dn_two_prod_(a, b);
}

#define dn_sqr_n DN_M(dn_sqr_n)
DN_INLINE struct dn dn_sqr_n(const dn_native_t a)
  DN_ATTRS_CONST_;
struct dn
dn_sqr_n(const dn_native_t a)
{
  return dn_two_sqr_(a);
}

#define dn_mul_2 DN_M(dn_mul_2)
DN_INLINE struct dn dn_mul_2(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_2(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x[0]*b, a.x[1]*b);
}

#define dn_macc DN_M(dn_macc)
DN_INLINE struct dn dn_macc(const struct dn a, const struct dn b,
			     const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_macc(const struct dn a, const struct dn b, const struct dn c)
{
  return dn_add(dn_mul(a, b), c);
}

#define dn_macc_n DN_M(dn_macc_n)
DN_INLINE struct dn dn_macc_n(const dn_native_t a, const dn_native_t b,
			      const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_macc_n(const dn_native_t a, const dn_native_t b, const struct dn c)
{
  return dn_add(dn_mul_n_n(a, b), c);
}

#define dn_sqracc DN_M(dn_sqracc)
DN_INLINE struct dn dn_sqracc(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sqracc(const struct dn a, const struct dn b)
{
  return dn_add(dn_sqr(a), b);
}

#define dn_sqracc_n DN_M(dn_sqracc_n)
DN_INLINE struct dn dn_sqracc_n(const dn_native_t a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sqracc_n(const dn_native_t a, const struct dn b)
{
  return dn_add(dn_sqr_n(a), b);
}

#define dn_nmacc DN_M(dn_nmacc)
DN_INLINE struct dn dn_nmacc(const struct dn a, const struct dn b,
			     const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_nmacc(const struct dn a, const struct dn b, const struct dn c)
{
  return dn_sub(c, dn_mul(a, b));
}

#define dn_nmacc_n DN_M(dn_nmacc_n)
DN_INLINE struct dn dn_nmacc_n(const dn_native_t a, const dn_native_t b,
			       const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_nmacc_n(const dn_native_t a, const dn_native_t b, const struct dn c)
{
  return dn_sub(c, dn_mul_n_n(a, b));
}

#define dn_nsqracc DN_M(dn_nsqracc)
DN_INLINE struct dn dn_nsqracc(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_nsqracc(const struct dn a, const struct dn b)
{
  return dn_sub(b, dn_sqr(a));
}

#define dn_nsqracc_n DN_M(dn_nsqracc_n)
DN_INLINE struct dn dn_nsqracc_n(const dn_native_t a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_nsqracc_n(const dn_native_t a, const struct dn b)
{
  return dn_sub(b, dn_sqr_n(a));
}

/* double-double / double-double */
#define dn_div DN_M(dn_div)
DN_INLINE struct dn dn_div(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_div(const struct dn a, const struct dn b)
{
  dn_native_t q3;
  struct dn q, r;

  q.x[0] = dn_head_(a) / dn_head_(b);  /* approximate quotient */

  r = dn_sub(a, dn_mul_n(b, q.x[0]));

  q.x[1] = dn_head_(r) / dn_head_(b);
  r = dn_sub(r, dn_mul_n(b, q.x[1]));

  q3 = dn_head_(r) / dn_head_(b);

  q = dn_quick_two_sum_(dn_head_(q), dn_tail_(q));
  return dn_add_n(q, q3);
}

#define dn_div_n DN_M(dn_div_n)
DN_INLINE struct dn dn_div_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_div_n(const struct dn a, const dn_native_t b)
{
  dn_native_t q3;
  struct dn q, r;

  q.x[0] = dn_head_(a) / b;  /* approximate quotient */

  r = dn_sub(a, dn_two_prod_(b, q.x[0]));

  q.x[1] = dn_head_(r) / b;
  r = dn_sub(r, dn_two_prod_(b, q.x[1]));

  q3 = dn_head_(r) / b;

  q = dn_quick_two_sum_(dn_head_(q), dn_tail_(q));
  return dn_add_n(q, q3);
}

#define dn_div_n_n DN_M(dn_div_n_n)
DN_INLINE struct dn dn_div_n_n(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_div_n_n(const dn_native_t a, const dn_native_t b)
{
  dn_native_t q3, r;
  struct dn q;

  q.x[0] = a / b;  /* approximate quotient */

  r = DN_N_REM_(a, b);

  q.x[1] = r / b;
  r = DN_N_REM_(r, b);

  q3 = r / b;

  q = dn_quick_two_sum_(dn_head_(q), dn_tail_(q));
  return dn_add_n(q, q3);
}

#define dn_inv DN_M(dn_inv)
DN_INLINE struct dn dn_inv(const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_inv(const struct dn a)
{
  return dn_div(dn_one(), a);
}

#define dn_gt DN_M(dn_gt)
DN_INLINE DN_BOOL dn_gt(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_gt(const struct dn a, const struct dn b)
{
  return (dn_head_(a) > dn_head_(b) || (dn_head_(a) == dn_head_(b) && dn_tail_(a) > dn_tail_(b)));
}

#define dn_ge DN_M(dn_ge)
DN_INLINE DN_BOOL dn_ge(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ge(const struct dn a, const struct dn b)
{
  return (dn_head_(a) >= dn_head_(b) || (dn_head_(a) == dn_head_(b) && dn_tail_(a) >= dn_tail_(b)));
}

#define dn_lt DN_M(dn_lt)
DN_INLINE DN_BOOL dn_lt(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_lt(const struct dn a, const struct dn b)
{
  return (dn_head_(a) < dn_head_(b) || (dn_head_(a) == dn_head_(b) && dn_tail_(a) < dn_tail_(b)));
}

#define dn_le DN_M(dn_le)
DN_INLINE DN_BOOL dn_le(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_le(const struct dn a, const struct dn b)
{
  return (dn_head_(a) <= dn_head_(b) || (dn_head_(a) == dn_head_(b) && dn_tail_(a) <= dn_tail_(b)));
}

#define dn_eq DN_M(dn_eq)
DN_INLINE DN_BOOL dn_eq(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_eq(const struct dn a, const struct dn b)
{
  return (dn_head_(a) == dn_head_(b) && dn_tail_(a) == dn_tail_(b));
}

#define dn_ne DN_M(dn_ne)
DN_INLINE DN_BOOL dn_ne(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ne(const struct dn a, const struct dn b)
{
  return (dn_head_(a) != dn_head_(b) || dn_tail_(a) != dn_tail_(b));
}

#define dn_iszero DN_M(dn_iszero)
DN_INLINE DN_BOOL dn_iszero (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_iszero (const struct dn a)
{
  return dn_head_(a) == 0.0;
}

#define dn_isone DN_M(dn_isone)
DN_INLINE DN_BOOL dn_isone (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isone (const struct dn a)
{
  return dn_head_(a) == 1.0 && dn_tail_(a) == 0.0;
}

#define dn_isnegone DN_M(dn_isnegone)
DN_INLINE DN_BOOL dn_isnegone (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isnegone (const struct dn a)
{
  return dn_head_(a) == -1.0 && dn_tail_(a) == 0.0;
}

#define dn_ispos DN_M(dn_ispos)
DN_INLINE DN_BOOL dn_ispos (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ispos (const struct dn a)
{
  return dn_head_(a) > 0.0;
}

#define dn_isneg DN_M(dn_isneg)
DN_INLINE DN_BOOL dn_isneg (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isneg (const struct dn a)
{
  return dn_head_(a) < 0.0;
}

#define dn_abs DN_M(dn_abs)
DN_INLINE struct dn dn_abs (const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_abs (const struct dn a)
{
  if (dn_head_(a) < 0) return dn_negate(a);
  return a;
}

#define dn_copysign DN_M(dn_copysign)
DN_INLINE struct dn dn_copysign (const struct dn, const struct dn)
  DN_ATTRS_CONST_;
struct dn
dn_copysign (const struct dn x, const struct dn y)
{
  const dn_native_t hd = DN_N_COPYSIGN_(dn_head_(x), dn_head_(y));
  if (DN_N_COPYSIGN_(1.0, dn_head_(x)) != DN_N_COPYSIGN_(1.0, hd))
    return dn_make_(hd, -dn_tail_(x));
  return x; /* sign wasn't changed. */
}

#define dn_max DN_M(dn_max)
DN_INLINE struct dn dn_max (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_max (const struct dn a, const struct dn b)
{
  /* XXX: handle NaNs better... */
  if (dn_head_(a) > dn_head_(b))
    return a;
  if (dn_head_(a) == dn_head_(b) && dn_tail_(a) > dn_tail_(b))
    return a;
  return b;
}

#define dn_min DN_M(dn_min)
DN_INLINE struct dn dn_min (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_min (const struct dn a, const struct dn b)
{
  /* XXX: handle NaNs better... */
  if (dn_head_(a) < dn_head_(b))
    return a;
  if (dn_head_(a) == dn_head_(b) && dn_tail_(a) < dn_tail_(b))
    return a;
  return b;
}

#define dn_amax DN_M(dn_amax)
DN_INLINE struct dn dn_amax (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_amax (const struct dn a, const struct dn b)
{
  /* XXX: handle NaNs better... */
  const dn_native_t ah = DN_N_ABS_(dn_head_(a)),
    bh = DN_N_ABS_(dn_head_(b));
  if (ah > bh)
    return a;
  if (ah == bh && DN_N_ABS_(dn_tail_(a)) > DN_N_ABS_(dn_tail_(b)))
    return a;
  return b;
}

#define dn_amin DN_M(dn_amin)
DN_INLINE struct dn dn_amin (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_amin (const struct dn a, const struct dn b)
{
  /* XXX: handle NaNs better... */
  const dn_native_t ah = DN_N_ABS_(dn_head_(a)),
    bh = DN_N_ABS_(dn_head_(b));
  if (ah < bh)
    return a;
  if (ah == bh && DN_N_ABS_(dn_tail_(a)) < DN_N_ABS_(dn_tail_(b)))
    return a;
  return b;
}

/* Round to Nearest integer */
#define dn_nint DN_M(dn_nint)
DN_INLINE struct dn dn_nint (const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_nint (const struct dn a)
{
  dn_native_t hi = dn_nint_n_(dn_head_(a)), lo;

  if (DN_UNLIKELY_(hi == dn_head_(a))) {
    /* High word is an integer already.  Round the low word.*/
    lo = dn_nint_n_(dn_tail_(a));

    /* Renormalize. This is needed if hi = some integer, lo = 1/2.*/
    return dn_quick_two_sum_(hi, lo);
  } else {
    /* High word is not an integer. */
    lo = 0.0;
    if (DN_N_ABS_(hi - dn_head_(a)) == 0.5 && dn_tail_(a) < 0.0) {
      /* There is a tie in the high word, consult the low word
         to break the tie. */
      hi -= 1.0;  /* NOTE: This does not cause INEXACT. */
    }
    return dn_make_(hi, lo);
  }
}

#define dn_floor DN_M(dn_floor)
DN_INLINE struct dn dn_floor (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_floor(const struct dn a)
{
  struct dn out;
  out.x[0] = DN_N_FLOOR_(dn_head_(a));
  out.x[1] = 0.0;
  if (DN_UNLIKELY_(dn_head_(out) == dn_head_(a))) {
    /* High word is integer already.  Round the low word. */
    out.x[1] = DN_N_FLOOR_(dn_tail_(a));
    return dn_quick_two_sum_(dn_head_(out), dn_tail_(out));
  }
  return out;
}

#define dn_ceil DN_M(dn_ceil)
DN_INLINE struct dn dn_ceil (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_ceil(const struct dn a)
{
  struct dn out;
  out.x[0] = DN_N_CEIL_(dn_head_(a));
  out.x[1] = 0.0;
  if (DN_UNLIKELY_(dn_head_(out) == dn_head_(a))) {
    /* High word is integer already.  Round the low word. */
    out.x[1] = DN_N_CEIL_(dn_tail_(a));
    return dn_quick_two_sum_(dn_head_(out), dn_tail_(out));
  }
  return out;
}

#define dn_aint DN_M(dn_aint)
DN_INLINE struct dn dn_aint (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_aint (const struct dn a)
{
  if (dn_head_(a) >= 0.0)
    return dn_floor(a);
  return dn_ceil(a);
}

/* Computes the square root of the double-double number dd. */
#define dn_sqrt DN_M(dn_sqrt)
struct dn dn_sqrt (const struct dn a) DN_ATTRS_CONST_;

/* Reciprocal sqrt, somewhat sloppy */
#define dn_rsqrt DN_M(dn_rsqrt)
struct dn dn_rsqrt (const struct dn a) DN_ATTRS_CONST_;

#define dn_renormalize_ DN_M(dn_renormalize_)
DN_INLINE struct dn dn_renormalize_ (struct dn x) DN_ATTRS_CONST_;
struct dn
dn_renormalize_ (struct dn x)
{
  return dn_make(dn_head_(x), dn_tail_(x));
}
