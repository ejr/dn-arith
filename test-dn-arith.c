/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#include "dn-arith.h"

int nerrs = 0;

static void
test_add (void)
{
  struct dn a, b, c;
  double x, y;

  x = 1.0;
  y = LDBL_EPSILON/2.0;
  a = dn_make_n(x);
  b = dn_make_n(y);
  c = dn_add(a, b);
  c = dn_sub(c, a);
  if (y != dn_to_n(c)) {
    printf("Add/sub failed: %20.17e != %20.17e\n", y, (double)dn_to_n(c));
    ++nerrs;
  }
}

static void
test_sub (void)
{
  struct dn a, b, c;
  double x, y;

  x = 1.0;
  y = LDBL_EPSILON/16.0;
  a = dn_make_n(x);
  b = dn_make_n(y);
  c = dn_sub(a, b);
  c = dn_add(c, dn_negate(a));
  if (y != -dn_to_n(c)) {
    printf("Sub/negate failed: %20.17e != %20.17e\n", y, (double)dn_to_n(c));
    ++nerrs;
  }
}

static void
test_mul (void)
{
  struct dn a, b, c;
  long double tst;

  a = dn_make(1.0L, LDBL_EPSILON);
  b = dn_make(1.0L, LDBL_EPSILON);
  c = dn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = dn_sub(c, a); /* eps + eps**2 */
  c = dn_sub(c, dn_make_n(LDBL_EPSILON)); /* eps**2 */
  tst = LDBL_EPSILON*LDBL_EPSILON;
  if (tst != dn_to_n(c)) {
    printf("Mul/sub failed: %20.17e != %20.17e\n", (double)tst,
	   (double)dn_to_n(c));
    ++nerrs;
  }
}

static void
test_sqrt (void)
{
  struct dn a, b, c;

  a = dn_make(1.0L, LDBL_EPSILON);
  b = dn_make(1.0L, LDBL_EPSILON);
  c = dn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = dn_sqrt(c); /* 1+eps */
  c = dn_sub(c, a);
  if (!dn_iszero(c)) {
    printf("Sqrt/sub failed: abs(%20.17e) != 0\n", (double)dn_to_n(c));
    ++nerrs;
  }
}

static void
test_div (void)
{
  struct dn a, b, c;

  a = dn_make(1.0L, LDBL_EPSILON);
  b = dn_make(1.0L, LDBL_EPSILON);
  c = dn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = dn_div(c, a); /* 1+eps */
  c = dn_sub(c, b);
  if (!dn_iszero(c)) {
    printf("Sqrt/sub failed: %20.17e != 0\n", (double)dn_to_n(c));
    ++nerrs;
  }
}

static void
test_cmp (void)
{
  struct dn a, b;

  a = dn_make(1.0, LDBL_EPSILON);
  b = dn_make_n(1.0);
  if (!dn_gt(a, b)) {
    printf("dn_gt failed\n");
    ++nerrs;
  }
  if (!dn_lt(b, a)) {
    printf("dn_lt failed\n");
    ++nerrs;
  }
  if (!dn_ne(b, a)) {
    printf("dn_ne failed\n");
    ++nerrs;
  }
  b = dn_add_n(b, LDBL_EPSILON);
  if (!dn_eq(a, b)) {
    printf("dn_eq failed\n");
    ++nerrs;
  }
}

static void
test_to_dd (void)
{
  struct dn a, b;
  double d1, d2;

  a = dn_make(1.0, 2.0*DBL_EPSILON);
  b = dn_make(0.0, DBL_EPSILON/2.0);
  a = dn_add (a, b);
  dn_to_dd (a, &d1, &d2);
  if (d1 != 1.0+(2.0*DBL_EPSILON)) {
    printf("first half of dn_to_dd failed\n");
    ++nerrs;
  }
  if (d2 != DBL_EPSILON/2.0) {
    printf("second half of dn_to_dd failed\n");
    ++nerrs;
  }
}

int
main (void)
{
  test_add();
  test_mul();
  test_sqrt();
  test_div();
  test_sub();
  test_cmp();
  test_to_dd();
  return nerrs;
}
