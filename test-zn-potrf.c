/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"
#include "zn-lapack.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct zn *A;
  int i, j, info;

  A = calloc(ldA*N,sizeof(*A));

  for (j = 0; j < N; ++j)
    for (i = j; i < N; ++i)
      AREF(A,i,j) = zn_one();

  zn_potrf("Upper", N, A, ldA, &info);
  if (info) {
    printf("potrf ident U utterly failed! info %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i >= j) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("potrf ident U diag/lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("potrf ident U upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i > j) AREF(A,i,j) = zn_zero();
      else AREF(A,i,j) = zn_one();

  zn_potrf("Lower", N, A, ldA, &info);
  if (info) {
    printf("potrf ident L utterly failed! info %d\n", info);
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i <= j) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("potrf ident L diag/upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("potrf ident U lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(A);
}

static void
test_with_herk_L (void)
{
  const int N = 17, ldA = N, ldAsav = N, ldB = N;
  struct zn *A, *Asav, *B;
  struct dn epserr = dn_mul_n(dn_eps(), 2*N*N*N);
  struct zn big = zn_make_n(64.0);
  int i, j, info;

  A = malloc(ldA*N*sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Asav = malloc(ldAsav*N*sizeof(*Asav));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      if (i == j)
	AREF(Asav,i,j) = AREF(A,i,j) = big;
      else if (i > j) /* lower */
	AREF(Asav,i,j) = AREF(A,i,j) = zn_make_n(-1.0);
	/*
	AREF(Asav,i,j) = AREF(A,i,j) = zn_make(1.0-2.0*drand48(),
					       0.0);
					       //DBL_EPSILON*drand48());
					       */
      else /* upper */
	AREF(Asav,i,j) = AREF(A,i,j) = zn_zero();
    }

  zn_potrf("Lower", N, A, ldA, &info);
  zn_herk("Lower", "No trans", N, N, zn_one(), A, ldA, zn_zero(), B, ldB);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i >= j) { /* lower/diag */
	if (dn_ge(zn_abs(zn_sub(AREF(B,i,j),AREF(Asav,i,j))), epserr)) {
	  struct dn df = zn_abs(zn_sub(AREF(B,i,j),AREF(Asav,i,j)));
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* upper */
	if (!zn_iszero(AREF(A,i,j))) {
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(Asav);
  free(B);
  free(A);
}

static void
test_with_herk_U (void)
{
  const int N = 17, ldA = N, ldAsav = N, ldB = N;
  struct zn *A, *Asav, *B;
  struct dn epserr = dn_mul_n(dn_eps(), 2*N*N*N);
  struct zn big = zn_make_n(64.0);
  int i, j, info;

  A = malloc(ldA*N*sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Asav = malloc(ldAsav*N*sizeof(*Asav));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      if (i == j)
	AREF(Asav,i,j) = AREF(A,i,j) = big;
      else if (i < j) /* upper */
	AREF(Asav,i,j) = AREF(A,i,j) = zn_make_n(-1.0);
	/*
	AREF(Asav,i,j) = AREF(A,i,j) = zn_make(1.0-2.0*drand48(),
					       0.0);
					       //DBL_EPSILON*drand48());
					       */
      else /* lower */
	AREF(Asav,i,j) = AREF(A,i,j) = zn_zero();
    }

  zn_potrf("Upper", N, A, ldA, &info);
  zn_herk("Upper", "Trans", N, N, zn_one(), A, ldA, zn_zero(), B, ldB);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i <= j) { /* upper/diag */
	if (dn_ge(zn_abs(zn_sub(AREF(B,i,j),AREF(Asav,i,j))), epserr)) {
	  struct dn df = zn_abs(zn_sub(AREF(B,i,j),AREF(Asav,i,j)));
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (!zn_iszero(AREF(A,i,j))) {
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(Asav);
  free(B);
  free(A);
}

int
main (void)
{
  test_ident();
  test_with_herk_L();
  test_with_herk_U();
  return nerrs;
}
