# -*- Makefile -*- Copyright 2007 The Regents of the University of California.
# All rights reserved.  See COPYING for license.

dn_arith_inc=dn-arith.h
dn_arith_src=dn-arith-impl.c dn-arith-ops.c

dn_blas_inc=dn-blas.h
dn_blas_src=dn-dot.c dn-scal.c dn-gemm.c dn-gemv.c dn-ger.c dn-syrk.c	\
	dn-trsv.c dn-trsm.c dn-iamax.c dn-geamv.c dn-geamm.c

dn_lapack_inc=dn-lapack.h
dn_lapack_src=dn-potrf.c dn-getrf.c dn-laswp.c dn-getrs.c dn-getri.c

dn_inc=$(dn_arith_inc) $(dn_blas_inc) $(dn_lapack_inc)
dn_src=$(dn_arith_src) $(dn_blas_src) $(dn_lapack_src)
dn_str_src=dn-arith-str.c
