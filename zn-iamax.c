/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <stdio.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

int
DN_M(zn_iamax) (const int N, const struct zn * restrict X, const int incX)
{
    int j, curmx;
    struct dn mx;

    if (!N) { return 0; }

    mx = zn_abs1(VREF(X,0));
    curmx = 0;
    for (j = 1; j < N; ++j) {
	const struct dn xj = zn_abs1(VREF(X,j));
	if (dn_gt(xj, mx)) {
	    curmx = j;
	    mx = xj;
	}
    }
    return curmx;
}

int
DN_M(zn_iamax_f) (const int *N, const struct zn * restrict X, const int *incx)
{
    return 1 + DN_M(zn_iamax)(*N, X, *incx);
}
