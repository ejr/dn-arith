/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
typedef double dn_native_t;
#define DN_NATIVE_T double

struct dn {
  dn_native_t x[2];
};

#if defined(FP_FAST_FMA)
#define DN_N_FMA_ fma
#else
#define DN_SPLITTER_ (134217729.0)
/* = 2**27 + 1 */
#define DN_SPLIT_THRESH_ (6.69692879491417e+299)
/* \approx 2**(2**10-28) */
#define DN_SPLIT_SCALE_ (268435456.0)
/* = 2**28 */
#define DN_SPLIT_INV_SCALE_ (3.7252902984619140625e-09)
/* = 2**-28 */
#endif

#define DN_N_ABS_ fabs
#define DN_N_FLOOR_ floor
#define DN_N_CEIL_ ceil
#define DN_N_SQRT_ sqrt
#define DN_N_REM_ remainder
#define DN_N_ISNAN_ isnan
#define DN_N_ISINF_ isinf
#define DN_N_ISFINITE_ isfinite
#define DN_N_COPYSIGN_ copysign
#define DN_N_EPS_ DBL_EPSILON
#define DN_N_OV_ DBL_MAX
#define DN_N_UN_ DBL_MIN
