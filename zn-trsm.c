/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdio.h>
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

#define zn_gemm DN_M(zn_gemm)
void zn_gemm (const char *, const char *,
	      const int, const int, const int,
	      const struct zn,
	      const struct zn * DN_RESTRICT, const int,
	      const struct zn * DN_RESTRICT, const int,
	      const struct zn,
	      struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

enum diag { UNIT, DEFINED };

static void
trsm_LUN (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = M;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,K-1,K-1));
    for (int j = 0; j < N; ++j)
      AREF(B,K-1,j) = zn_mul(AREF(B,K-1,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,K-1-k,K-k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_mul( zn_nmacc(offdg, AREF(B,K-k,j),
					     AREF(B,K-1-k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_nmacc(offdg, AREF(B,K-k,j),
				     AREF(B,K-1-k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Notrans", n_to_update, N, n_to_pull,
		zn_negone(), &AREF(A,K-k-n_to_update,K-k), ldA,
		&AREF(B,K-k,0), ldB,
		zn_one(), &AREF(B,K-k-n_to_update,0), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_mul(AREF(B,K-1-k,j), invdg);
      }
    }
  }
}

static void
trsm_RUN (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = N;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,0,0));
    for (int j = 0; j < M; ++j)
      AREF(B,j,0) = zn_mul(AREF(B,j,0), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,k-1,k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_mul( zn_nmacc(offdg, AREF(B,j,k-1),
					 AREF(B,j,k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_nmacc(offdg, AREF(B,j,k-1),
				 AREF(B,j,k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Notrans", M, n_to_update, n_to_pull,
		zn_negone(),
		&AREF(B,0,k-n_to_pull), ldB,
		&AREF(A,k-n_to_pull,k), ldA,
		zn_one(), &AREF(B,0,k), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_mul(AREF(B,j,k), invdg);
      }
    }
  }
}

static void
trsm_LLN (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = M;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,0,0));
    for (int j = 0; j < N; ++j)
      AREF(B,0,j) = zn_mul(AREF(B,0,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,k,k-1);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_mul( zn_nmacc(offdg, AREF(B,k-1,j),
					 AREF(B,k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_nmacc(offdg, AREF(B,k-1,j),
				 AREF(B,k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Notrans", n_to_update, N, n_to_pull,
		zn_negone(), &AREF(A,k,k-n_to_pull), ldA,
		&AREF(B,k-n_to_pull,0), ldB,
		zn_one(), &AREF(B,k,0), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_mul(AREF(B,k,j), invdg);
      }
    }
  }
}

static void
trsm_RLN (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = N;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,K-1,K-1));
    for (int j = 0; j < M; ++j)
      AREF(B,j,K-1) = zn_mul(AREF(B,j,K-1), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,K-k,K-1-k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_mul( zn_nmacc(offdg, AREF(B,j,K-k),
					     AREF(B,j,K-1-k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_nmacc(offdg, AREF(B,j,K-k),
				     AREF(B,j,K-1-k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Notrans", M, n_to_update, n_to_pull,
		zn_negone(),
		&AREF(B,0,K-k), ldB,
		&AREF(A,K-k,K-k-n_to_update), ldA,
		zn_one(), &AREF(B,0,K-k-n_to_update), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_mul(AREF(B,j,K-1-k), invdg);
      }
    }
  }
}

static void
trsm_LUT (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = M;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,0,0));
    for (int j = 0; j < N; ++j)
      AREF(B,0,j) = zn_mul(AREF(B,0,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,k-1,k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_mul( zn_nmacc(offdg, AREF(B,k-1,j),
					 AREF(B,k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_nmacc(offdg, AREF(B,k-1,j),
				 AREF(B,k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Trans", "Notrans", n_to_update, N, n_to_pull,
		zn_negone(), &AREF(A,k-n_to_pull,k), ldA,
		&AREF(B,k-n_to_pull,0), ldB,
		zn_one(), &AREF(B,k,0), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = zn_mul(AREF(B,k,j), invdg);
      }
    }
  }
}

static void
trsm_RUT (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = N;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,K-1,K-1));
    for (int j = 0; j < M; ++j)
      AREF(B,j,K-1) = zn_mul(AREF(B,j,K-1), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,K-1-k,K-k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_mul( zn_nmacc(offdg, AREF(B,j,K-k),
					     AREF(B,j,K-1-k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_nmacc(offdg, AREF(B,j,K-k),
				     AREF(B,j,K-1-k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Trans", M, n_to_update, n_to_pull,
		zn_negone(),
		&AREF(B,0,K-k), ldB,
		&AREF(A,K-k-n_to_update,K-k), ldA,
		zn_one(), &AREF(B,0,K-k-n_to_update), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = zn_mul(AREF(B,j,K-1-k), invdg);
      }
    }
  }
}

static void
trsm_LLT (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = M;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,K-1,K-1));
    for (int j = 0; j < N; ++j)
      AREF(B,K-1,j) = zn_mul(AREF(B,K-1,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,K-k,K-1-k);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_mul( zn_nmacc(offdg, AREF(B,K-k,j),
					     AREF(B,K-1-k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_nmacc(offdg, AREF(B,K-k,j),
				     AREF(B,K-1-k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Trans", "Notrans", n_to_update, N, n_to_pull,
		zn_negone(), &AREF(A,K-k,K-k-n_to_update), ldA,
		&AREF(B,K-k,0), ldB,
		zn_one(), &AREF(B,K-k-n_to_update,0), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = zn_mul(AREF(B,K-1-k,j), invdg);
      }
    }
  }
}

static void
trsm_RLT (const enum diag diag, const int M, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict B, const int ldB)
{
  const int K = N;

  if (K == 0) return;
  if (diag != UNIT) {
    const struct zn invdg = zn_inv(AREF(A,0,0));
    for (int j = 0; j < M; ++j)
      AREF(B,j,0) = zn_mul(AREF(B,j,0), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct zn offdg = AREF(A,k,k-1);
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_mul( zn_nmacc(offdg, AREF(B,j,k-1),
					 AREF(B,j,k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_nmacc(offdg, AREF(B,j,k-1),
				 AREF(B,j,k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	zn_gemm("Notrans", "Trans", M, n_to_update, n_to_pull,
		zn_negone(),
		&AREF(B,0,k-n_to_pull), ldB,
		&AREF(A,k,k-n_to_pull), ldA,
		zn_one(), &AREF(B,0,k), ldB);
      }
      if (diag != UNIT) {
	const struct zn invdg = zn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = zn_mul(AREF(B,j,k), invdg);
      }
    }
  }
}

void
DN_M(zn_trsm)(const char *side, const char *uplo,
	      const char *transa, const char *diag,
	      const int M,
	      const int N,
	      const struct zn alpha,
	      const struct zn * restrict A, const int ldA,
	      struct zn * restrict B, const int ldB)
{
  int i, j;
  enum diag d;

  if (zn_iszero(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = zn_zero();
    return;
  }
  if (zn_isnegone(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = zn_negate(AREF(B,i,j));
  }
  else if (!zn_isone(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = zn_mul(alpha, AREF(B,i,j));
  }

  if (tolower(*diag) == 'u')
    d = UNIT;
  else
    d = DEFINED;

  if (tolower(*side) == 'l') {
    if (tolower(*uplo) == 'u') {
      if (tolower(*transa) == 'n')
	trsm_LUN(d, M, N, A, ldA, B, ldB);
      else
	trsm_LUT(d, M, N, A, ldA, B, ldB);
    }
    else {
      if (tolower(*transa) == 'n')
	trsm_LLN(d, M, N, A, ldA, B, ldB);
      else
	trsm_LLT(d, M, N, A, ldA, B, ldB);
    }
  }
  else { /* Right */
    if (tolower(*uplo) == 'u') {
      if (tolower(*transa) == 'n')
	trsm_RUN(d, M, N, A, ldA, B, ldB);
      else
	trsm_RUT(d, M, N, A, ldA, B, ldB);
    }
    else {
      if (tolower(*transa) == 'n')
	trsm_RLN(d, M, N, A, ldA, B, ldB);
      else
	trsm_RLT(d, M, N, A, ldA, B, ldB);
    }
  }

}

void
DN_M(zn_trsm_f)(const char *side, const char *uplo,
		const char *transa, const char *diag,
		const int *M,
		const int *N,
		const struct zn *alpha,
		const struct zn *A, const int *ldA,
		struct zn *B, const int *ldB)
{
  DN_M(zn_trsm)(side, uplo, transa, diag, *M, *N, *alpha, A, *ldA,
		B, *ldB);
}
