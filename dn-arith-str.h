/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/

/* ASCII-only, ignores current locale */
#define dn_asciitodn DN_M(dn_asciitodn)
struct dn dn_asciitodn(const char*, char **);
#define dn_asciitopdn DN_M(dn_asciitopdn)
int dn_asciitopdn(const char*, char **, struct dn*);

/* ASCII-only formatting */
#define dn_asciifmt DN_M(dn_asciifmt)
char *dn_asciifmt(char *, struct dn, int, unsigned);

