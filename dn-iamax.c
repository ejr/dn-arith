/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <stdio.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

int
DN_M(dn_iamax) (const int N, const struct dn * restrict X, const int incX)
{
    int j, curmx;
    struct dn mx;

    if (!N) { return 0; }

    mx = dn_abs(VREF(X,0));
    curmx = 0;
    for (j = 1; j < N; ++j) {
	const struct dn xj = dn_abs(VREF(X,j));
	if (dn_gt(xj, mx)) {
	    curmx = j;
	    mx = xj;
	}
    }
    return curmx;
}

int
DN_M(dn_iamax_f) (const int *N, const struct dn * restrict X, const int *incx)
{
    return 1 + DN_M(dn_iamax)(*N, X, *incx);
}
