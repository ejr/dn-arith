/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

#define DIMTHRESH 8

typedef void (*gemmfn) (const int M, const int N, const int K,
			const struct zn alpha,
			const struct zn * restrict a, const int lda,
			const struct zn * restrict b, const int ldb,
			struct zn * restrict c, const int ldc);

static void
gemm_NN (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_(K, &AREF(a,i,0), lda,
					  &AREF(b,0,j), 1);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgemm_NN (const int M, const int N, const int K,
	    const struct zn alpha,
	    const struct zn * restrict a, const int lda,
	    const struct zn * restrict b, const int ldb,
	    struct zn * restrict c, const int ldc)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is MxK, B is KxN, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    gemm_NN (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_NN (mlow, N, K, alpha, a, lda, b, ldb, c, ldc);
#pragma omp section
      recgemm_NN (mhigh, N, K, alpha, a+mlow, lda, b, ldb, c+mlow, ldc);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_NN (M, nlow, K, alpha, a, lda, b, ldb, c, ldc);
#pragma omp section
      recgemm_NN (M, nhigh, K, alpha, a, lda, b + nlow*ldb, ldb,
		  c + nlow*ldc, ldc);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgemm_NN (M, N, klow, alpha, a, lda, b, ldb, c, ldc);
    recgemm_NN (M, N, khigh, alpha, a + klow*lda, lda, b + klow, ldb, c, ldc);
  }
}

static void
gemm_TN (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_(K, &AREF(a,0,i), &AREF(b,0,j));
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
gemm_CN (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_nc_(K, &AREF(b,0,j), &AREF(a,0,i));
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgemm_TN (const int M, const int N, const int K,
	    const struct zn alpha,
	    const struct zn * restrict a, const int lda,
	    const struct zn * restrict b, const int ldb,
	    struct zn * restrict c, const int ldc,
	    gemmfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is KxM, B is KxN, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_TN (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_TN (mhigh, N, K, alpha, a+mlow*lda, lda, b, ldb, c+mlow, ldc,
		  fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_TN (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_TN (M, nhigh, K, alpha, a, lda, b + nlow*ldb, ldb,
		  c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgemm_TN (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgemm_TN (M, N, khigh, alpha, a + klow, lda, b + klow, ldb, c, ldc, fn);
  }
}

static void
gemm_NT (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_(K, &AREF(a,i,0), lda,
					  &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
gemm_NC (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_nc_(K, &AREF(a,i,0), lda,
					     &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgemm_NT (const int M, const int N, const int K,
	    const struct zn alpha,
	    const struct zn * restrict a, const int lda,
	    const struct zn * restrict b, const int ldb,
	    struct zn * restrict c, const int ldc,
	    gemmfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is MxK, B is NxK, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_NT (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_NT (mhigh, N, K, alpha, a+mlow, lda, b, ldb, c+mlow, ldc, fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_NT (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_NT (M, nhigh, K, alpha, a, lda, b + nlow, ldb,
		  c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgemm_NT (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgemm_NT (M, N, khigh, alpha, a + klow*lda, lda, b + klow*ldb, ldb,
		c, ldc, fn);
  }
}

static void
gemm_TT (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_(K, &AREF(a,0,i), 1,
					  &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
gemm_CT (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_nc_(K, &AREF(b,j,0), ldb,
					     &AREF(a,0,i), 1);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
gemm_TC (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_nc_(K, &AREF(a,0,i), 1,
					     &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
gemm_CC (const int M, const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict a, const int lda,
	 const struct zn * restrict b, const int ldb,
	 struct zn * restrict c, const int ldc)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct zn tmp = zn_sumprod_strided_cc_(K, &AREF(a,0,i), 1,
					     &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(c,i,j) = zn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgemm_TT (const int M, const int N, const int K,
	    const struct zn alpha,
	    const struct zn * restrict a, const int lda,
	    const struct zn * restrict b, const int ldb,
	    struct zn * restrict c, const int ldc,
	    gemmfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is KxM, B is NxK, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_TT (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_TT (mhigh, N, K, alpha, a+mlow*lda, lda, b, ldb, c+mlow, ldc,
		  fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgemm_TT (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgemm_TT (M, nhigh, K, alpha, a, lda, b + nlow, ldb,
		  c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgemm_TT (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgemm_TT (M, N, khigh, alpha, a + klow, lda, b + klow*ldb, ldb,
		c, ldc, fn);
  }
}

void
DN_M(zn_gemm)(const char *transa, const char *transb,
	      const int m, const int n, const int k,
	      const struct zn alpha,
	      const struct zn * restrict a, const int lda,
	      const struct zn * restrict b, const int ldb,
	      const struct zn beta,
	      struct zn * restrict c, const int ldc)
{
#pragma omp parallel
  if (!zn_isone(beta)) {
    if (zn_iszero(beta)) {
#pragma omp for nowait
      for (int j = 0; j < n; ++j)
	for (int i = 0; i < m; ++i)
	  AREF(c,i,j) = zn_zero();
    } else if (zn_isnegone(beta)) {
#pragma omp for nowait
      for (int j = 0; j < n; ++j)
	for (int i = 0; i < m; ++i)
	  AREF(c,i,j) = zn_negate(AREF(c,i,j));
    } else {
#pragma omp for nowait
      for (int j = 0; j < n; ++j)
	for (int i = 0; i < m; ++i)
	  AREF(c,i,j) = zn_mul(AREF(c,i,j), beta);
    }
  }

  if (zn_iszero(alpha)) return;

  if (tolower(*transa) == 'n' && tolower(*transb) == 'n')
    recgemm_NN(m, n, k, alpha, a, lda, b, ldb, c, ldc);
  else if (tolower(*transa) == 'n' && tolower(*transb) == 't')
    recgemm_NT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_NT);
  else if (tolower(*transa) == 'n' && tolower(*transb) == 'c')
    recgemm_NT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_NC);
  else if (tolower(*transa) == 't' && tolower(*transb) == 'n')
    recgemm_TN(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_TN);
  else if (tolower(*transa) == 'c' && tolower(*transb) == 'n')
    recgemm_TN(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_CN);
  else if (tolower(*transa) == 't' && tolower(*transb) == 't')
    recgemm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_TT);
  else if (tolower(*transa) == 't' && tolower(*transb) == 'c')
    recgemm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_TC);
  else if (tolower(*transa) == 'c' && tolower(*transb) == 't')
    recgemm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_CT);
  else
    recgemm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, gemm_CC);

}

void
DN_M(zn_gemm_f)(const char *transa, const char *transb,
		const int *m, const int *n, const int *k,
		const struct zn *alpha,
		const struct zn *a, const int *lda,
		const struct zn *b, const int *ldb,
		const struct zn *beta,
		struct zn *c, const int *ldc)
{
  DN_M(zn_gemm)(transa, transb, *m, *n, *k, *alpha, a, *lda,
		b, *ldb, *beta, c, *ldc);
}
