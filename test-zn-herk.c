/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_herk_N (void)
{
  const int K = 11, N = 38;
  const int ldA = N + 4, ldC = N + 7;
  struct zn *A, *C;
  int i, j;
  struct zn small = zn_make(dn_make_n(DBL_EPSILON/4096.0),
			    dn_make_n(DBL_EPSILON/4096.0));

  A = malloc(ldA*K*sizeof(*A));
  C = calloc(ldC*N,sizeof(*C));

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j)
	AREF(C,i,j) = small;
      else
	AREF(C,i,j) = zn_zero();

  for (j = 0; j < K; ++j)
    for (i = 0; i < N; ++i)
      AREF(A,i,j) = zn_I();

  zn_herk("Upper", "No trans", N, K, zn_one(), A, ldA, zn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (zn_ne(zn_make_n(K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (zn_ne(small, zn_sub_n(AREF(C,i,j), K))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (zn_ne(zn_zero(), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  zn_herk("Lower", "No trans", N, K, zn_negone(), A, ldA, zn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (zn_ne(zn_make_n(K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (zn_ne(small, AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (zn_ne(zn_make_n(-K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(C);
  free(A);
}

static void
test_herk_T (void)
{
  const int K = 11, N = 38;
  const int ldA = K + 4, ldC = N + 7;
  struct zn *A, *C;
  int i, j;
  struct zn small = zn_make(dn_make_n(DBL_EPSILON/4096.0),
			    dn_make_n(DBL_EPSILON/4096.0));

  A = malloc(ldA*N*sizeof(*A));
  C = calloc(ldC*N,sizeof(*C));

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j)
	AREF(C,i,j) = small;
      else
	AREF(C,i,j) = zn_zero();

  for (j = 0; j < N; ++j)
    for (i = 0; i < K; ++i)
      AREF(A,i,j) = zn_I();

  zn_herk("Upper", "Trans", N, K, zn_one(), A, ldA, zn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (zn_ne(zn_make_n(K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (zn_ne(small, zn_sub_n(AREF(C,i,j), K))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (zn_ne(zn_zero(), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  zn_herk("Lower", "Trans", N, K, zn_negone(), A, ldA, zn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (zn_ne(zn_make_n(K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (zn_ne(small, AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (zn_ne(zn_make_n(-K), AREF(C,i,j))) {
	  printf("test herk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(C);
  free(A);
}

int
main (void)
{
  test_herk_N();
  test_herk_T();
  return nerrs;
}
