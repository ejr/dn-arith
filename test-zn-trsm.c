/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident_L(void)
{
  const int M = 7, N = 11;
  const int ldA = M+5, ldB = M+7, ldBsav = M;
  int i, j;
  struct zn *A, *B, *Bsav;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = calloc(ldA*M, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Bsav = malloc(ldBsav*N*sizeof(*Bsav));

  srand48(3735);

  for (j = 0; j < M; ++j)
    AREF(A, j, j) = zn_one();

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(Bsav,i,j) = zn_make(dn_make(drand48(), drand48()),
			       dn_make(drand48(), drand48()));

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    AREF(B,i,j) = AREF(Bsav,i,j);

	zn_trsm("left", &uplo[ui], &trans[ti], &diag[di], M, N,
		zn_one(), A, ldA, B, ldB);

	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    if (zn_ne(AREF(B,i,j), AREF(Bsav,i,j))) {
	      printf("identL(%c%c%c)\\rand failed (%d %d)\n",
		     uplo[ui], trans[ti], diag[di], i, j);
	      ++nerrs;
	  }
      }

  free(Bsav);
  free(B);
  free(A);
}

static void
test_ident_R(void)
{
  const int M = 7, N = 11;
  const int ldA = N+5, ldB = M+7, ldBsav = M;
  int i, j;
  struct zn *A, *B, *Bsav;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = calloc(ldA*N, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Bsav = malloc(ldBsav*N*sizeof(*Bsav));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, j, j) = zn_one();

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(Bsav,i,j) = zn_make(dn_make(drand48(), drand48()),
			       dn_make(drand48(), drand48()));

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    AREF(B,i,j) = AREF(Bsav,i,j);

	zn_trsm("right", &uplo[ui], &trans[ti], &diag[di], M, N,
		zn_one(), A, ldA, B, ldB);

	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    if (zn_ne(AREF(B,i,j), AREF(Bsav,i,j))) {
	      printf("identR(%c%c%c)\\rand failed (%d %d)\n",
		     uplo[ui], trans[ti], diag[di], i, j);
	      ++nerrs;
	  }
      }

  free(Bsav);
  free(B);
  free(A);
}

static void
test_ones_L(void)
{
  const int M = 7, N = 11;
  const int ldA = M+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = calloc(ldA*M, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < M; ++j)
    for (i = 0; i < M; ++i)
      AREF(A, i, j) = zn_one();

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	for (j = 0; j < N; ++j)
	  if (tolower(uplo[ui]) == 'u') {
	    if (tolower(trans[ti]) == 'n')
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(M-i));
	    else
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(i+1));
	  }
	  else {
	    if (tolower(trans[ti]) == 'n')
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(i+1));
	    else
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(M-i));
	  }

	zn_trsm("left", &uplo[ui], &trans[ti], &diag[di], M, N,
		zn_one(), A, ldA, B, ldB);

	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    if (zn_ne(AREF(B,i,j), zn_one())) {
	      printf("onesL(%c%c%c)\\1:%d' failed (%d %d)\n",
		     uplo[ui], trans[ti], diag[di], M, i, j);
	      ++nerrs;
	  }
      }

  free(B);
  free(A);
}

static void
test_ones_R(void)
{
  const int M = 7, N = 11;
  const int ldA = N+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = calloc(ldA*N, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      AREF(A, i, j) = zn_one();

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	for (j = 0; j < N; ++j)
	  if (tolower(uplo[ui]) == 'u') {
	    if (tolower(trans[ti]) == 'n')
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(j+1));
	    else
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(N-j));
	  }
	  else {
	    if (tolower(trans[ti]) == 'n')
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(N-j));
	    else
	      for (i = 0; i < M; ++i)
		AREF(B,i,j) = zn_make_n((double)(j+1));
	  }

	zn_trsm("right", &uplo[ui], &trans[ti], &diag[di], M, N,
		zn_one(), A, ldA, B, ldB);

	for (j = 0; j < N; ++j)
	  for (i = 0; i < M; ++i)
	    if (zn_ne(AREF(B,i,j), zn_one())) {
	      printf("onesR(%c%c%c)\\1:%d' failed (%d %d)\n",
		     uplo[ui], trans[ti], diag[di], N, i, j);
	      ++nerrs;
	  }
      }

  free(B);
  free(A);
}

static void
test_threes_L(void)
{
  const int M = 7, N = 11;
  const int ldA = M+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  int ui, ti;

  A = calloc(ldA*M, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < M; ++j)
    for (i = 0; i < M; ++i)
      AREF(A, i, j) = zn_make_n(3.0);

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti) {
      for (j = 0; j < N; ++j)
	if (tolower(uplo[ui]) == 'u') {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(M-i));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(i+1));
	}
	else {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(i+1));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(M-i));
	}

      zn_trsm("left", &uplo[ui], &trans[ti], "N", M, N,
	      zn_one(), A, ldA, B, ldB);

      for (j = 0; j < N; ++j)
	for (i = 0; i < M; ++i)
	  if (zn_ne(AREF(B,i,j), zn_one())) {
	    printf("threesL(%c%cN)\\1:%d' failed (%d %d)\n",
		   uplo[ui], trans[ti], M, i, j);
	    ++nerrs;
	  }
    }

  free(B);
  free(A);
}

static void
test_threes_R(void)
{
  const int M = 7, N = 11;
  const int ldA = N+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  int ui, ti;

  A = calloc(ldA*N, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      AREF(A, i, j) = zn_make_n(3.0);

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti) {
      for (j = 0; j < N; ++j)
	if (tolower(uplo[ui]) == 'u') {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(j+1));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(N-j));
	}
	else {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(N-j));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(3.0*(j+1));
	}

      zn_trsm("right", &uplo[ui], &trans[ti], "N", M, N,
	      zn_one(), A, ldA, B, ldB);

      for (j = 0; j < N; ++j)
	for (i = 0; i < M; ++i)
	  if (zn_ne(AREF(B,i,j), zn_one())) {
	    printf("onesR(%c%cn)\\1:%d' failed (%d %d)\n",
		   uplo[ui], trans[ti], N, i, j);
	    ++nerrs;
	  }
    }

  free(B);
  free(A);
}

static void
test_alpha_L(void)
{
  const int M = 7, N = 11;
  const int ldA = M+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  int ui, ti;

  A = calloc(ldA*M, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < M; ++j)
    for (i = 0; i < M; ++i)
      AREF(A, i, j) = zn_one();

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti) {
      for (j = 0; j < N; ++j)
	if (tolower(uplo[ui]) == 'u') {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(M-i));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(i+1));
	}
	else {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(i+1));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(M-i));
	}

      zn_trsm("left", &uplo[ui], &trans[ti], "N", M, N,
	      zn_make_im_n(0.5), A, ldA, B, ldB);

      for (j = 0; j < N; ++j)
	for (i = 0; i < M; ++i)
	  if (zn_ne(AREF(B,i,j), zn_I())) {
	    printf("alphaL(%c%cN)\\1:%d' failed (%d %d)\n",
		   uplo[ui], trans[ti], M, i, j);
	    ++nerrs;
	  }
    }

  free(B);
  free(A);
}

static void
test_alpha_R(void)
{
  const int M = 7, N = 11;
  const int ldA = N+5, ldB = M+7;
  int i, j;
  struct zn *A, *B;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  int ui, ti;

  A = calloc(ldA*N, sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));

  srand48(3735);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      AREF(A, i, j) = zn_one();

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti) {
      for (j = 0; j < N; ++j)
	if (tolower(uplo[ui]) == 'u') {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(j+1));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(N-j));
	}
	else {
	  if (tolower(trans[ti]) == 'n')
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(N-j));
	  else
	    for (i = 0; i < M; ++i)
	      AREF(B,i,j) = zn_make_n(2.0*(j+1));
	}

      zn_trsm("right", &uplo[ui], &trans[ti], "N", M, N,
	      zn_make_n(0.5), A, ldA, B, ldB);

      for (j = 0; j < N; ++j)
	for (i = 0; i < M; ++i)
	  if (zn_ne(AREF(B,i,j), zn_one())) {
	    printf("onesR(%c%cn)\\1:%d' failed (%d %d)\n",
		   uplo[ui], trans[ti], N, i, j);
	    ++nerrs;
	  }
    }

  free(B);
  free(A);
}

int
main (void)
{
  test_ident_L();
  test_ident_R();
  test_ones_L();
  test_ones_R();
  test_threes_L();
  test_threes_R();
  test_alpha_L();
  test_alpha_R();
  return nerrs;
}
