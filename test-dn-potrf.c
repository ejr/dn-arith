/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"
#include "dn-lapack.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct dn *A;
  int i, j, info;

  A = calloc(ldA*N,sizeof(*A));

  for (j = 0; j < N; ++j)
    for (i = j; i < N; ++i)
      AREF(A,i,j) = dn_one();

  dn_potrf("Upper", N, A, ldA, &info);
  if (info) {
    printf("potrf ident U utterly failed! info %d\n", info);
    if (info > 0)
      printf("   %g %g\n", (double)dn_head_(AREF(A,info,info)),
	     (double)dn_tail_(AREF(A,info,info)));
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i >= j) {
	if (dn_ne(dn_one(), AREF(A,i,j))) {
	  printf("potrf ident U diag/lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (dn_ne(dn_zero(), AREF(A,i,j))) {
	  printf("potrf ident U upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i > j) AREF(A,i,j) = dn_zero();
      else AREF(A,i,j) = dn_one();

  dn_potrf("Lower", N, A, ldA, &info);
  if (info) {
    printf("potrf ident L utterly failed! info %d\n", info);
    if (info > 0)
      printf("   %g %g\n", (double)dn_head_(AREF(A,info,info)),
	     (double)dn_tail_(AREF(A,info,info)));
    ++nerrs;
    return;
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i <= j) {
	if (dn_ne(dn_one(), AREF(A,i,j))) {
	  printf("potrf ident L diag/upper: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (dn_ne(dn_zero(), AREF(A,i,j))) {
	  printf("potrf ident U lower: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(A);
}

static void
test_with_syrk_L (void)
{
  const int N = 17, ldA = N, ldAsav = N, ldB = N;
  struct dn *A, *Asav, *B;
  struct dn epserr = dn_mul_n(dn_eps(), N*N*N);
  struct dn big = dn_make_n(64.0);
  int i, j, info;

  A = malloc(ldA*N*sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Asav = malloc(ldAsav*N*sizeof(*Asav));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      if (i == j)
	AREF(Asav,i,j) = AREF(A,i,j) = big;
      else if (i > j) /* lower */
	//AREF(Asav,i,j) = AREF(A,i,j) = dn_make_n(-1.0);
	AREF(Asav,i,j) = AREF(A,i,j) = dn_make(1.0-2.0*drand48(),
					       //0.0);
					       DBL_EPSILON*drand48());
      else /* upper */
	AREF(Asav,i,j) = AREF(A,i,j) = dn_zero();
    }

  dn_potrf("Lower", N, A, ldA, &info);
  dn_syrk("Lower", "No trans", N, N, dn_one(), A, ldA, dn_zero(), B, ldB);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i >= j) { /* lower/diag */
	if (dn_ge(dn_abs(dn_sub(AREF(B,i,j),AREF(Asav,i,j))), epserr)) {
	  struct dn df = dn_abs(dn_sub(AREF(B,i,j),AREF(Asav,i,j)));
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  printf("  sav %g %g\n"
		 "  new %g %g\n"
		 "  dif %e %e   %e\n",
		 (double)dn_head_(AREF(Asav,i,j)),
		 (double)dn_tail_(AREF(Asav,i,j)),
		 (double)dn_head_(AREF(B,i,j)),
		 (double)dn_tail_(AREF(B,i,j)),
		 (double)dn_head_(df),
		 (double)dn_tail_(df),
		 (double)dn_head_(epserr)
		 );
	  ++nerrs;
	}
      } else { /* upper */
	if (!dn_iszero(AREF(A,i,j))) {
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(Asav);
  free(B);
  free(A);
}

static void
test_with_syrk_U (void)
{
  const int N = 17, ldA = N, ldAsav = N, ldB = N;
  struct dn *A, *Asav, *B;
  struct dn epserr = dn_mul_n(dn_eps(), N*N*N);
  struct dn big = dn_make_n(64.0);
  int i, j, info;

  A = malloc(ldA*N*sizeof(*A));
  B = malloc(ldB*N*sizeof(*B));
  Asav = malloc(ldAsav*N*sizeof(*Asav));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      if (i == j)
	AREF(Asav,i,j) = AREF(A,i,j) = big;
      else if (i < j) /* upper */
	//AREF(Asav,i,j) = AREF(A,i,j) = dn_make_n(-1.0);
	AREF(Asav,i,j) = AREF(A,i,j) = dn_make(1.0-2.0*drand48(),
					       //0.0);
					       DBL_EPSILON*drand48());
      else /* lower */
	AREF(Asav,i,j) = AREF(A,i,j) = dn_zero();
    }

  dn_potrf("Upper", N, A, ldA, &info);
  dn_syrk("Upper", "Trans", N, N, dn_one(), A, ldA, dn_zero(), B, ldB);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i <= j) { /* upper/diag */
	if (dn_ge(dn_abs(dn_sub(AREF(B,i,j),AREF(Asav,i,j))), epserr)) {
	  struct dn df = dn_abs(dn_sub(AREF(B,i,j),AREF(Asav,i,j)));
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  printf("  sav %g %g\n"
		 "  new %g %g\n"
		 "  dif %e %e   %e\n",
		 (double)dn_head_(AREF(Asav,i,j)),
		 (double)dn_tail_(AREF(Asav,i,j)),
		 (double)dn_head_(AREF(B,i,j)),
		 (double)dn_tail_(AREF(B,i,j)),
		 (double)dn_head_(df),
		 (double)dn_tail_(df),
		 (double)dn_head_(epserr)
		 );
	  ++nerrs;
	}
      } else { /* lower */
	if (!dn_iszero(AREF(A,i,j))) {
	  printf("potrf L reform failed (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(Asav);
  free(B);
  free(A);
}

int
main (void)
{
  test_ident();
  test_with_syrk_L();
  test_with_syrk_U();
  return nerrs;
}
