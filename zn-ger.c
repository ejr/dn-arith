/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

void
DN_M(zn_gerc)(const int M, const int N,
	      const struct zn alpha,
	      const struct zn * restrict X, const int incX,
	      const struct zn * restrict Y, const int incY,
	      struct zn * restrict A, const int ldA)
{
#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    const struct zn yel = zn_mul(alpha, zn_conj(VREF(Y,j)));
    for (int i = 0; i < M; ++i)
      AREF(A,i,j) = zn_macc(VREF(X,i), yel, AREF(A,i,j));
  }
}

void
DN_M(zn_geru)(const int M, const int N,
	      const struct zn alpha,
	      const struct zn * restrict X, const int incX,
	      const struct zn * restrict Y, const int incY,
	      struct zn * restrict A, const int ldA)
{
#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    const struct zn yel = zn_mul(alpha, VREF(Y,j));
    for (int i = 0; i < M; ++i)
      AREF(A,i,j) = zn_macc(VREF(X,i), yel, AREF(A,i,j));
  }
}

void
DN_M(zn_gerc_f)(const int *M, const int *N,
		const struct zn *alpha,
		const struct zn *X, const int *incX,
		const struct zn *Y, const int *incY,
		struct zn *A, const int *ldA)
{
  DN_M(zn_gerc)(*M, *N, *alpha, X, *incX, Y, *incY, A, *ldA);
}

void
DN_M(zn_geru_f)(const int *M, const int *N,
		const struct zn *alpha,
		const struct zn *X, const int *incX,
		const struct zn *Y, const int *incY,
		struct zn *A, const int *ldA)
{
  DN_M(zn_geru)(*M, *N, *alpha, X, *incX, Y, *incY, A, *ldA);
}
