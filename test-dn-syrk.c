/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_syrk_N (void)
{
  const int K = 11, N = 38;
  const int ldA = N + 4, ldC = N + 7;
  struct dn *A, *C;
  int i, j;
  struct dn small = dn_make_n(DBL_EPSILON/4096.0);

  A = malloc(ldA*K*sizeof(*A));
  C = calloc(ldC*N,sizeof(*C));

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j)
	AREF(C,i,j) = small;
      else
	AREF(C,i,j) = dn_zero();

  for (j = 0; j < K; ++j)
    for (i = 0; i < N; ++i)
      AREF(A,i,j) = dn_one();

  dn_syrk("Upper", "No trans", N, K, dn_one(), A, ldA, dn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (dn_ne(dn_make_n(K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (dn_ne(small, dn_sub_n(AREF(C,i,j), K))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (dn_ne(dn_zero(), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  dn_syrk("Lower", "No trans", N, K, dn_negone(), A, ldA, dn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (dn_ne(dn_make_n(K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (dn_ne(small, AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (dn_ne(dn_make_n(-K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(C);
  free(A);
}

static void
test_syrk_T (void)
{
  const int K = 11, N = 38;
  const int ldA = K + 4, ldC = N + 7;
  struct dn *A, *C;
  int i, j;
  struct dn small = dn_make_n(DBL_EPSILON/4096.0);

  A = malloc(ldA*N*sizeof(*A));
  C = calloc(ldC*N,sizeof(*C));

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j)
	AREF(C,i,j) = small;
      else
	AREF(C,i,j) = dn_zero();

  for (j = 0; j < N; ++j)
    for (i = 0; i < K; ++i)
      AREF(A,i,j) = dn_one();

  dn_syrk("Upper", "Trans", N, K, dn_one(), A, ldA, dn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (dn_ne(dn_make_n(K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (dn_ne(small, dn_sub_n(AREF(C,i,j), K))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (dn_ne(dn_zero(), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  dn_syrk("Lower", "Trans", N, K, dn_negone(), A, ldA, dn_one(), C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i < j) { /* upper */
	if (dn_ne(dn_make_n(K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else if (i == j) { /* diag */
	if (dn_ne(small, AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      } else { /* lower */
	if (dn_ne(dn_make_n(-K), AREF(C,i,j))) {
	  printf("test syrk N U failed: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(C);
  free(A);
}

int
main (void)
{
  test_syrk_N();
  test_syrk_T();
  return nerrs;
}
