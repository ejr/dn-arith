#include "config.h"

#include "dn-arith"

#include <limits>
#include "dn-arith-limits"

#if defined(HAVE_TYPE_TRAITS)
#include <type_traits>
#include "dn-arith-traits"
#elif defined(HAVE_TR1_TYPE_TRAITS)
#include <tr1/type_traits>
#include "dn-arith-traits"
#elif defined(HAVE_BOOST_TR1_TYPE_TRAITS)
#include <boost/tr1/type_traits>
#include "dn-arith-traits"
#endif

#include <iostream>
#include <iomanip>
#include <cstring>
#include <string>

#include <stdio.h>

#include "dn-arith-io"

#if defined(dn_asciifmt)
std::ostream &
operator<<(std::ostream & os, struct ::dn x)
{
     std::ostream::sentry oss(os);
     if (!bool(oss)) return os;

     using std::min;
     using std::max;

     bool showpos = (os.flags() & os.showpos) != 0;
     bool leftjust = (os.flags() & os.left) != 0;
     std::streamsize prec = os.precision();
     std::streamsize width = os.width(), bufwidth, w2;

     if (prec <= 0) return os;
     bufwidth = (width? width : max(std::streamsize(20), prec+4));

     char *buf, *b2;
     double dd[2];

     buf = new char[bufwidth+1];

     b2 = buf;
     w2 = bufwidth;
     if (showpos && x > 0.0) {
	  os << '+';
	  ++b2;
	  --w2;
     }
     b2 = dn_asciifmt(b2, x, prec, w2);
     if (!b2) {
	  while (width--) os << 'X';
	  os.setstate(os.badbit);
	  return os;
     }

     w2 = strlen(buf);
     if (w2 >= width) width = 0;
     else width -= strlen(buf);

     if (leftjust) {
	  os << buf;
	  while (width--)
	       os << os.fill();
     } else {
	  while (width--)
	       os << os.fill();
	  os << buf;
     }

     return os;
}

/*
  This is a miserable definition.  It'll eat *past* an error.  So
  "1.0fFHNBRI DJDJ" >> dn >> s will give 1.0 to dn and "DJDJ" to s
  when it *should* give 1.0 to dn and FHNBRI to s.  But I'm not up to
  proper lexing (incl. Inf and Nan) right now.
*/
std::istream &
operator>>(std::istream & is, struct ::dn & x)
{
     std::istream::sentry iss(is);
     if (!bool(iss)) return is;
     std::string buf;
     const char *cstr, *endptr;
     is >> buf;
     if (!buf.length()) {
	  x = dn_zero();
	  return is;
     }
     cstr = buf.c_str();
     x = dn_asciitodn(cstr, const_cast<char**>(&endptr));
     if (cstr == endptr) {
	  is.setstate(is.badbit);
	  x = dn_nan();
     }
     return is;
}
#else // defined(dn_asciifmt)
std::ostream &
operator<<(std::ostream & os, struct ::dn x)
{
     std::ostream::sentry oss(os);
     os << dn_head_(x) << "+" << dn_tail_(x);
     return os;
}
std::istream &
operator>>(std::istream & is, struct ::dn & x)
{
     std::istream::sentry iss(is);
     if (!bool(iss)) return is;
     dn_native_t xx;
     is >> xx;
     x = dn_make(xx);
     return is;
}
#endif // defined(dn_asciifmt)

