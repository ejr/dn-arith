/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include "dn-arith.h"
#include "dn-ops-internal.h"

/*
  I'm not at all sure I have an alias-safe union here...
*/

struct dpair {
  double d[2];
};

union dpairu {
  long double ld;
  struct dpair x;
};

static dn_native_t force_head(long double x)
  DN_ATTRS_CONST_;
dn_native_t
force_head(long double x)
{
  union dpairu foo;
  foo.ld = x;
  return foo.x.d[0];
}

static dn_native_t force_tail(long double x)
  DN_ATTRS_CONST_;
dn_native_t
force_tail(long double x)
{
  union dpairu foo;
  foo.ld = x;
  return foo.x.d[1];
}

struct dn
dn_renormalize_ (struct dn x)
{
  return ((struct dn){.x = force_head(x.x) + force_tail(x.x)});
}

static struct dpair dn_quick_two_sum_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dpair
dn_quick_two_sum_(const dn_native_t a, const dn_native_t b)
{
  struct dpair out;
  out.d[0] = a + b;
  out.d[1] = b - (out.d[0] - a);
  return out;
}

DN_INLINE struct dpair dn_two_sum_(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dpair
dn_two_sum_(const dn_native_t a, const dn_native_t b)
{
  struct dpair out;
  out.d[0] = a + b;
  const dn_native_t bb = out.d[0] - a;
  out.d[1] = (a - (out.d[0] - bb)) + (b - bb);
  return out;
}

struct dpair dn_running_sum_n_ (const struct dpair x,
				const dn_native_t y)
  DN_ATTRS_CONST_;
struct dpair
dn_running_sum_n_ (const struct dpair x, const dn_native_t y)
{
  /* Compensated summation without renormalization, or one step of
     Sum2s in Ogita, Rump, & Oishi. */
  struct dpair pq = dn_two_sum_(x.d[0], y);
  pq.d[1] += x.d[1];
  return pq;
}

struct dpair dn_running_sum_ (const struct dpair x,
			      const struct dpair y)
  DN_ATTRS_CONST_;
struct dpair
dn_running_sum_ (const struct dpair x, const struct dpair y)
{
  return dn_running_sum_n_(dn_running_sum_n_(x, y.d[0]), y.d[1]);
}

struct dn dn_running_sum_wrapped_ (const struct dn x, const struct dn y)
  DN_ATTRS_CONST_;
struct dn
dn_running_sum_wrapped_ (const struct dn x, const struct dn y)
{
  union dpairu xu, yu, ou;
  xu.ld = x.x;
  yu.ld = y.x;
  ou.x = dn_running_sum_n_(dn_running_sum_n_(xu.x, yu.x.d[0]), yu.x.d[1]);
  return (struct dn)({.x = ou.ld});
}

struct dn
dn_sum_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = x[0];
  sm.d[1] = x[1];
  for (i = 2; i < n; ++i)
    sm = dn_running_sum_n_(sm, x[i]);

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_sum_d_core_ (const size_t n, const dn_native_t *x)
{
  return dn_sum_n_core_(n, x);
}

struct dn
dn_asum_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = DN_N_ABS_(x[0]);
  sm.d[1] = DN_N_ABS_(x[1]);
  for (i = 2; i < n; ++i)
    sm = dn_running_sum_n_(sm, DN_N_ABS_(x[i]));

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_asum_d_core_ (const size_t n, const double *x)
{
  return dn_asum_n_core_(n, x);
}

static struct dn
asum_core_ (const size_t n, const double *x)
{
  struct dpair sm;
  union dpairu out;
  size_t i;

  sm.d[0] = x[0];
  sm.d[1] = x[1];
  for (i = 2; i < n; i += 2) {
    if (x[i] < 0)
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, -x[i]), -x[i+1]);
    else
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, x[i]), x[i+1]);
  }

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_asum_core_ (const size_t n, const struct dn *x)
{
  return asum_core_(2*n, (const double*)x);
}

struct dn
dn_sum_strided_n_core_ (const size_t n, const dn_native_t *x,
			const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = x[0];
  sm.d[1] = x[stride];
  for (i = 2; i < n; ++i)
    sm = dn_running_sum_n_(sm, x[i*stride]);

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_sum_strided_d_core_ (const size_t n, const double *x,
			const size_t stride)
{
  return dn_sum_strided_n_core_(n, x, stride);
}

struct dn
dn_sum_strided_np_core_ (const size_t n, const dn_native_t *x,
			 const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = x[0];
  sm.d[1] = x[stride];
  for (i = 2; i < n; ++i) {
    sm = dn_running_sum_n_(sm, x[i*stride]);
    sm = dn_running_sum_n_(sm, x[i*stride+1]);
  }

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_sum_strided_dp_core_ (const size_t n, const double *x,
			 const size_t stride)
{
  return dn_sum_strided_np_core_(n, x, stride);
}

static struct dn
asum_strided_core_ (const size_t n, const double *x, const size_t stride)
{
  struct dpair sm;
  union dpairu out;
  size_t i;

  sm.d[0] = x[0];
  sm.d[1] = x[1];
  for (i = 2; i < n; i += 2) {
    const size_t k = i*stride;
    if (x[i] < 0)
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, -x[k]), -x[k+1]);
    else
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, x[k]), x[k+1]);
  }

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_asum_strided_core_ (const size_t n, const struct dn *x,
		       const size_t stride)
{
  return asum_strided_core_(2*n, (const double*)x, 2*stride);
}

struct dn
dn_asum_strided_n_core_ (const size_t n, const dn_native_t *x,
			 const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = DN_N_ABS_(x[0]);
  sm.d[1] = DN_N_ABS_(x[stride]);
  for (i = 2; i < n; ++i)
    sm = dn_running_sum_n_(sm, x[i*stride]);

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_asum_strided_d_core_ (const size_t n, const double *x,
			 const size_t stride)
{
  return dn_asum_strided_n_core_(n, x, stride);
}

struct dn
dn_asum_strided_np_core_ (const size_t n, const dn_native_t *x,
			  const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu out;

  sm.d[0] = DN_N_ABS_(x[0]);
  sm.d[1] = DN_N_ABS_(x[stride]);
  for (i = 2; i < n; ++i) {
    const size_t k = i*stride;
    sm = dn_running_sum_n_(sm, DN_N_ABS_(x[k]));
    sm = dn_running_sum_n_(sm, DN_N_ABS_(x[k+1]));
  }

  out.x = sm;
  return ((struct dn){.x = out.ld});
}

struct dn
dn_asum_strided_dp_core_ (const size_t n, const double *x,
			  const size_t stride)
{
  return dn_asum_strided_np_core_(n, x, stride);
}


struct dn
dn_sumprod_n_core_ (const size_t n, const dn_native_t *x, const dn_native_t *y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0] * (long double)y[0];
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i] * (long double)y[i];
    sm = dn_running_sum_(sm, tmp.x);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumprod_d_core_ (const size_t n, const double *x, const double *y)
{
  return dn_sumprod_d_core_(n, x, y);
}

struct dn
dn_asumprod_n_core_ (const size_t n,
		     const dn_native_t *x, const dn_native_t *y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = DN_N_ABS_(x[0]) * (long double)DN_N_ABS_(y[0]);
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = DN_N_ABS_(x[i]) * (long double)DN_N_ABS_(y[i]);
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_asumprod_d_core_ (const size_t n, const double *x, const double *y)
{
  return dn_asumprod_n_core_(n, x, y);
}

struct dn
dn_sumprod_core_ (const size_t n, const struct dn *x, const struct dn *y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x * y[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i].x * y[i].x;
    sm = dn_running_sum_(sm, tmp.x);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_asumprod_core_ (const size_t n, const struct dn *x, const struct dn *y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x * y[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i].x * y[i].x;
    if (tmp.x.d[0] < 0)
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, -tmp.x.d[0]), -tmp.x.d[1]);
    else
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, tmp.x.d[0]), tmp.x.d[1]);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumprod_strided_core_ (const size_t n,
			  const struct dn *x, const size_t stride_x,
			  const struct dn *y, const size_t stride_y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x * y[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i*stride_x].x * y[i*stride_y].x;
    sm = dn_running_sum_(sm, tmp.x);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumprod_strided_n_core_ (const size_t n,
			    const dn_native_t *x, const size_t stride_x,
			    const dn_native_t *y, const size_t stride_y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0] * (long double)y[0];
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i*stride_x] * (long double)y[i*stride_y];
    sm = dn_running_sum_(sm, tmp.x);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumprod_strided_d_core_ (const size_t n,
			    const double *x, const size_t stride_x,
			    const double *y, const size_t stride_y)
{
  return dn_sumprod_strided_n_core_(n, x, stride_x, y, stride_y);
}

struct dn
dn_asumprod_strided_core_ (const size_t n,
			   const struct dn *x, const size_t stride_x,
			   const struct dn *y, const size_t stride_y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x * y[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i*stride_x].x * y[i*stride_y].x;
    if (tmp.x.d[0] < 0)
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, -tmp.x.d[0]), -tmp.x.d[1]);
    else
      sm = dn_running_sum_n_(dn_running_sum_n_(sm, tmp.x.d[0]), tmp.x.d[1]);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_asumprod_strided_n_core_ (const size_t n,
			     const dn_native_t *x, const size_t stride_x,
			     const dn_native_t *y, const size_t stride_y)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = DN_N_ABS_(x[0]) * (long double)DN_N_ABS_(y[0]);
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = DN_N_ABS_(x[i*stride_x]) * (long double)DN_N_ABS_(y[i*stride_y]);
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_asumprod_strided_d_core_ (const size_t n,
			     const double *x, const size_t stride_x,
			     const double *y, const size_t stride_y)
{
  return dn_asumprod_strided_n_core_(n, x, stride_x, y, stride_y);
}

struct dn
dn_sumsqr_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0]*(long double)x[0];
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i]*(long double)x[i];
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumsqr_d_core_ (const size_t n, const double *x)
{
  return dn_sumsqr_n_core_(n, x);
}

struct dn
dn_sumsqr_core_ (const size_t n, const struct dn *x)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x*x[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    tmp.ld = x[i].x*x[i].x;
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumsqr_strided_n_core_ (const size_t n, const dn_native_t *x,
			   const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0]*(long double)x[0];
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    const size_t k = i*stride;
    tmp.ld = x[k]*(long double)x[k];
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}

struct dn
dn_sumsqr_strided_d_core_ (const size_t n, const double *x,
			   const size_t stride)
{
  return dn_sumsqr_strided_n_core_(n, x, stride);
}

struct dn
dn_sumsqr_strided_core_ (const size_t n, const struct dn *x,
			 const size_t stride)
{
  size_t i;
  struct dpair sm;
  union dpairu tmp;

  tmp.ld = x[0].x*x[0].x;
  sm = tmp.x;
  for (i = 1; i < n; ++i) {
    const size_t k = i*stride;
    tmp.ld = x[k].x*x[k].x;
    sm = dn_running_sum_(tmp.x, sm);
  }

  tmp.x = sm;
  return ((struct dn){.x = tmp.ld});
}
