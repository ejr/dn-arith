/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#include "zn-arith.h"

int nerrs = 0;

static void
test_add (void)
{
  struct zn a, b, c;
  double x, y;

  x = 1.0;
  y = LDBL_EPSILON/2.0;
  a = zn_make_dn(dn_make_n(x));
  b = zn_make_dn(dn_make_n(y));
  c = zn_add(a, b);
  c = zn_sub(c, a);
  if (y != dn_to_n(zn_real(c))) {
    printf("Add/sub failed: %20.17e != %20.17e\n", y, (double)dn_to_n(zn_real(c)));
    ++nerrs;
  }
}

static void
test_sub (void)
{
  struct zn a, b, c;
  double x, y;

  x = 1.0;
  y = LDBL_EPSILON/16.0;
  a = zn_make_dn(dn_make_n(x));
  b = zn_make_dn(dn_make_n(y));
  c = zn_sub(a, b);
  c = zn_add(c, zn_negate(a));
  if (y != -dn_to_n(zn_real(c))) {
    printf("Sub/negate failed: %20.17e != %20.17e\n", y, (double)dn_to_n(zn_real(c)));
    ++nerrs;
  }
}

static void
test_mul (void)
{
  struct zn a, b, c;
  long double tst;

  a = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  b = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  c = zn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = zn_sub(c, a); /* eps + eps**2 */
  c = zn_sub(c, zn_make_n(LDBL_EPSILON)); /* eps**2 */
  tst = LDBL_EPSILON*LDBL_EPSILON;
  if (tst != dn_to_n(zn_real(c))) {
    printf("Mul/sub failed: %20.17e != %20.17e\n", (double)tst,
	   (double)dn_to_n(zn_real(c)));
    ++nerrs;
  }
}

static void
test_sqrt (void)
{
  struct zn a, b, c;

  a = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  b = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  c = zn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = zn_sqrt(c); /* 1+eps */
  c = zn_sub(c, a);
  if (dn_ge(zn_abs(c), dn_eps())) {
    printf("Sqrt/sub failed (%e)\n", (double)dn_to_n(dn_eps()));
    printf("  %e %e ; %e %e\n",
	   (double)dn_head_(zn_real(c)),
	   (double)dn_tail_(zn_real(c)),
	   (double)dn_head_(zn_imag(c)),
	   (double)dn_tail_(zn_imag(c)));
    ++nerrs;
  }
}

static void
test_div (void)
{
  struct zn a, b, c;

  a = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  b = zn_make_dn(dn_make(1.0L, LDBL_EPSILON));
  c = zn_mul(a, b); /* 1 + 2eps + eps**2 */
  c = zn_div(c, a); /* 1+eps */
  c = zn_sub(c, b);
  if (!zn_iszero(c)) {
    printf("div/sub failed\n");
    printf("  %e %e ; %e %e\n",
	   (double)dn_head_(zn_real(c)),
	   (double)dn_tail_(zn_real(c)),
	   (double)dn_head_(zn_imag(c)),
	   (double)dn_tail_(zn_imag(c)));
    ++nerrs;
  }
}

static void
test_cmp (void)
{
  struct zn a, b;

  a = zn_make_dn(dn_make(1.0, LDBL_EPSILON));
  b = zn_make_n(1.0);
  if (!zn_ne(b, a)) {
    printf("zn_ne failed\n");
    ++nerrs;
  }
  b = zn_add_n(b, LDBL_EPSILON);
  if (!zn_eq(a, b)) {
    printf("zn_eq failed\n");
    ++nerrs;
  }
}

int
main (void)
{
  test_add();
  test_mul();
  test_sqrt();
  test_div();
  test_sub();
  test_cmp();
  return nerrs;
}
