# -*- Makefile -*- Copyright 2007 The Regents of the University of California.
# All rights reserved.  See COPYING for license.

zn_arith_inc=zn-arith.h
zn_arith_src=zn-arith-impl.c

zn_blas_inc=zn-blas.h
zn_blas_src=zn-dot.c zn-scal.c zn-gemm.c zn-gemv.c zn-ger.c zn-herk.c	\
	zn-trsv.c zn-trsm.c zn-iamax.c zn-geamv.c zn-geamm.c

zn_lapack_inc=zn-lapack.h
zn_lapack_src=zn-potrf.c zn-laswp.c zn-getrf.c zn-getrs.c zn-getri.c

zn_inc=$(zn_arith_inc) $(zn_blas_inc) $(zn_lapack_inc)
zn_src=$(zn_arith_src) $(zn_blas_src) $(zn_lapack_src)
