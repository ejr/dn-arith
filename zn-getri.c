/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include <string.h>

#include <assert.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

void
DN_M(zn_getri) (const int n, 
		struct zn * restrict LU, const int ldLU,
		const int * restrict ipiv,
		int *info)
{
    int i, j;
    struct zn *Ainv = NULL;
    const int ldAinv = n;

    *info = 0; /* XXX: should check args. */

    Ainv = calloc (n * n, sizeof(*Ainv));
    if (!Ainv) { *info = -1; return; }

    for (i = 0; i < n; ++i)
	AREF(Ainv,i,i) = zn_one();

    /* Invert U by solving U Ainv = I*/
    zn_trsm("Left", "Upper", "No trans", "Non-unit", n, n,
	    zn_one(), LU, ldLU, Ainv, n);

    /* Solve Ainv L = inv(U) */
    zn_trsm("Right", "Lower", "No trans", "Unit", n, n,
	    zn_one(), LU, ldLU, Ainv, n);

    /* Permute columns */
    for (j = n-2; j >= 0; --j) {
	const int jp = ipiv[j];
	if (jp != j) {
	    for (i = 0; i < n; ++i) {
		const struct zn tmp = AREF(Ainv,i,j);
		AREF(Ainv,i,j) = AREF(Ainv,i,jp);
		AREF(Ainv,i,jp) = tmp;
	    }
	}
    }

    /* Copy back */
    for (j = 0; j < n; ++j)
	memcpy (&AREF(LU,0,j), &AREF(Ainv,0,j), n*sizeof(*Ainv));

    free (Ainv);
}

void
DN_M(zn_getri_f) (const int *n, 
		  struct zn * restrict LU, const int *ldLU,
		  const int * restrict ipiv,
		  int *info)
{
    zn_getri(*n, LU, *ldLU, ipiv, info);
}
