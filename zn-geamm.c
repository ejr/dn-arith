/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

#define DIMTHRESH 8

typedef void (*geammfn) (const int M, const int N, const int K,
			 const struct dn alpha,
			 const struct zn * restrict a, const int lda,
			 const struct zn * restrict b, const int ldb,
			 struct dn * restrict c, const int ldc);

static void
geamm_NN (const int M, const int N, const int K,
	  const struct dn alpha,
	  const struct zn * restrict a, const int lda,
	  const struct zn * restrict b, const int ldb,
	  struct dn * restrict c, const int ldc)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct dn tmp = zn_asumprod_strided_(K, &AREF(a,i,0), lda,
					   &AREF(b,0,j), 1);
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(c,i,j) = dn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgeamm_NN (const int M, const int N, const int K,
	     const struct dn alpha,
	     const struct zn * restrict a, const int lda,
	     const struct zn * restrict b, const int ldb,
	     struct dn * restrict c, const int ldc)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is MxK, B is KxN, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    geamm_NN (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_NN (mlow, N, K, alpha, a, lda, b, ldb, c, ldc);
#pragma omp section
      recgeamm_NN (mhigh, N, K, alpha, a+mlow, lda, b, ldb, c+mlow, ldc);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_NN (M, nlow, K, alpha, a, lda, b, ldb, c, ldc);
#pragma omp section
      recgeamm_NN (M, nhigh, K, alpha, a, lda, b + nlow*ldb, ldb,
		   c + nlow*ldc, ldc);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgeamm_NN (M, N, klow, alpha, a, lda, b, ldb, c, ldc);
    recgeamm_NN (M, N, khigh, alpha, a + klow*lda, lda, b + klow, ldb, c, ldc);
  }
}

static void
geamm_TN (const int M, const int N, const int K,
	  const struct dn alpha,
	  const struct zn * restrict a, const int lda,
	  const struct zn * restrict b, const int ldb,
	  struct dn * restrict c, const int ldc)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct dn tmp = zn_asumprod_(K, &AREF(a,0,i), &AREF(b,0,j));
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(c,i,j) = dn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgeamm_TN (const int M, const int N, const int K,
	     const struct dn alpha,
	     const struct zn * restrict a, const int lda,
	     const struct zn * restrict b, const int ldb,
	     struct dn * restrict c, const int ldc,
	     geammfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is KxM, B is KxN, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_TN (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_TN (mhigh, N, K, alpha, a+mlow*lda, lda, b, ldb, c+mlow, ldc,
		   fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_TN (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_TN (M, nhigh, K, alpha, a, lda, b + nlow*ldb, ldb,
		   c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgeamm_TN (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgeamm_TN (M, N, khigh, alpha, a + klow, lda, b + klow, ldb, c, ldc, fn);
  }
}

static void
geamm_NT (const int M, const int N, const int K,
	  const struct dn alpha,
	  const struct zn * restrict a, const int lda,
	  const struct zn * restrict b, const int ldb,
	  struct dn * restrict c, const int ldc)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct dn tmp = zn_asumprod_strided_(K, &AREF(a,i,0), lda,
					   &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(c,i,j) = dn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgeamm_NT (const int M, const int N, const int K,
	     const struct dn alpha,
	     const struct zn * restrict a, const int lda,
	     const struct zn * restrict b, const int ldb,
	     struct dn * restrict c, const int ldc,
	     geammfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is MxK, B is NxK, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_NT (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_NT (mhigh, N, K, alpha, a+mlow, lda, b, ldb, c+mlow, ldc, fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_NT (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_NT (M, nhigh, K, alpha, a, lda, b + nlow, ldb,
		   c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgeamm_NT (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgeamm_NT (M, N, khigh, alpha, a + klow*lda, lda, b + klow*ldb, ldb,
		 c, ldc, fn);
  }
}

static void
geamm_TT (const int M, const int N, const int K,
	  const struct dn alpha,
	  const struct zn * restrict a, const int lda,
	  const struct zn * restrict b, const int ldb,
	  struct dn * restrict c, const int ldc)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i < M; ++i) {
      struct dn tmp = zn_asumprod_strided_(K, &AREF(a,0,i), 1,
					   &AREF(b,j,0), ldb);
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(c,i,j) = dn_add(AREF(c,i,j), tmp);
    }
  }
}

static void
recgeamm_TT (const int M, const int N, const int K,
	     const struct dn alpha,
	     const struct zn * restrict a, const int lda,
	     const struct zn * restrict b, const int ldb,
	     struct dn * restrict c, const int ldc,
	     geammfn fn)
{
  const int lrgdim = (M > N? M : (N > K? N : K));
  /* A is KxM, B is NxK, C is MxN */

  if (M < DIMTHRESH && N < DIMTHRESH && K < DIMTHRESH) {
    fn (M, N, K, alpha, a, lda, b, ldb, c, ldc);
    return;
  }

  if (M == lrgdim) {
    const int mlow = M >> 1;
    const int mhigh = M - mlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_TT (mlow, N, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_TT (mhigh, N, K, alpha, a+mlow*lda, lda, b, ldb, c+mlow, ldc,
		   fn);
    }
  }
  else if (N == lrgdim) {
    const int nlow = N >> 1;
    const int nhigh = N - nlow;
#pragma omp parallel sections
    {
#pragma omp section
      recgeamm_TT (M, nlow, K, alpha, a, lda, b, ldb, c, ldc, fn);
#pragma omp section
      recgeamm_TT (M, nhigh, K, alpha, a, lda, b + nlow, ldb,
		   c + nlow*ldc, ldc, fn);
    }
  }
  else {
    const int klow = K >> 1;
    const int khigh = K - klow;
    recgeamm_TT (M, N, klow, alpha, a, lda, b, ldb, c, ldc, fn);
    recgeamm_TT (M, N, khigh, alpha, a + klow, lda, b + klow*ldb, ldb,
		 c, ldc, fn);
  }
}

void
DN_M(zn_geamm)(const char *transa, const char *transb,
	       const int m, const int n, const int k,
	       const struct dn alpha,
	       const struct zn * restrict a, const int lda,
	       const struct zn * restrict b, const int ldb,
	       const struct dn beta,
	       struct dn * restrict c, const int ldc)
{
#pragma omp parallel
  if (dn_isone(beta)) {
#pragma omp for nowait
    for (int j = 0; j < n; ++j)
      for (int i = 0; i < m; ++i)
	AREF(c,i,j) = dn_abs(AREF(c,i,j));
  } else if (dn_iszero(beta)) {
#pragma omp for nowait
    for (int j = 0; j < n; ++j)
      for (int i = 0; i < m; ++i)
	AREF(c,i,j) = dn_zero();
  } else if (dn_isnegone(beta)) {
#pragma omp for nowait
    for (int j = 0; j < n; ++j)
      for (int i = 0; i < m; ++i)
	AREF(c,i,j) = dn_negate(dn_abs(AREF(c,i,j)));
  } else {
#pragma omp for nowait
    for (int j = 0; j < n; ++j)
      for (int i = 0; i < m; ++i)
	AREF(c,i,j) = dn_mul(dn_abs(AREF(c,i,j)), beta);
  }

  if (dn_iszero(alpha)) return;

  if (tolower(*transa) == 'n' && tolower(*transb) == 'n')
    recgeamm_NN(m, n, k, alpha, a, lda, b, ldb, c, ldc);
  else if (tolower(*transa) == 'n' && tolower(*transb) == 't')
    recgeamm_NT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_NT);
  else if (tolower(*transa) == 'n' && tolower(*transb) == 'c')
    recgeamm_NT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_NT);
  else if (tolower(*transa) == 't' && tolower(*transb) == 'n')
    recgeamm_TN(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TN);
  else if (tolower(*transa) == 'c' && tolower(*transb) == 'n')
    recgeamm_TN(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TN);
  else if (tolower(*transa) == 't' && tolower(*transb) == 't')
    recgeamm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TT);
  else if (tolower(*transa) == 't' && tolower(*transb) == 'c')
    recgeamm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TT);
  else if (tolower(*transa) == 'c' && tolower(*transb) == 't')
    recgeamm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TT);
  else
    recgeamm_TT(m, n, k, alpha, a, lda, b, ldb, c, ldc, geamm_TT);

}

void
DN_M(zn_geamm_f)(const char *transa, const char *transb,
		 const int *m, const int *n, const int *k,
		 const struct dn *alpha,
		 const struct zn *a, const int *lda,
		 const struct zn *b, const int *ldb,
		 const struct dn *beta,
		 struct dn *c, const int *ldc)
{
  DN_M(zn_geamm)(transa, transb, *m, *n, *k, *alpha, a, *lda,
		 b, *ldb, *beta, c, *ldc);
}
