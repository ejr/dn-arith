/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include "dn-arith.h"
#include "dn-ops-internal.h"

struct dn
dn_sqrt (const struct dn a)
{
  /* Strategy:  Use Karp's trick:  if x is an approximation
     to 1/sqrt(a), then

        sqrt(a) = a*x + [a - (a*x)^2] * x / 2   (approx)

     The approximation is accurate to twice the accuracy of x.
     Also, the multiplication (a*x) and [-]*x can be done with
     only half the precision.
  */

  if (dn_iszero(a) || dn_isone(a))
    return a;

  if (dn_head_(a) < 0.0)
    return dn_nan();

  const dn_native_t x = ((dn_native_t)1.0) / DN_N_SQRT_(dn_head_(a));
  const dn_native_t ax = dn_head_(a) * x;
  struct dn tmp = dn_sqr_n(ax);
  tmp = dn_sub(a, tmp);
  const dn_native_t tmp2 = dn_head_(tmp) * (x * (dn_native_t)0.5);
  tmp = dn_two_sum_(ax, tmp2);
  return tmp;
}

struct dn
dn_rsqrt (const struct dn a)
{
  /* Strategy, from Markstein:
    r \approx 1/sqrt(a)
    tmp = r*a - 1.0
    out = r - r*tmp
  */

  if (dn_iszero(a)) return dn_inf();// ignoring -0.
  if (dn_isone(a)) return a;

  if (dn_head_(a) < 0.0) return dn_nan();

  const dn_native_t x = ((dn_native_t)1.0) / DN_N_SQRT_(dn_head_(a));

  struct dn tmp = dn_sub_n(dn_mul_n(a, x), 1.0);
  tmp = dn_add_n(dn_mul_n(dn_negate(tmp), x), x);
  return tmp;
}

static struct dn dn_running_sum_n_ (const struct dn x, const dn_native_t y)
  DN_ATTRS_CONST_;
struct dn
dn_running_sum_n_ (const struct dn x, const dn_native_t y)
{
  /* Compensated summation without renormalization, or one step of
     Sum2s in Ogita, Rump, & Oishi. */
  struct dn pq = dn_two_sum_(dn_head_(x), y);
  dn_native_t s = dn_tail_(x) + dn_tail_(pq);
  return dn_make_(dn_head_(pq), s);
}

static struct dn dn_running_sum_ (const struct dn x, const struct dn y)
  DN_ATTRS_CONST_;
struct dn
dn_running_sum_ (const struct dn x, const struct dn y)
{
  return dn_running_sum_n_(dn_running_sum_n_(x,dn_head_(y)),dn_tail_(y));
}

struct dn
dn_running_sum_wrapped_ (const struct dn x, const struct dn y)
{
  return dn_running_sum_(x, y);
}

static struct dn dn_running_asum_ (const struct dn x, const struct dn y)
  DN_ATTRS_CONST_;
struct dn
dn_running_asum_ (const struct dn x, const struct dn y)
{
  if (dn_head_(y) < 0)
    return dn_running_sum_n_(dn_running_sum_n_(x,-dn_head_(y)),-dn_tail_(y));
  else
    return dn_running_sum_n_(dn_running_sum_n_(x,dn_head_(y)),dn_tail_(y));
}

struct dn
dn_sum_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[1]);
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, x[i]);

  return out;
}

struct dn
dn_sum_d_core_ (const size_t n, const double *x)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[1]);
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, x[i]);

  return out;
}

struct dn
dn_sumprod_n_core_ (const size_t n, const dn_native_t *x, const dn_native_t *y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(x[i], y[i]), out);

  return out;
}

struct dn
dn_sumprod_d_core_ (const size_t n, const double *x, const double *y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(x[i], y[i]), out);

  return out;
}

struct dn
dn_sumprod_core_ (const size_t n, const struct dn *x, const struct dn *y)
{
  size_t i;
  struct dn out;

  out = dn_mul(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul(x[i], y[i]), out);

  return out;
}

struct dn
dn_asum_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[1]));
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i]));

  return out;
}

struct dn
dn_asum_d_core_ (const size_t n, const double *x)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[1]));
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i]));

  return out;
}

struct dn
dn_asum_core_ (const size_t n, const struct dn *x)
{
  size_t i;
  struct dn out;

  out = x[0];
  for (i = 1; i < n; ++i)
    out = dn_running_asum_(out, x[i]);

  return out;
}

struct dn
dn_asumprod_n_core_ (const size_t n, const dn_native_t *x,
		     const dn_native_t *y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(DN_N_ABS_(x[i]), DN_N_ABS_(y[i])),
			  out);

  return out;
}

struct dn
dn_asumprod_d_core_ (const size_t n, const double *x, const double *y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(DN_N_ABS_(x[0]), DN_N_ABS_(y[0]));
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(DN_N_ABS_(x[i]), DN_N_ABS_(y[i])),
			  out);

  return out;
}

struct dn
dn_asumprod_core_ (const size_t n, const struct dn *x, const struct dn *y)
{
  size_t i;
  struct dn out;

  out = dn_abs(dn_mul(x[0], y[0]));
  for (i = 1; i < n; ++i)
    out = dn_running_asum_(out, dn_mul(x[i], y[i]));

  return out;
}

struct dn
dn_sum_strided_n_core_ (const size_t n, const dn_native_t *x,
			const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[stride]);
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, x[i*stride]);

  return out;
}

struct dn
dn_sum_strided_d_core_ (const size_t n, const double *x,
			const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[stride]);
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, x[i*stride]);

  return out;
}

struct dn
dn_sum_strided_np_core_ (const size_t n, const dn_native_t *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[1]);
  for (i = 2; i < n; ++i) {
    out = dn_running_sum_n_(out, x[i*stride]);
    out = dn_running_sum_n_(out, x[i*stride+1]);
  }

  return out;
}

struct dn
dn_sum_strided_dp_core_ (const size_t n, const double *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(x[0], x[1]);
  for (i = 2; i < n; ++i) {
    out = dn_running_sum_n_(out, x[i*stride]);
    out = dn_running_sum_n_(out, x[i*stride+1]);
  }

  return out;
}

struct dn
dn_sumprod_strided_n_core_ (const size_t n,
			    const dn_native_t *x, const size_t stride_x,
			    const dn_native_t *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(x[i*stride_x], y[i*stride_y]), out);

  return out;
}

struct dn
dn_sumprod_strided_d_core_ (const size_t n,
			    const double *x, const size_t stride_x,
			    const double *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(x[i*stride_x], y[i*stride_y]), out);

  return out;
}

struct dn
dn_sumprod_strided_core_ (const size_t n,
			    const struct dn *x, const size_t stride_x,
			    const struct dn *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_mul(x[0], y[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul(x[i*stride_x], y[i*stride_y]), out);

  return out;
}

struct dn
dn_asum_strided_core_ (const size_t n, const struct dn *x,
		       const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_abs(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_asum_(out, x[i*stride]);

  return out;
}

struct dn
dn_asum_strided_n_core_ (const size_t n, const dn_native_t *x,
			const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[stride]));
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride]));

  return out;
}

struct dn
dn_asum_strided_d_core_ (const size_t n, const double *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[stride]));
  for (i = 2; i < n; ++i)
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride]));

  return out;
}

struct dn
dn_asumprod_strided_core_ (const size_t n,
			   const struct dn *x, const size_t stride_x,
			   const struct dn *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_abs(dn_mul(x[0], y[0]));
  for (i = 1; i < n; ++i)
    out = dn_running_asum_(out, dn_mul(x[i*stride_x], y[i*stride_y]));

  return out;
}

struct dn
dn_asumprod_strided_n_core_ (const size_t n,
			     const dn_native_t *x, const size_t stride_x,
			     const dn_native_t *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(DN_N_ABS_(x[0]), DN_N_ABS_(y[0]));
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(DN_N_ABS_(x[i*stride_x]),
				     DN_N_ABS_(y[i*stride_y])), out);

  return out;
}

struct dn
dn_asumprod_strided_d_core_ (const size_t n,
			     const double *x, const size_t stride_x,
			     const double *y, const size_t stride_y)
{
  size_t i;
  struct dn out;

  out = dn_mul_n_n(DN_N_ABS_(x[0]), DN_N_ABS_(y[0]));
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_mul_n_n(DN_N_ABS_(x[i*stride_x]),
				     DN_N_ABS_(y[i*stride_y])), out);

  return out;
}

struct dn
dn_asum_strided_np_core_ (const size_t n, const dn_native_t *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[1]));
  for (i = 2; i < n; ++i) {
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride]));
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride+1]));
  }

  return out;
}

struct dn
dn_asum_strided_dp_core_ (const size_t n, const double *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_make(DN_N_ABS_(x[0]), DN_N_ABS_(x[1]));
  for (i = 2; i < n; ++i) {
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride]));
    out = dn_running_sum_n_(out, DN_N_ABS_(x[i*stride+1]));
  }

  return out;
}

struct dn
dn_sumsqr_n_core_ (const size_t n, const dn_native_t *x)
{
  size_t i;
  struct dn out;

  out = dn_sqr_n(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr_n(x[i]), out);

  return out;
}

struct dn
dn_sumsqr_d_core_ (const size_t n, const double *x)
{
  size_t i;
  struct dn out;

  out = dn_sqr_n(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr_n(x[i]), out);

  return out;
}

struct dn
dn_sumsqr_core_ (const size_t n, const struct dn *x)
{
  size_t i;
  struct dn out;

  out = dn_sqr(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr(x[i]), out);

  return out;
}

struct dn
dn_sumsqr_strided_n_core_ (const size_t n, const dn_native_t *x,
			   const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_sqr_n(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr_n(x[i*stride]), out);

  return out;
}

struct dn
dn_sumsqr_strided_d_core_ (const size_t n, const double *x,
			   const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_sqr_n(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr_n(x[i*stride]), out);

  return out;
}

struct dn
dn_sumsqr_strided_core_ (const size_t n, const struct dn *x,
			 const size_t stride)
{
  size_t i;
  struct dn out;

  out = dn_sqr(x[0]);
  for (i = 1; i < n; ++i)
    out = dn_running_sum_(dn_sqr(x[i*stride]), out);

  return out;
}
