#include <stddef.h>

#include "config.h"

#if defined(USE_INCLUDED_GDTOA)
#include "gdtoa/gdtoa.h"
#elif defined(HAVE_GDTOA_H)
#include <gdtoa.h>
#elif defined(HAVE_GDTOA_GDTOA_H)
#include <gdtoa/gdtoa.h>
#else
#error "No gdtoa configured, must not build dn-arith-str.c."
#endif

#include "dn-arith.h"

struct dn
dn_asciitodn(const char *nptr, char **endptr)
{
  double d[2];
  (void)strtopdd(nptr, endptr, d);
  return dn_make(d[0], d[1]);
}

int
dn_asciitopdn(const char *nptr, char **endptr, struct dn *dn)
{
  double d[2];
  int err;
  err = strtopdd(nptr, endptr, d);
  *dn = dn_make(d[0], d[1]);
  return err;
}

char *
dn_asciifmt(char *buf, struct dn dn, int ndig, unsigned bufsize)
{
  double d[2];
  dn_to_dd (dn, d, d+1);
  return g_ddfmt(buf, d, ndig, bufsize);
}
