/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_geru (void)
{
  const int M = 11, N = 38;
  const int ldA = M + 4, incX = 3, incY = 8;
  struct zn *A, *X, *Y;
  int i, j;
  struct zn small = zn_make(dn_make_n(DBL_EPSILON/4096.0),
			    dn_make_n(DBL_EPSILON/4096.0));

  A = malloc(ldA*N*sizeof(*A));
  X = malloc(M*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = small;

  for (i = 0; i < M; ++i)
    VREF(X,i) = zn_one();
  for (j = 0; j < N; ++j)
    VREF(Y,j) = zn_make_n(2.0);

  zn_geru(M, N, zn_one(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (zn_ne(small, zn_sub_n(AREF(A,i,j), 2.0))) {
	printf("test_geru failed: (%d %d)\n", i, j);
	++nerrs;
      }

  zn_geru(M, N, zn_negone(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (zn_ne(small, AREF(A,i,j))) {
	printf("test_geru - old failed: (%d %d)\n", i, j);
	++nerrs;
      }

  free(Y);
  free(X);
  free(A);
}

static void
test_gerc (void)
{
  const int M = 11, N = 38;
  const int ldA = M + 4, incX = 3, incY = 8;
  struct zn *A, *X, *Y;
  int i, j;
  struct zn small = zn_make(dn_make_n(DBL_EPSILON/4096.0),
			    dn_make_n(DBL_EPSILON/4096.0));

  A = malloc(ldA*N*sizeof(*A));
  X = malloc(M*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = small;

  for (i = 0; i < M; ++i)
    VREF(X,i) = zn_one();
  for (j = 0; j < N; ++j)
    VREF(Y,j) = zn_make_im_n(2.0);

  zn_gerc(M, N, zn_I(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (zn_ne(small, zn_sub_n(AREF(A,i,j), 2.0))) {
	printf("test_gerc failed: (%d %d)\n", i, j);
	++nerrs;
      }

  zn_gerc(M, N, zn_negI(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (zn_ne(small, AREF(A,i,j))) {
	printf("test_gerc - old failed: (%d %d)\n", i, j);
	++nerrs;
      }

  free(Y);
  free(X);
  free(A);
}

int
main (void)
{
  test_geru();
  test_gerc();
  return nerrs;
}
