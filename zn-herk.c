/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

/*
  uplo == U
    trans = N : C := alpha * A * conj(A') + beta * C
    trans = T|C : C := alpha * conj(A') * A + beta * C
  uplo == L
    trans = N : C := alpha * A * A' + beta * C
    trans = T|C : C := alpha * conj(A') * A + beta * C
*/

static void
herk_UN (const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict C, const int ldC)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i <= j; ++i) {
      struct zn tmp = zn_sumprod_strided_nc_(K, &AREF(A,i,0), ldA,
					     &AREF(A,j,0), ldA);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(C,i,j) = zn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
herk_UC (const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict C, const int ldC)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i <= j; ++i) {
      struct zn tmp = zn_sumprod_nc_(K, &AREF(A,0,j), &AREF(A,0,i));
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(C,i,j) = zn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
herk_LN (const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict C, const int ldC)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = j; i < N; ++i) {
      struct zn tmp = zn_sumprod_strided_nc_(K, &AREF(A,i,0), ldA,
					     &AREF(A,j,0), ldA);
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(C,i,j) = zn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
herk_LC (const int N, const int K,
	 const struct zn alpha,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict C, const int ldC)
{
  const _Bool one_alpha = zn_isone(alpha);
  const _Bool negone_alpha = zn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = j; i < N; ++i) {
      struct zn tmp = zn_sumprod_nc_(K, &AREF(A,0,j), &AREF(A,0,i));
      if (negone_alpha)
	tmp = zn_negate(tmp);
      else if (!one_alpha)
	tmp = zn_mul(tmp, alpha);
      AREF(C,i,j) = zn_add(AREF(C,i,j), tmp);
    }
  }
}


void
DN_M(zn_herk)(const char *uplo, const char *trans, const int N,
	      const int K,
	      const struct zn alpha,
	      const struct zn * restrict A, const int ldA,
	      const struct zn beta,
	      struct zn * restrict C, const int ldC)
{
#pragma omp parallel
  if (!zn_isone(beta)) {
    if (zn_iszero(beta)) {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = zn_zero();
    } else if (zn_isnegone(beta)) {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = zn_negate(AREF(C,i,j));
    } else {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = zn_mul(AREF(C,i,j), beta);
    }
  }

  if (zn_iszero(alpha)) return;

  if (tolower(*uplo) == 'u') {
    if (tolower(*trans) == 'n')
      herk_UN(N, K, alpha, A, ldA, C, ldC);
    else
      herk_UC(N, K, alpha, A, ldA, C, ldC);
  }
  else {
    if (tolower(*trans) == 'n')
      herk_LN(N, K, alpha, A, ldA, C, ldC);
    else
      herk_LC(N, K, alpha, A, ldA, C, ldC);
  }
}

void
DN_M(zn_herk_f)(const char *uplo, const char *trans, const int *N,
	      const int *K,
	      const struct zn *alpha,
	      const struct zn *A, const int *ldA,
	      const struct zn *beta,
	      struct zn *C, const int *ldC)
{
  DN_M(zn_herk)(uplo, trans, *N, *K, *alpha, A, *ldA, *beta, C, *ldC);
}

