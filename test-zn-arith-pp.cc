/* -*- C++ -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <math.h>

#include <limits>
#include <iostream>

using namespace std;

#include "zn-arith"
#include "dn-arith-io"
#include "zn-arith-io"

using namespace dn_ns;

int nerrs = 0;

static void
test_add (void)
{
  struct zn a, b, c;
  double x, y;

  x = 1.0;
  y = numeric_limits<dn_native_t>::epsilon()/2.0;
  a = zn_make(x);
  b = zn_make(y);
  c = a + b;
  c -= a;
  if (y != dn_to_n(zn_real(c))) {
    cout << "Add/sub failed: " << y << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_sub (void)
{
  struct zn a, b, c;
  double x, y;

  x = 1.0;
  y = numeric_limits<dn_native_t>::epsilon()/16.0;
  a = zn_make(x);
  b = zn_make(y);
  c = a - b;
  c += -a;
  if (y != -dn_to_n(zn_real(c))) {
    cout << "Sub/negate failed: " << y << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_mul (void)
{
  struct zn a, b, c;
  long double tst;

  a = zn_make(dn_make(1.0, numeric_limits<dn_native_t>::epsilon()));
  b = zn_make(dn_make(1.0, numeric_limits<dn_native_t>::epsilon()));
  c = a * b; /* 1 + 2eps + eps**2 */
  c -= a; /* eps + eps**2 */
  c = c -  zn_make(numeric_limits<dn_native_t>::epsilon()); /* eps**2 */
  tst = numeric_limits<dn_native_t>::epsilon();
  tst *= tst;
  if (tst != dn_to_n(zn_real(c))) {
    cout << "Mul/sub failed: " << tst << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_sqrt (void)
{
  struct zn a, b, c;

  a = zn_make(dn_make(1.0, numeric_limits<dn_native_t>::epsilon()));
  b = zn_make(dn_make(1.0, numeric_limits<dn_native_t>::epsilon()));
  c = a * b; /* 1 + 2eps + eps**2 */
  c = sqrt(c); /* 1+eps */
  c -= a;
  if (abs(c) >  dn_eps()) {
    cout << "Sqrt/sub failed: abs( " << c << " ) = "
	 << dn_eps() << endl;
    ++nerrs;
  }
}

static void
test_div (void)
{
  struct zn a, b, c;

  a = zn_make(dn_make(1.0L, numeric_limits<dn_native_t>::epsilon()));
  b = zn_make(dn_make(1.0L, numeric_limits<dn_native_t>::epsilon()));
  c = a * b; /* 1 + 2eps + eps**2 */
  c /= a; /* 1+eps */
  c -= b;
  if (!iszero(c)) {
    cout << "Div/sub failed: abs(" << c << ") != 0\n";
    ++nerrs;
  }
}

static void
test_cmp (void)
{
  struct zn a, b;

  a = zn_make(dn_make(1.0, numeric_limits<dn_native_t>::epsilon()));
  b = zn_make(1.0);
  if (!(b != a)) {
    cout << "zn_ne failed\n";
    ++nerrs;
  }
  b = b + numeric_limits<dn_native_t>::epsilon();
  if (!(a == b)) {
    cout << "zn_eq failed\n";
    ++nerrs;
  }
}

int
main (void)
{
  test_add();
  test_mul();
  test_sqrt();
  test_div();
  test_sub();
  test_cmp();
  return nerrs;
}
