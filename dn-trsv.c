/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdio.h>
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

enum diag { UNIT, DEFINED };

static void
trsv_UN (const enum diag diag, const int N,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    struct dn tmp = dn_sumprod_strided_(N-1-j, &AREF(A,j,j+1), ldA,
					&VREF(X,j+1), incX);
    const _Bool nonunit = diag != UNIT && !dn_isone(AREF(A,j,j));
    tmp = dn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = dn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LN (const enum diag diag, const int N,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    struct dn tmp = dn_sumprod_strided_(j, &AREF(A,j,0), ldA,
					&VREF(X,0), incX);
    const _Bool nonunit = diag != UNIT && !dn_isone(AREF(A,j,j));
    tmp = dn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = dn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_UT (const enum diag diag, const int N,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    const _Bool nonunit = diag != UNIT && !dn_isone(AREF(A,j,j));
    struct dn tmp;
    if (1 == incX)
      tmp = dn_sumprod_(j, &AREF(A,0,j), &VREF(X,0));
    else
      tmp = dn_sumprod_strided_(j, &AREF(A,0,j), 1, &VREF(X,0), incX);
    tmp = dn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = dn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LT (const enum diag diag, const int N,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    const _Bool nonunit = diag != UNIT && !dn_isone(AREF(A,j,j));
    struct dn tmp = dn_sumprod_strided_(N-1-j, &AREF(A,j,j+1), ldA,
					&VREF(X,j+1), incX);
    tmp = dn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = dn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

void
DN_M(dn_trsv)(const char *uplo, const char *trans, const char *diag,
	      const int N,
	      const struct dn * restrict A, const int ldA,
	      struct dn * restrict X, const int incX)
{
  enum diag d;

  if (tolower(*diag) == 'u')
    d = UNIT;
  else
    d = DEFINED;

  if (tolower(*uplo) == 'u') {
    if (tolower(*trans) == 'n')
      trsv_UN(d, N, A, ldA, X, incX);
    else
      trsv_UT(d, N, A, ldA, X, incX);
  }
  else {
    if (tolower(*trans) == 'n')
      trsv_LN(d, N, A, ldA, X, incX);
    else
      trsv_LT(d, N, A, ldA, X, incX);
  }
}

void
DN_M(dn_trsv_f)(const char *uplo, const char *trans, const char *diag,
		const int *N,
		const struct dn *A, const int *ldA,
		struct dn *X, const int *incX)
{
  DN_M(dn_trsv)(uplo, trans, diag, *N, A, *ldA, X, *incX);
}
