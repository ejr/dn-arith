/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdio.h>
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

enum diag { UNIT, DEFINED };

static void
trsv_UN (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    struct zn tmp = zn_sumprod_strided_(N-1-j, &AREF(A,j,j+1), ldA,
					&VREF(X,j+1), incX);
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = zn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LN (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    struct zn tmp = zn_sumprod_strided_(j, &AREF(A,j,0), ldA,
					&VREF(X,0), incX);
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = zn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LNC (const enum diag diag, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    struct zn tmp = zn_sumprod_strided_nc_(j, &VREF(X,0), incX,
					   &AREF(A,j,0), ldA);
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = zn_div(tmp, zn_conj(AREF(A,j,j)));
    VREF(X,j) = tmp;
  }
}

static void
trsv_UNC (const enum diag diag, const int N,
	  const struct zn * restrict A, const int ldA,
	  struct zn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    struct zn tmp = zn_sumprod_strided_nc_(N-1-j, &VREF(X,j+1), incX,
					   &AREF(A,j,j+1), ldA);
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit)
      tmp = zn_div(tmp, zn_conj(AREF(A,j,j)));
    VREF(X,j) = tmp;
  }
}

static void
trsv_UT (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    struct zn tmp;
    if (1 == incX)
      tmp = zn_sumprod_(j, &AREF(A,0,j), &VREF(X,0));
    else
      tmp = zn_sumprod_strided_(j, &AREF(A,0,j), 1, &VREF(X,0), incX);
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = zn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LT (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    struct zn tmp = zn_sumprod_strided_(N-1-j, &AREF(A,j,j+1), ldA,
					&VREF(X,j+1), incX);
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = zn_div(tmp, AREF(A,j,j));
    VREF(X,j) = tmp;
  }
}

static void
trsv_UC (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = 0; j < N; ++j) {
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    struct zn tmp;
    if (1 == incX)
      tmp = zn_sumprod_nc_(j, &VREF(X,0), &AREF(A,0,j));
    else
      tmp = zn_sumprod_strided_nc_(j, &VREF(X,0), incX, &AREF(A,0,j), 1);
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = zn_div(tmp, zn_conj(AREF(A,j,j)));
    VREF(X,j) = tmp;
  }
}

static void
trsv_LC (const enum diag diag, const int N,
	 const struct zn * restrict A, const int ldA,
	 struct zn * restrict X, const int incX)
{
  for (int j = N-1; j >= 0; --j) {
    const _Bool nonunit = diag != UNIT && !zn_isone(AREF(A,j,j));
    struct zn tmp = zn_sumprod_strided_nc_(N-1-j, &VREF(X,j+1), incX,
					   &AREF(A,j,j+1), ldA);
    tmp = zn_sub(VREF(X,j), tmp);
    if (nonunit) tmp = zn_div(tmp, zn_conj(AREF(A,j,j)));
    VREF(X,j) = tmp;
  }
}

void
DN_M(zn_trsv)(const char *uplo, const char *trans, const char *diag,
	      const int N,
	      const struct zn * restrict A, const int ldA,
	      struct zn * restrict X, const int incX)
{
  enum diag d;

  if (tolower(*diag) == 'u')
    d = UNIT;
  else
    d = DEFINED;

  if (tolower(*uplo) == 'u') {
    if (tolower(*trans) == 'n')
      trsv_UN(d, N, A, ldA, X, incX);
    else if (tolower(*trans) == 't')
      trsv_UT(d, N, A, ldA, X, incX);
    else
      trsv_UC(d, N, A, ldA, X, incX);
  }
  else {
    if (tolower(*trans) == 'n')
      trsv_LN(d, N, A, ldA, X, incX);
    else if (tolower(*trans) == 't')
      trsv_LT(d, N, A, ldA, X, incX);
    else
      trsv_LC(d, N, A, ldA, X, incX);
  }
}

void
DN_M(zn_trsv_f)(const char *uplo, const char *trans, const char *diag,
		const int *N,
		const struct zn *A, const int *ldA,
		struct zn *X, const int *incX)
{
  DN_M(zn_trsv)(uplo, trans, diag, *N, A, *ldA, X, *incX);
}
