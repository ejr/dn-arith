/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <string.h>

#include <assert.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

#define zn_laswp DN_M(zn_laswp)
void zn_laswp (int, struct zn * restrict, int, int, int,
	       const int * restrict, int);

void
DN_M(zn_getrs) (const char *trans, const int n, const int nrhs,
		const struct zn * restrict LU, const int ldLU,
		const int * restrict ipiv,
		struct zn * restrict B, const int ldB,
		int *info)
{
    *info = 0; /* XXX: should check args. */
    if (tolower(*trans) == 'n') {
	zn_laswp(nrhs, B, ldB, 0, n-1, ipiv, 1);
	zn_trsm("Left", "Lower", "No trans", "Unit", n, nrhs,
		zn_one(), LU, ldLU, B, ldB);
	zn_trsm("Left", "Upper", "No trans", "Non-unit", n, nrhs,
		zn_one(), LU, ldLU, B, ldB);
    }
    else {
	zn_trsm("Left", "Upper", trans, "Non-unit", n, nrhs,
		zn_one(), LU, ldLU, B, ldB);
	zn_trsm("Left", "Lower", trans, "Unit", n, nrhs,
		zn_one(), LU, ldLU, B, ldB);
	zn_laswp(nrhs, B, ldB, 0, n-1, ipiv, -1);
    }
}

void
DN_M(zn_getrs_f) (const char *trans, const int *n, const int *nrhs,
		  const struct zn * restrict LU, const int *ldLU,
		  const int * restrict ipiv,
		  struct zn * restrict B, const int *ldB,
		  int *info)
{
    zn_getrs(trans, *n, *nrhs, LU, *ldLU, ipiv, B, *ldB, info);
}
