/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(DN_HELPERS_H_)
#define DN_HELPERS_H_

#define AREF(a, i, j) ((a)[(i) + (j)*(ld##a)])
#define VREF(x, i) ((x)[(i)*(inc##x)])

#endif /* DN_HELPERS_H_ */
