/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#if 0
#include <stdio.h>
#include <valgrind/memcheck.h>
#endif
#include <string.h>

#include <assert.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

#define dn_laswp DN_M(dn_laswp)
void dn_laswp (int, struct dn * restrict, int, int, int,
	       int * restrict, int);

void
DN_M(dn_getrf)(const int M, const int N, struct dn * restrict A, const int ldA,
	       int * restrict ipiv, int *info)
{
  const int nstep = (M < N? M : N);
  int j, kcols;

#if 0
  printf("GETRF %d %d\n", M, N);
#endif
  memset (ipiv, 0, nstep*sizeof(*ipiv));
  *info = 0;

#if defined VALGRIND_MAKE_MEM_UNDEFINED
  VALGRIND_MAKE_MEM_UNDEFINED(ipiv, nstep*sizeof(int));
#endif

  for (j = 0; j < nstep; ++j) {
    const int pind = j + dn_iamax(M-j, &AREF(A,j,j), 1);
    ipiv[j] = pind;

    /* Max number of U cols to update, also how far back to look. */
    const int kahead = (j+1) & (1+~(j+1));
    /* Where the updating trsm starts. */
    const int kstart = j + 1 - kahead;
    /* Actual number of cols to update, limited by total number of cols. */
    kcols = (kahead < nstep-(j+1))? kahead : nstep-(j+1);

#if 0
    printf("%d: kstart %d  kahead %d  kcols %d\n",
	     j, kstart, kahead, kcols);
#endif

    /* Permute just this column. */
    if (pind != j) {
      const struct dn tmp = AREF(A,pind,j);
      AREF(A,pind,j) = AREF(A,j,j);
      AREF(A,j,j) = tmp;
    }

    /* Apply pending permutations to L */
    int n_to_piv = 1;
    int istart = j;
    int jstart = j - n_to_piv;
    while (n_to_piv < kahead) {
#if 0
      printf("Permuting L in A(%d:%d, %d:%d)  (%d < %d)\n",
	       istart, j, jstart, jstart+n_to_piv-1, n_to_piv, kahead);
#endif
      dn_laswp(n_to_piv, &AREF(A,0,jstart), ldA, istart, j, ipiv, 1);
      istart -= n_to_piv;
      n_to_piv <<= 1;
      jstart -= n_to_piv;
    }

    const struct dn pv = AREF(A,j,j);
    const struct dn inv_pv = dn_inv(pv);
#if 0
    printf("piv %d %g @ %d\n", j, (double)dn_to_n(pv), pind);
#endif
    if (!dn_iszero(pv) && !dn_isnan(pv)) {
#if 0
      printf("A(%d:%d, %d) *= 1/%g;\n", j+1,M-1, j, (double)dn_to_n(pv));
#endif
      for (int i = j+1; i < M; ++i)
	AREF(A,i,j) = dn_mul(AREF(A,i,j), inv_pv);
    }
    else
      if (0 == *info)
	*info = j+1;

    if (kcols == 0) break;

    /* Permute U to match L */
    dn_laswp(kcols, &AREF(A,0,j+1), ldA, kstart, j, ipiv, 1);

#if 0
    printf("trsm A(%d:%d, %d:%d) \\ A(%d:%d, %d:%d)\n",
	     kstart, kstart+kahead-1, kstart, kstart+kahead-1,
	     kstart, kstart+kahead-1, j+1, j+1+kcols-1);
#endif
    dn_trsm("Left", "Lower", "No trans", "Unit",
	    kahead, kcols, dn_one(),
	    &AREF(A, kstart, kstart), ldA,
	    &AREF(A, kstart, j+1), ldA);

#if 0
    printf("gemm A(%d:%d, %d:%d) -= A(%d:%d, %d:%d) * A(%d:%d, %d:%d)\n",
	   j+1, j+1+M-j-1-1, j+1, j+1+kcols-1,
	   j+1, j+1+M-j-1-1, kstart, kstart+kahead-1,
	   kstart, kstart+kahead-1, j+1, j+1+kcols-1);
#endif
    dn_gemm("N","N", M-j-1, kcols, kahead,
	    dn_negone(),
	    &AREF(A, j+1, kstart), ldA,
	    &AREF(A, kstart, j+1), ldA,
	    dn_one(),
	    &AREF(A, j+1, j+1), ldA);
  }
  /* Permute remaining portions of L */
  const int npived = nstep & (1+~nstep);
  j = (nstep-1) - npived;

  while (j >= 0) {
    const int n_to_piv = (j+1) & (1+~(j+1));
#if 0
    printf("Catchup permuting L in A(%d:%d, %d:%d) %d %d\n",
	     j+1, nstep-1, j-n_to_piv+1, j, n_to_piv, nstep);
#endif
    dn_laswp(n_to_piv, &AREF(A,0,j-n_to_piv+1), ldA, j+1, nstep-1, ipiv, 1);
    j -= n_to_piv;
  }

  if (M < N) {
#if 0
    printf("Catchup: N-M = %d-%d = %d\n",
	     N, M, N-M);
    printf("trsm A(%d:%d, %d:%d) \\ A(%d:%d, %d:%d)\n",
	     0, M-1, 0, M-1,
	     0, M-1, M+kcols, M+kcols + (N-(M+kcols)-1));
#endif
    dn_laswp(N-M, &AREF(A,0,M), ldA, 0, M-1, ipiv, 1);
    dn_trsm("Left", "Lower", "No trans", "Unit",
	    M, N - M, dn_one(),
	    A, ldA,
	    &AREF(A, 0, M), ldA);
  }

#if 0
  printf("GETRF done\n");
#endif
}

void
DN_M(dn_getrf_f)(const int *M, const int *N, struct dn *A, const int *ldA,
		 int * restrict ipiv, int *info)
{
  DN_M(dn_getrf)(*M, *N, A, *ldA, ipiv, info);
  if (*info) return;
  const int ncol = *N;
  /* Adjust to 1-based indices. */
  for (int j = 0; j < ncol; ++j) {
    ++ipiv[j];
  }
}
