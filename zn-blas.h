/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(ZN_BLAS_H_)
#define ZN_BLAS_H_

#if defined(__cplusplus)
extern "C" {
#endif

#define zn_dotu DN_M(zn_dotu)
struct zn zn_dotu(const int,
		  const struct zn * DN_RESTRICT, const int,
		  const struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_dotu_f DN_M(zn_dotu_f)
struct zn zn_dotu_f(const int *,
		    const struct zn * DN_RESTRICT, const int *,
		    const struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_dotc DN_M(zn_dotc)
struct zn zn_dotc(const int,
		  const struct zn * DN_RESTRICT, const int,
		  const struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_dotc_f DN_M(zn_dotc_f)
struct zn zn_dotc_f(const int *,
		    const struct zn * DN_RESTRICT, const int *,
		    const struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_scal DN_M(zn_scal)
void zn_scal(const int,
	     const struct zn,
	     struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_scal_f DN_M(zn_scal_f)
void zn_scal_f(const int *,
	       const struct zn *,
	       struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_iamax DN_M(zn_iamax)
int zn_iamax(const int,
	     const struct zn * DN_RESTRICT,
	     const int)
  DN_ATTRS_;

#define zn_iamax_f DN_M(zn_iamax_f)
int zn_iamax_f(const int *,
	       const struct zn * DN_RESTRICT,
	       const int *)
  DN_ATTRS_;

#define zn_gemv DN_M(zn_gemv)
void zn_gemv(const char *, const int, const int,
	     const struct zn,
	     const struct zn * DN_RESTRICT, const int,
	     const struct zn * DN_RESTRICT, const int,
	     const struct zn,
	     struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_gemv_f DN_M(zn_gemv_f)
void zn_gemv_f(const char *, const int *, const int *,
	       const struct zn *,
	       const struct zn * DN_RESTRICT, const int *,
	       const struct zn * DN_RESTRICT, const int *,
	       const struct zn *,
	       struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_geamv DN_M(zn_geamv)
void zn_geamv(const char *, const int, const int,
	      const struct dn,
	      const struct zn * DN_RESTRICT, const int,
	      const struct zn * DN_RESTRICT, const int,
	      const struct dn,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_geamv_f DN_M(zn_geamv_f)
void zn_geamv_f(const char *, const int *, const int *,
		const struct dn *,
		const struct zn * DN_RESTRICT, const int *,
		const struct zn * DN_RESTRICT, const int *,
		const struct dn *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_gemm DN_M(zn_gemm)
void zn_gemm (const char *, const char *,
	      const int, const int, const int,
	      const struct zn,
	      const struct zn * DN_RESTRICT, const int,
	      const struct zn * DN_RESTRICT, const int,
	      const struct zn,
	      struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_gemm_f DN_M(zn_gemm_f)
void zn_gemm_f (const char *, const char *,
		const int *, const int *, const int *,
		const struct zn *,
		const struct zn * DN_RESTRICT, const int *,
		const struct zn * DN_RESTRICT, const int *,
		const struct zn *,
		struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_geamm DN_M(zn_geamm)
void zn_geamm (const char *, const char *,
	       const int, const int, const int,
	       const struct dn,
	       const struct zn * DN_RESTRICT, const int,
	       const struct zn * DN_RESTRICT, const int,
	       const struct dn,
	       struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_geamm_f DN_M(zn_geamm_f)
void zn_geamm_f (const char *, const char *,
		 const int *, const int *, const int *,
		 const struct dn *,
		 const struct zn * DN_RESTRICT, const int *,
		 const struct zn * DN_RESTRICT, const int *,
		 const struct dn *,
		 struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_geru DN_M(zn_geru)
void zn_geru(const int, const int,
	     const struct zn,
	     const struct zn * DN_RESTRICT, const int,
	     const struct zn * DN_RESTRICT, const int,
	     struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_geru_f DN_M(zn_geru_f)
void zn_geru_f(const int *, const int *,
	       const struct zn *,
	       const struct zn * DN_RESTRICT, const int *,
	       const struct zn * DN_RESTRICT, const int *,
	       struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_gerc DN_M(zn_gerc)
void zn_gerc(const int, const int,
	     const struct zn,
	     const struct zn * DN_RESTRICT, const int,
	     const struct zn * DN_RESTRICT, const int,
	     struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_gerc_f DN_M(zn_gerc_f)
void zn_gerc_f(const int *, const int *,
	       const struct zn *,
	       const struct zn * DN_RESTRICT, const int *,
	       const struct zn * DN_RESTRICT, const int *,
	       struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_herk DN_M(zn_herk)
void zn_herk(const char *, const char *, const int,
	     const int,
	     const struct zn,
	     const struct zn * DN_RESTRICT, const int,
	     const struct zn,
	     struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_herk_f DN_M(zn_herk_f)
void zn_herk_f(const char *, const char *, const int *,
	       const int *,
	       const struct zn *,
	       const struct zn * DN_RESTRICT, const int *,
	       const struct zn *,
	       struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_trsv DN_M(zn_trsv)
void zn_trsv (const char *, const char *, const char *,
	      const int,
	      const struct zn * DN_RESTRICT, const int,
	      struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_trsv_f DN_M(zn_trsv_f)
void zn_trsv_f (const char *, const char *, const char *,
		const int *,
		const struct zn * DN_RESTRICT, const int *,
		struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define zn_trsm DN_M(zn_trsm)
void zn_trsm (const char *, const char *,
	      const char *, const char *,
	      const int,
	      const int,
	      const struct zn,
	      const struct zn * DN_RESTRICT, const int,
	      struct zn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define zn_trsm_f DN_M(zn_trsm_f)
void zn_trsm_f (const char *, const char *,
		const char *, const char *,
		const int *,
		const int *,
		const struct zn *,
		const struct zn * DN_RESTRICT, const int *,
		struct zn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#if defined(__cplusplus)
}
#endif

#endif /* ZN_BLAS_H_ */
