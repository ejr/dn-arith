/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(ZN_LAPACK_H_)
#define ZN_LAPACK_H_

#if defined(__cplusplus)
extern "C" {
#endif

#define zn_potf2 DN_M(zn_potf2)
void zn_potf2(const char *, const int,
	      struct zn * DN_RESTRICT, const int,
	      int *)
  DN_ATTRS_;

#define zn_potf2_f DN_M(zn_potf2_f)
void zn_potf2_f(const char *, const int *,
		struct zn * DN_RESTRICT, const int *,
		int *)
  DN_ATTRS_;

#define zn_potrf DN_M(zn_potrf)
void zn_potrf(const char *, const int,
	      struct zn * DN_RESTRICT, const int,
	      int *)
  DN_ATTRS_;

#define zn_potrf_f DN_M(zn_potrf_f)
void zn_potrf_f(const char *, const int *,
		struct zn * DN_RESTRICT, const int *,
		int *)
  DN_ATTRS_;

#define zn_laswp DN_M(zn_laswp)
void zn_laswp (int, struct zn * DN_RESTRICT, int, int, int,
	       const int * DN_RESTRICT, int);

#define zn_laswp_f DN_M(zn_laswp_f)
void zn_laswp_f (int *, struct zn * DN_RESTRICT, int, int, int,
		 int * DN_RESTRICT, int);

#define zn_getrf DN_M(zn_getrf)
void zn_getrf(const int, const int,
	      struct zn * DN_RESTRICT, const int,
	      int * DN_RESTRICT, int *)
  DN_ATTRS_;

#define zn_getrf_f DN_M(zn_getrf_f)
void zn_getrf_f(const int *, const int *,
		struct zn * DN_RESTRICT, const int *,
		int * DN_RESTRICT, int *)
  DN_ATTRS_;

#define zn_getrs DN_M(zn_getrs)
void zn_getrs (const char *, const int, const int,
	       const struct zn * DN_RESTRICT, const int,
	       const int * DN_RESTRICT,
	       struct zn * DN_RESTRICT, const int,
	       int *)
  DN_ATTRS_;

#define zn_getrs_f DN_M(zn_getrs_f)
void zn_getrs_f (const char *, const int *, const int *,
		 const struct zn * DN_RESTRICT, const int *,
		 const int * DN_RESTRICT,
		 struct zn * DN_RESTRICT, const int *,
		 int *)
  DN_ATTRS_;

#define zn_getri DN_M(zn_getri)
void zn_getri (const int,
	       struct zn * DN_RESTRICT, const int,
	       const int * DN_RESTRICT,
	       int *)
  DN_ATTRS_;

#define zn_getri_f DN_M(zn_getri_f)
void zn_getri_f (const int *,
		 struct zn * DN_RESTRICT, const int *,
		 const int * DN_RESTRICT,
		 int *)
  DN_ATTRS_;

#if defined(__cplusplus)
}
#endif

#endif /* ZN_LAPACK_H_ */
