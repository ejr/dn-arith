#include "config.h"

#include <math.h>

#include <iostream>

#include "zn-arith"

#include "dn-arith-io"
#include "zn-arith-io"

std::ostream &
operator<<(std::ostream &os, struct ::zn x)
{
     std::ostream::sentry oss(os);
     if (!bool(oss)) return os;

     os << '(' << dn_ns::real(x) << ',' << dn_ns::imag(x) << ')';

     return os;
}

std::istream &
operator>>(std::istream &is, struct ::zn &x)
{
     std::istream::sentry iss(is);
     if (!bool(iss)) return is;

     struct ::dn re, im;
     char c;
     is >> c;
     if (c == '(') {
	  is >> re >> c;
	  if (c == ',') {
	       is >> im >> c;
	       if (c == ')')
		    x = zn_make(re, im);
	       else
		    is.setstate(is.failbit);
	  } else if (c == ')')
	       x = zn_make(re);
	  else
	       is.setstate(is.failbit);
     } else {
	  is.putback(c);
	  is >> re;
	  x = zn_make(re);
     }
     return is;

}
