/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"
#include "zn-lapack.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct zn *A;
  int i, j, info;
  int *ipiv;

  A = calloc(ldA*N,sizeof(*A));
  ipiv = calloc(N, sizeof(*ipiv));

  for (j = 0; j < N; ++j)
    AREF(A,j,j) = zn_one();

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getri: getrf ident utterly failed! info %d\n", info);
    ++nerrs;
    return;
  }

  zn_getri(N, A, ldA, ipiv, &info);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == j) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("getri ident diag: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("getri ident non-diag: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(ipiv);
  free(A);
}

static void
test_upsidedown_ident (void)
{
  const int N = 14, ldA = N + 8;
  struct zn *A;
  int i, j, info;
  int *ipiv;

  A = calloc(ldA*N,sizeof(*A));
  ipiv = calloc(N, sizeof(*ipiv));

  for (j = 0; j < N; ++j)
    AREF(A,N-j-1,j) = zn_one();

  zn_getrf(N, N, A, ldA, ipiv, &info);
  if (info) {
    printf("getri: getrf upsidedown ident utterly failed! info %d\n", info);
    ++nerrs;
  }

  zn_getri(N, A, ldA, ipiv, &info);
  if (info) {
    printf("getri: upsidedown ident utterly failed! info %d\n", info);
    ++nerrs;
  }

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      if (i == N-j-1) {
	if (zn_ne(zn_one(), AREF(A,i,j))) {
	  printf("getri upsidedown ident anti-diag: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }	else {
	if (zn_ne(zn_zero(), AREF(A,i,j))) {
	  printf("getri upsidedown ident non-anti-diag: (%d %d)\n", i, j);
	  ++nerrs;
	}
      }

  free(ipiv);
  free(A);
}

static void
test_with_gemm (void)
{
  const int N = 17, M = N, ldA = N, ldEye = N, ldAinv = N;
  struct zn *A, *Eye, *Ainv;
  struct dn epserr = dn_mul_n_n(N*N*N*DBL_EPSILON, DBL_EPSILON);
  int i, j, info, *ipiv, *permed;

  A = malloc(ldA*N*sizeof(*A));
  Eye = calloc(ldEye*N, sizeof(*Eye));
  Ainv = malloc(ldAinv*N*sizeof(*Ainv));
  ipiv = malloc(N*sizeof(*ipiv));

  srand48(9383);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      AREF(Ainv,i,j) = AREF(A,i,j) = zn_make(
	dn_make(1.0-2.0*drand48(),
		//0.0);
		DBL_EPSILON*drand48()),
	dn_make(1.0-2.0*drand48(),
		//0.0);
		DBL_EPSILON*drand48()) );
    }

  for (j = 0; j < N; ++j) AREF(Eye,j,j) = zn_one();

  zn_getrf(N, N, Ainv, ldAinv, ipiv, &info);
  if (info) {
    printf("getri: getrf failed: %d\n", info);
    ++nerrs;
    return;
  }

  zn_getri(N, Ainv, ldAinv, ipiv, &info);
  if (info) {
    printf("getri: getri failed: %d\n", info);
    ++nerrs;
    return;
  }

  zn_gemm("No", "No", N, N, N, zn_one(), A, ldA, Ainv, ldAinv,
	  zn_negone(), Eye, ldEye);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      const struct dn X = zn_abs(AREF(Eye,i,j));
      if (dn_gt(X, epserr)) {
	struct zn foo = AREF(Eye,i,j);
	printf("getri right multiplication failed, (%d %d) %g ... %g %g\n", i, j,
	       (double)dn_to_n(X), 
	       (double)dn_to_n(zn_real(foo)),(double)dn_to_n(zn_imag(foo)) );
	++nerrs;
      }
    }

  for (j = 0; j < N; ++j) AREF(Eye,j,j) = zn_one();

  zn_gemm("No", "No", N, N, N, zn_one(), Ainv, ldAinv, A, ldA,
	  zn_negone(), Eye, ldEye);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i) {
      const struct dn X = zn_abs(AREF(Eye,i,j));
      if (dn_gt(X, epserr)) {
	printf("getri left multiplication failed, (%d %d) %g\n", i, j,
	       (double)dn_to_n(X));
	++nerrs;
      }
    }

  free(ipiv);
  free(Eye);
  free(Ainv);
  free(A);
}

int
main (void)
{
  test_ident();
  test_upsidedown_ident();
  test_with_gemm();
  return nerrs;
}
