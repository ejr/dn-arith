/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident(void)
{
  const int M = 20, N = 13, K = M;
  const int ldA = M, ldB = K, ldC = M;
  int i, j;
  struct dn *A, *B, *C;
  struct dn alpha, beta;

  A = calloc(M*K, sizeof(*A));
  B = malloc(K*N*sizeof(*A));
  C = malloc(M*N*sizeof(*A));

  srand48(3735);

  for (j = 0; j < M; ++j)
    AREF(A, j, j) = dn_one();

  for (j = 0; j < N; ++j)
    for (i = 0; i < K; ++i)
      AREF(B, i, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(C, i, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_gemm("N", "N", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (dn_ne(AREF(B,i,j), AREF(C,i,j))) {
	printf("ident*rand failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  alpha = dn_negone();
  beta = dn_one();

  dn_gemm("T", "N", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (!dn_iszero(AREF(C,i,j))) {
	printf("ident*rand - self failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  free(C);
  free(B);
  free(A);
}

static void
test_reverse(void)
{
  const int M = 20, N = 13, K = M;
  const int ldA = M, ldB = K, ldC = M;
  int i, j;
  struct dn *A, *B, *C;
  struct dn alpha, beta;

  A = calloc(M*K, sizeof(*A));
  B = malloc(K*N*sizeof(*A));
  C = malloc(M*N*sizeof(*A));

  srand48(3735);

  for (j = 0; j < M; ++j)
    AREF(A, M-j-1, j) = dn_one();

  for (j = 0; j < N; ++j)
    for (i = 0; i < K; ++i)
      AREF(B, i, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(C, i, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_gemm("N", "N", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (dn_ne(AREF(B,M-i-1,j), AREF(C,i,j))) {
	printf("ident*rand failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  alpha = dn_negone();
  beta = dn_one();

  dn_gemm("T", "N", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (!dn_iszero(AREF(C,i,j))) {
	printf("reverse*rand - self failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  free(C);
  free(B);
  free(A);
}

static void
test_reverse_tt(void)
{
  const int M = 20, N = 13, K = M;
  const int ldA = K, ldB = N, ldC = M;
  int i, j;
  struct dn *A, *B, *C;
  struct dn alpha, beta;

  A = calloc(K*M, sizeof(*A));
  B = malloc(N*K*sizeof(*A));
  C = malloc(M*N*sizeof(*A));

  srand48(3735);

  for (j = 0; j < K; ++j)
    AREF(A, K-j-1, j) = dn_one();

  for (j = 0; j < K; ++j)
    for (i = 0; i < N; ++i)
      AREF(B, i, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(C, i, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_gemm("T", "T", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (dn_ne(AREF(B,j,K-i-1), AREF(C,i,j))) {
	printf("ident*rand failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  alpha = dn_negone();
  beta = dn_one();

  dn_gemm("N", "T", M, N, K, alpha, A, ldA, B, ldB,
	  beta, C, ldC);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i) {
      if (!dn_iszero(AREF(C,i,j))) {
	printf("reverse*rand - self failed (%d,%d)\n", i, j);
	++nerrs;
      }
    }

  free(C);
  free(B);
  free(A);
}

static void
test_ident_gemv(void)
{
  const int N = 13;
  const int ldA = N+1, incX = 3, incY = 2;
  int j;
  struct dn *A, *X, *Y;
  struct dn alpha, beta;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(N*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, j, j) = dn_one();

  for (j = 0; j < N; ++j)
    VREF(X, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    VREF(Y, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_gemv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (dn_ne(VREF(X,j), VREF(Y,j))) {
	printf("ident*randvect failed (%d)\n", j);
	++nerrs;
    }

  alpha = dn_negone();
  beta = dn_one();

  dn_gemv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (!dn_iszero(VREF(Y,j))) {
      printf("ident*randvect - self failed (%d)\n", j);
      ++nerrs;
    }

  free(Y);
  free(X);
  free(A);
}

static void
test_ident_geamv(void)
{
  const int N = 13;
  const int ldA = N+1, incX = 3, incY = 2;
  int j;
  struct dn *A, *X, *Y;
  struct dn alpha, beta;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(N*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, j, j) = dn_negone();

  for (j = 0; j < N; ++j)
    VREF(X, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    VREF(Y, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_geamv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (dn_ne(dn_abs(VREF(X,j)), dn_abs(VREF(Y,j)))) {
	printf("geamv: -ident*randvect failed (%d)\n", j);
	++nerrs;
    }

  free(Y);
  free(X);
  free(A);
}

static void
test_ident_geamm(void)
{
  const int N = 13, M = 4;
  const int ldA = N+1, ldX = N+3, ldY = N;
  int i, j;
  struct dn *A, *X, *Y;
  struct dn alpha, beta;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(ldX*M*sizeof(*X));
  Y = malloc(ldY*M*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, j, j) = dn_negone();

  for (j = 0; j < M; ++j)
    for (i = 0; i < N; ++i) 
      AREF(X, i, j) = dn_make_n(drand48());

  for (j = 0; j < M; ++j)
    for (i = 0; i < N; ++i)
      AREF(Y, i, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_geamm("N", "N", N, M, N, alpha, A, ldA, X, ldX, beta, Y, ldY);

  for (j = 0; j < M; ++j)
    for (i = 0; i < N; ++i)
      if (dn_ne(dn_abs(AREF(X,i,j)), dn_abs(AREF(Y,i,j)))) {
	printf("geamv: -ident*randvect failed (%d, %d)\n", i, j);
	++nerrs;
      }

  free(Y);
  free(X);
  free(A);
}

static void
test_reverse_gemv(void)
{
  const int N = 13;
  const int ldA = N+1, incX = 3, incY = 2;
  int j;
  struct dn *A, *X, *Y;
  struct dn alpha, beta;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(N*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, N-j-1, j) = dn_one();

  for (j = 0; j < N; ++j)
    VREF(X, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    VREF(Y, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_gemv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (dn_ne(VREF(X,N-j-1), VREF(Y,j))) {
	printf("reverse*randvect failed (%d)\n", j);
	++nerrs;
    }

  alpha = dn_negone();
  beta = dn_one();

  dn_gemv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (!dn_iszero(VREF(Y,j))) {
      printf("reverse*randvect - self failed (%d)\n", j);
      ++nerrs;
    }

  free(Y);
  free(X);
  free(A);
}

static void
test_reverse_geamv(void)
{
  const int N = 13;
  const int ldA = N+1, incX = 3, incY = 2;
  int j;
  struct dn *A, *X, *Y;
  struct dn alpha, beta;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(N*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, N-j-1, j) = dn_negone();

  for (j = 0; j < N; ++j)
    VREF(X, j) = dn_make_n(drand48());

  for (j = 0; j < N; ++j)
    VREF(Y, j) = dn_make_n(drand48());

  alpha = dn_one();
  beta = dn_zero();

  dn_geamv("N", N, N, alpha, A, ldA, X, incX, beta, Y, incY);

  for (j = 0; j < N; ++j)
    if (dn_ne(VREF(X,N-j-1), VREF(Y,j))) {
	printf("geamv: -reverse*randvect failed (%d)\n", j);
	++nerrs;
    }

  free(Y);
  free(X);
  free(A);
}

/*
  test:
   ints * (1 + ldbl_eps/64.0) - ldbl_eps/64.0 * ints ??
*/

int
main (void)
{
  test_ident();
  test_reverse();
  test_reverse_tt();
  test_ident_gemv();
  test_ident_geamv();
  test_reverse_gemv();
  test_reverse_geamv();
  test_ident_geamm();
  return nerrs;
}
