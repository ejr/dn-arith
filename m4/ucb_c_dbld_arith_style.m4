# -*- autoconf -*-
# SYNOPSIS
#
#   UCB_C_DBLD_ARITH_STYLE
#
# DESCRIPTION
#
#   Foo!
#
# LAST MODIFICATION
#
#   2007-05-24
#
# COPYRIGHT
#
# Copyright (c) 2007 The Regents of the University of California.
# All rights reserved.
#
# Permission is hereby granted, without written agreement and without
# license or royalty fees, to use, copy, modify, and distribute this
# software and its documentation for any purpose, provided that the above
# copyright notice and the following two paragraphs appear in all copies
# of this software.
#
# IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA OR OTHER COPYRIGHT
# HOLDERS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL,
# INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS
# SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA
# OR OTHER COPYRIGHT HOLDERS HAVE BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# THE UNIVERSITY OF CALIFORNIA AND OTHER COPYRIGHT HOLDERS SPECIFICALLY
# DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
# THE UNIVERSITY OF CALIFORNIA AND OTHER COPYRIGHT HOLDERS HAVE NO
# OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
# MODIFICATIONS.

AC_DEFUN([UCB_C_DBLD_ARITH_STYLE],
[
AC_REQUIRE([AC_PROG_CC])
AC_REQUIRE([AC_TYPE_LONG_DOUBLE_WIDER])

ucb_c_dbld_arith_style=undef

AC_ARG_WITH(dbld-arith,
	[AC_HELP_STRING([--with-dbld-arith=<style>],
			[use doubled arithmetic <style> (ld,dld,dd)])])

case "$with_dbld_arith" in
        yes | set | "" ) ucb_c_dbld_arith_style=undef ;;
        ld | dld | dd ) ucb_c_dbld_arith_style="${with_dbld_arith}" ;;
        * ) ucb_c_dbld_arith_style=error ;;
esac

if test $ucb_c_dbld_arith_style = "error" ; then
	AC_MSG_ERROR([Unknown arith style ${with_dbld_arith}])
fi

if test $cross_compiling = "yes" -a $ucb_c_dbld_arith_style = "undef" ; then
	AC_MSG_ERROR([Arith style must be provided for cross-compilation.])
fi

if test $ucb_c_dbld_arith_style = "undef" ; then
AC_MSG_CHECKING([style of doubled arithmetic])
dnl 	AC_CACHE_CHECK([style of doubled arithmetic],
dnl 		       [ucb_c_dbld_arith_style],[
AC_LANG_PUSH([C])

# First: Check if plain long double is doubled double already.
if test $ucb_c_dbld_arith_style = "undef" ; then
AC_RUN_IFELSE(
  AC_LANG_SOURCE([[
    #include <float.h>
    int main(void) {
    #if !defined(HAVE_LONG_DOUBLE)
      return -1;
    #elif !defined(DBL_EPSILON)
      return -2;
    #elif !defined(LDBL_EPSILON)
      return -3;
    #else
      long double tmp1, tmp2, tmp3;
      if (LDBL_EPSILON > DBL_EPSILON*(long double)DBL_EPSILON)
        return -4;
      tmp1 = 1.0;
      tmp2 = LDBL_EPSILON/4096.0;
      tmp3 = (tmp1 + tmp2) - tmp1;
      if (tmp3 != (LDBL_EPSILON/4096.0))
        return -5;
      return 0;
    #endif
    }
    ]]),
    [ucb_c_dbld_arith_style="ld"],[],[])
fi

# Second: Check for a better-than-double long double.
if test $ucb_c_dbld_arith_style = "undef" ; then
AC_RUN_IFELSE(
  AC_LANG_SOURCE([[
    #include <float.h>
    int main(void)
    {
    #if !defined(LDBL_EPSILON)
      return -1;
    #elif !defined(DBL_EPSILON)
      return -2;
    #else
      if (LDBL_EPSILON >= DBL_EPSILON)
        return -3;
      return 0;
    #endif
    }
    ]]),
    [ucb_c_dbld_arith_style="dld"],[],[])
fi

# Third: Default to doubled double
if test $ucb_c_dbld_arith_style = "undef" ; then
  ucb_c_dbld_arith_style="dd"
fi

AC_LANG_POP([C])

AC_MSG_RESULT([$ucb_c_dbld_arith_style])
dnl 	])dnl AC_CACHE_CHECK
else
AC_MSG_NOTICE([Using ${ucb_c_dbld_arith_style} style of doubled arithmetic])
fi
])dnl UCB_C_DBLD_ARITH_STYLE

