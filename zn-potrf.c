/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

void
DN_M(zn_potf2)(const char *uplo, const int N,
	       struct zn * restrict A, const int ldA,
	       int *info)
{
  _Bool upper = tolower(*uplo) == 'u';
  int j = -1;

  *info = 0;

  if (!upper && tolower(*uplo) != 'l') {
    *info = -1;
    return; /* XXX: XERBLA */
  }

  if (N < 0) {
    *info = -2;
    return;
  }

  if (0 == N) return;

  if (ldA < N) {
    *info = -4;
    return;
  }

  if (upper) {
    for (j = 0; j < N; ++j) {
      int k;
      struct dn Ajj = zn_real(AREF(A,j,j));
      for (k = 0; k < j; ++k) {
	Ajj = zn_ncsqracc(AREF(A,k,j), Ajj);
      }
      if (!dn_ispos(Ajj)) {
	AREF(A,j,j) = zn_make_dn(Ajj);
	goto not_posdef;
      }
      Ajj = dn_sqrt(Ajj);
      AREF(A,j,j) = zn_make_dn(Ajj);
      if (j < N-1) {
	for (k = 0; k < j; ++k) AREF(A,k,j) = zn_conj(AREF(A,k,j));
	zn_gemv("Transpose", j, N-j-1,
		zn_negone(), &AREF(A,0,j+1), ldA, &AREF(A,0,j), 1,
		zn_one(), &AREF(A,j,j+1), ldA);
	for (k = 0; k < j; ++k) AREF(A,k,j) = zn_conj(AREF(A,k,j));
	Ajj = dn_inv(Ajj);
	for (k = j+1; k < N; k++)
	  AREF(A,j,k) = zn_mul_dn(AREF(A,j,k), Ajj);
      }
    }
  }
  else { /* lower */
    for (j = 0; j < N; ++j) {
      int k;
      struct dn Ajj = zn_real(AREF(A,j,j));
      for (k = 0; k < j; ++k) {
	Ajj = zn_ncsqracc(AREF(A,j,k), Ajj);
      }
      if (!dn_ispos(Ajj)) {
	AREF(A,j,j) = zn_make_dn(Ajj);
	goto not_posdef;
      }
      Ajj = dn_sqrt(Ajj);
      AREF(A,j,j) = zn_make_dn(Ajj);
      if (j < N-1) {
	for (k = 0; k < j; ++k) AREF(A,j,k) = zn_conj(AREF(A,j,k));
	zn_gemv("No transpose", N-j-1, j,
		zn_negone(), &AREF(A,j+1,0), ldA, &AREF(A,j,0), ldA,
		zn_one(), &AREF(A,j+1,j), 1);
	Ajj = dn_inv(Ajj);
	for (k = 0; k < j; ++k) AREF(A,j,k) = zn_conj(AREF(A,j,k));
	for (k = j+1; k < N; k++)
	  AREF(A,k,j) = zn_mul_dn(AREF(A,k,j), Ajj);
      }
    }
  }
  return;

 not_posdef:
  *info = j;
  return;
}

void
DN_M(zn_potrf)(const char *uplo, const int N,
	       struct zn * restrict A, const int ldA,
	       int *info)
{
  DN_M(zn_potf2)(uplo, N, A, ldA, info);
}

void
DN_M(zn_potf2_f)(const char *uplo, const int *N,
		 struct zn *A, const int *ldA,
		 int *info)
{
  DN_M(zn_potf2)(uplo, *N, A, *ldA, info);
}

void
DN_M(zn_potrf_f)(const char *uplo, const int *N,
		 struct zn *A, const int *ldA,
		 int *info)
{
  DN_M(zn_potrf)(uplo, *N, A, *ldA, info);
}
