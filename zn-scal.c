/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"

#include "dn-helpers.h"

void
DN_M(zn_scal)(const int N,
	      const struct zn alpha,
	      struct zn * restrict X, const int incX)
{
  int i;

#pragma omp parallel for
  for (i = 0; i < N; ++i)
    VREF(X,i) = zn_mul(alpha, VREF(X,i));
}

void
DN_M(zn_scal_f)(const int *N,
		const struct zn *alpha,
		struct zn *X, const int *incX)
{
  DN_M(zn_scal)(*N, *alpha, X, *incX);
}
