/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/

/* Compatibility defines for debugging */
#define dn_head_ DN_M(dn_head_)
DN_INLINE dn_native_t dn_head_(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_head_(const struct dn a)
{
  return (double)a.x;
}

#define dn_tail_ DN_M(dn_tail_)
DN_INLINE dn_native_t dn_tail_(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_tail_(const struct dn a)
{
  return (double)(a.x - (double)a.x);
}

#define dn_make_ DN_M(dn_make_)
DN_INLINE struct dn dn_make_(const long double a)
  DN_ATTRS_CONST_;
struct dn
dn_make_(const long double a)
{
#if !defined(__cplusplus)
  return  ((struct dn){ .x = a });
#else
  struct dn outval;
  outval.x = a;
  return outval;
#endif
}

#define dn_make DN_M(dn_make)
DN_INLINE struct dn dn_make(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_make(const dn_native_t a, const dn_native_t b)
{
#if !defined(__cplusplus)
  return  ((struct dn){ .x = a + b });
#else
  struct dn outval;
  outval.x = a+b;
  return outval;
#endif
}

#define dn_make_n DN_M(dn_make_n)
DN_INLINE struct dn dn_make_n(const dn_native_t a)
  DN_ATTRS_CONST_;
struct dn
dn_make_n(const dn_native_t a)
{
  return dn_make_(a);
}

#define dn_to_n DN_M(dn_to_n)
DN_INLINE dn_native_t dn_to_n(const struct dn a) DN_ATTRS_CONST_;
dn_native_t
dn_to_n(const struct dn a)
{
  return a.x;
}

#define dn_to_dd DN_M(dn_to_dd)
DN_INLINE void dn_to_dd(const struct dn a, double*, double*);
void
dn_to_dd(const struct dn a, double *d1, double *d2)
{
  *d1 = dn_head_(a);
  *d2 = dn_tail_(a);
}

#define dn_zero DN_M(dn_zero)
DN_INLINE struct dn dn_zero(void) DN_ATTRS_CONST_;
struct dn
dn_zero(void)
{
  return dn_make_n(0.0);
}

#define dn_one DN_M(dn_one)
DN_INLINE struct dn dn_one(void) DN_ATTRS_CONST_;
struct dn
dn_one(void)
{
  return dn_make_n(1.0);
}

#define dn_negone DN_M(dn_negone)
DN_INLINE struct dn dn_negone(void) DN_ATTRS_CONST_;
struct dn
dn_negone(void)
{
  return dn_make_n(-1.0);
}

#define dn_eps DN_M(dn_eps)
DN_INLINE struct dn dn_eps(void) DN_ATTRS_CONST_;
struct dn
dn_eps(void)
{
  return dn_make_n(DBL_EPSILON);
}

#define dn_nan DN_M(dn_nan)
DN_INLINE struct dn dn_nan(void) DN_ATTRS_CONST_;
struct dn
dn_nan(void)
{
#if !defined(__cplusplus)
  return dn_make_n(NAN);
#else
  return dn_make_n(std::numeric_limits<dn_native_t>::quiet_NaN());
#endif
}

#define dn_isnan DN_M(dn_isnan)
DN_INLINE DN_BOOL dn_isnan(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isnan(const struct dn a)
{
  return isnan(a.x);
}

#define dn_inf DN_M(dn_inf)
DN_INLINE struct dn dn_inf(void) DN_ATTRS_CONST_;
struct dn
dn_inf(void)
{
#if !defined(__cplusplus)
  return dn_make_n(INFINITY);
#else
  return dn_make_n(std::numeric_limits<dn_native_t>::infinity());
#endif
}

#define dn_isinf DN_M(dn_isinf)
DN_INLINE DN_BOOL dn_isinf(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isinf(const struct dn a)
{
  return isinf(a.x);
}

#define dn_isfinite DN_M(dn_isfinite)
DN_INLINE DN_BOOL dn_isfinite(const struct dn a) DN_ATTRS_CONST_;
DN_BOOL
dn_isfinite(const struct dn a)
{
#if !defined(__cplusplus)
  return isfinite(a.x);
#else
  return !dn_isnan(a) && !dn_isinf(a);
#endif
}

#define dn_negate DN_M(dn_negate)
DN_INLINE struct dn dn_negate(const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_negate(const struct dn a)
{
  return dn_make_(-a.x);
}

/* Internal. */

/* Computes the nearest integer to d. */
#define dn_nint_n_ DN_M(dn_nint_n_)
DN_INLINE dn_native_t dn_nint_n_(dn_native_t d) DN_ATTRS_CONST_;
dn_native_t
dn_nint_n_(dn_native_t d)
{
  if (d == DN_N_FLOOR_(d))
    return d;
  return DN_N_FLOOR_(d + 0.5);
}

#define dn_add DN_M(dn_add)
DN_INLINE struct dn dn_add(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_add(const struct dn a, const struct dn b)
{
  return dn_make_(a.x + b.x);
}

/* double-double + double */
#define dn_add_n DN_M(dn_add_n)
DN_INLINE struct dn dn_add_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_add_n(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x + b);
}

/* double-double - double-double */
#define dn_sub DN_M(dn_sub)
DN_INLINE struct dn dn_sub(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sub(const struct dn a, const struct dn b)
{
  return dn_make_(a.x - b.x);
}

#define dn_sub_n DN_M(dn_sub_n)
DN_INLINE struct dn dn_sub_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_sub_n(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x - b);
}

/* double-double * double-double */
#define dn_mul DN_M(dn_mul)
DN_INLINE struct dn dn_mul(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_mul(const struct dn a, const struct dn b)
{
  return dn_make_(a.x * b.x);
}

#define dn_sqr DN_M(dn_sqr)
DN_INLINE struct dn dn_sqr(const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_sqr(const struct dn a)
{
  return dn_make_(a.x * a.x);
}

/* double-double * double */
#define dn_mul_n DN_M(dn_mul_n)
DN_INLINE struct dn dn_mul_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_n(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x * b);
}

#define dn_mul_n_n DN_M(dn_mul_n_n)
DN_INLINE struct dn dn_mul_n_n(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_n_n(const dn_native_t a, const dn_native_t b)
{
  return dn_make_(a * b);
}

#define dn_sqr_n DN_M(dn_sqr_n)
DN_INLINE struct dn dn_sqr_n(const dn_native_t a)
  DN_ATTRS_CONST_;
struct dn
dn_sqr_n(const dn_native_t a)
{
  return dn_make_(a * a);
}

#define dn_mul_2 DN_M(dn_mul_2)
DN_INLINE struct dn dn_mul_2(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_mul_2(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x * b);
}

#define dn_macc DN_M(dn_macc)
DN_INLINE struct dn dn_macc(const struct dn a, const struct dn b,
			    const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_macc(const struct dn a, const struct dn b, const struct dn c)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(a.x, b.x, c.x));
#else
  return dn_make_(a.x*b.x+c.x);
#endif
}

#define dn_macc_n DN_M(dn_macc_n)
DN_INLINE struct dn dn_macc_n(const dn_native_t a, const dn_native_t b,
			      const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_macc_n(const dn_native_t a, const dn_native_t b, const struct dn c)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(a,b,c.x));
#else
  return dn_make_(a*(long double)b+c.x);
#endif
}

#define dn_sqracc DN_M(dn_sqracc)
DN_INLINE struct dn dn_sqracc(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sqracc(const struct dn a, const struct dn b)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(a.x,a.x,b.x));
#else
  return dn_make_(a.x*a.x+b.x);
#endif
}

#define dn_sqracc_n DN_M(dn_sqracc_n)
DN_INLINE struct dn dn_sqracc_n(const dn_native_t a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_sqracc_n(const dn_native_t a, const struct dn b)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(a,a,b.x));
#else
  return dn_make_(a*(long double)a + b.x);
#endif
}

#define dn_nmacc DN_M(dn_nmacc)
DN_INLINE struct dn dn_nmacc(const struct dn a, const struct dn b,
			     const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_nmacc(const struct dn a, const struct dn b, const struct dn c)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(-a.x,b.x,c.x));
#else
  return dn_make_(c.x-a.x*b.x);
#endif
}

#define dn_nmacc_n DN_M(dn_nmacc_n)
DN_INLINE struct dn dn_nmacc_n(const dn_native_t a, const dn_native_t b,
			       const struct dn c)
  DN_ATTRS_CONST_;
struct dn
dn_nmacc_n(const dn_native_t a, const dn_native_t b, const struct dn c)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(-a,b,c.x));
#else
  return dn_make_(c.x-a*(long double)b);
#endif
}

#define dn_nsqracc DN_M(dn_nsqracc)
DN_INLINE struct dn dn_nsqracc(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_nsqracc(const struct dn a, const struct dn b)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(-a.x,a.x,b.x));
#else
  return dn_make_(b.x-a.x*a.x);
#endif
}

#define dn_nsqracc_n DN_M(dn_nsqracc_n)
DN_INLINE struct dn dn_nsqracc_n(const dn_native_t a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_nsqracc_n(const dn_native_t a, const struct dn b)
{
#ifdef DN_N_FMA_
  return dn_make_(DN_N_FMA(-a,a,b.x));
#else
  return dn_make_(b.x-a*(long double)a);
#endif
}

/* double-double / double-double */
#define dn_div DN_M(dn_div)
DN_INLINE struct dn dn_div(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_div(const struct dn a, const struct dn b)
{
  return dn_make_(a.x / b.x);
}

#define dn_div_n DN_M(dn_div_n)
DN_INLINE struct dn dn_div_n(const struct dn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_div_n(const struct dn a, const dn_native_t b)
{
  return dn_make_(a.x / b);
}

#define dn_div_n_n DN_M(dn_div_n_n)
DN_INLINE struct dn dn_div_n_n(const dn_native_t a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
dn_div_n_n(const dn_native_t a, const dn_native_t b)
{
  return dn_make_(a / (long double)b);
}

#define dn_inv DN_M(dn_inv)
DN_INLINE struct dn dn_inv(const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_inv(const struct dn a)
{
  return dn_make_(1.0L / a.x);
}

#define dn_gt DN_M(dn_gt)
DN_INLINE DN_BOOL dn_gt(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_gt(const struct dn a, const struct dn b)
{
  return a.x > b.x;
}

#define dn_ge DN_M(dn_ge)
DN_INLINE DN_BOOL dn_ge(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ge(const struct dn a, const struct dn b)
{
  return a.x >= b.x;
}

#define dn_lt DN_M(dn_lt)
DN_INLINE DN_BOOL dn_lt(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_lt(const struct dn a, const struct dn b)
{
  return a.x < b.x;
}

#define dn_le DN_M(dn_le)
DN_INLINE DN_BOOL dn_le(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_le(const struct dn a, const struct dn b)
{
  return a.x <= b.x;
}

#define dn_eq DN_M(dn_eq)
DN_INLINE DN_BOOL dn_eq(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_eq(const struct dn a, const struct dn b)
{
  return a.x == b.x;
}

#define dn_ne DN_M(dn_ne)
DN_INLINE DN_BOOL dn_ne(const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ne(const struct dn a, const struct dn b)
{
  return a.x != b.x;
}

#define dn_iszero DN_M(dn_iszero)
DN_INLINE DN_BOOL dn_iszero (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_iszero (const struct dn a)
{
  return a.x == 0.0L;
}

#define dn_isone DN_M(dn_isone)
DN_INLINE DN_BOOL dn_isone (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isone (const struct dn a)
{
  return a.x == 1.0L;
}

#define dn_isnegone DN_M(dn_isnegone)
DN_INLINE DN_BOOL dn_isnegone (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isnegone (const struct dn a)
{
  return a.x == -1.0L;
}

#define dn_ispos DN_M(dn_ispos)
DN_INLINE DN_BOOL dn_ispos (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_ispos (const struct dn a)
{
  return a.x > 0.0L;
}

#define dn_isneg DN_M(dn_isneg)
DN_INLINE DN_BOOL dn_isneg (const struct dn a)
  DN_ATTRS_CONST_;
DN_BOOL
dn_isneg (const struct dn a)
{
  return a.x < 0.0L;
}

#define dn_abs DN_M(dn_abs)
DN_INLINE struct dn dn_abs (const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_abs (const struct dn a)
{
  return dn_make_(fabsl(a.x));
}

#define dn_copysign DN_M(dn_copysign)
DN_INLINE struct dn dn_copysign (const struct dn, const struct dn)
  DN_ATTRS_CONST_;
struct dn
dn_copysign (const struct dn x, const struct dn y)
{
  return dn_make_(copysignl(a.x, a.y));
}

#define dn_max DN_M(dn_max)
DN_INLINE struct dn dn_max (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_max (const struct dn a, const struct dn b)
{
  return dn_make_(fmaxl(a.x, b.x));
}

#define dn_min DN_M(dn_min)
DN_INLINE struct dn dn_min (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_min (const struct dn a, const struct dn b)
{
  return dn_make_(fminl(a.x, b.x));
}

#define dn_amax DN_M(dn_amax)
DN_INLINE struct dn dn_amax (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_amax (const struct dn a, const struct dn b)
{
  return dn_make_(fmaxl(fabsl(a.x), fabsl(b.x)));
}

#define dn_amin DN_M(dn_amin)
DN_INLINE struct dn dn_amin (const struct dn a, const struct dn b)
  DN_ATTRS_CONST_;
struct dn
dn_amin (const struct dn a, const struct dn b)
{
  return dn_make_(fminl(fabsl(a.x), fabsl(b.x)));
}

/* Round to Nearest integer */
#define dn_nint DN_M(dn_nint)
DN_INLINE struct dn dn_nint (const struct dn a)
  DN_ATTRS_CONST_;
struct dn
dn_nint (const struct dn a)
{
  return dn_make_(dn_nint_n_(a.x));
}

#define dn_floor DN_M(dn_floor)
DN_INLINE struct dn dn_floor (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_floor(const struct dn a)
{
  return dn_make_(floorl(a.x));
}

#define dn_ceil DN_M(dn_ceil)
DN_INLINE struct dn dn_ceil (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_ceil(const struct dn a)
{
  return dn_make_(ceill(a.x));
}

#define dn_aint DN_M(dn_aint)
DN_INLINE struct dn dn_aint (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_aint (const struct dn a)
{
  if (a.x >= 0.0)
    return dn_make_(floorl(a.x));
  return dn_make_(ceill(a.x));
}

/* Computes the square root of the double-double number dd. */
#define dn_sqrt DN_M(dn_sqrt)
DN_INLINE struct dn dn_sqrt (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_sqrt (const struct dn a)
{
  return dn_make_(sqrtl(a.x));
}

#define dn_rsqrt DN_M(dn_rsqrt)
DN_INLINE struct dn dn_rsqrt (const struct dn a) DN_ATTRS_CONST_;
struct dn
dn_rsqrt (const struct dn a)
{
  return dn_make_(1.0/sqrtl(a.x));
}

#define dn_renormalize_ DN_M(dn_renormalize_)
struct dn dn_renormalize_ (struct dn) DN_ATTRS_CONST_;
