/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

void
DN_M(dn_ger)(const int M, const int N,
	     const struct dn alpha,
	     const struct dn * restrict X, const int incX,
	     const struct dn * restrict Y, const int incY,
	     struct dn * restrict A, const int ldA)
{
#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    const struct dn yel = dn_mul(alpha, VREF(Y,j));
    for (int i = 0; i < M; ++i)
      AREF(A,i,j) = dn_macc(VREF(X,i), yel, AREF(A,i,j));
  }
}

void
DN_M(dn_ger_f)(const int *M, const int *N,
	       const struct dn *alpha,
	       const struct dn *X, const int *incX,
	       const struct dn *Y, const int *incY,
	       struct dn *A, const int *ldA)
{
  DN_M(dn_ger)(*M, *N, *alpha, X, *incX, Y, *incY, A, *ldA);
}
