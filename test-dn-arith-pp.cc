/* -*- C++ -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <math.h> // for isfinite, etc.

#include <limits>
#include <iostream>

#if 0
using std::cout;
using std::cerr;
using std::endl;
using std::numeric_limits;
#endif
using namespace std;

#include "dn-arith"
#include "dn-arith-io"

using namespace dn_ns;

int nerrs = 0;

static void
test_add (void)
{
  struct dn a, b, c;
  double x, y;

  x = 1.0;
  y = numeric_limits<dn_native_t>::epsilon()/2.0;
  a = dn_make(x);
  b = dn_make(y);
  c = a + b;
  c -= a;
  if (y != c) {
    cout << "Add/sub failed: " << y << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_sub (void)
{
  struct dn a, b, c;
  double x, y;

  x = 1.0;
  y = numeric_limits<dn_native_t>::epsilon()/16.0;
  a = dn_make(x);
  b = dn_make(y);
  c = a - b;
  c += -a;
  if (y != -c) {
    cout << "Sub/negate failed: " << y << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_mul (void)
{
  struct dn a, b, c;
  dn_native_t tst;

  a = dn_make(1.0, numeric_limits<dn_native_t>::epsilon());
  b = dn_make(1.0, numeric_limits<dn_native_t>::epsilon());
  c = a * b; /* 1 + 2eps + eps**2 */
  c -= a; /* eps + eps**2 */
  c -= numeric_limits<dn_native_t>::epsilon(); /* eps**2 */
  tst = numeric_limits<dn_native_t>::epsilon();
  tst *= tst;
  if (tst != c) {
    cout << "Mul/sub failed: " << tst << " != " << c << endl;
    ++nerrs;
  }
}

static void
test_sqrt (void)
{
  struct dn a, b, c;

  a = dn_make(1.0L, numeric_limits<dn_native_t>::epsilon());
  b = dn_make(1.0L, numeric_limits<dn_native_t>::epsilon());
  c = a * b; /* 1 + 2eps + eps**2 */
  c = sqrt(c); /* 1+eps */
  c -= a;
  if (c != 0) {
    cout << "Sqrt/sub failed: abs(" << c << ") != 0\n";
    ++nerrs;
  }
}

static void
test_div (void)
{
  struct dn a, b, c;

  a = dn_make(1.0L, numeric_limits<dn_native_t>::epsilon());
  b = dn_make(1.0L, numeric_limits<dn_native_t>::epsilon());
  c = a * b; /* 1 + 2eps + eps**2 */
  c /= a; /* 1+eps */
  c -= b;
  if (c != 0) {
    cout << "Div/sub failed: abs(" << c << ") != 0\n";
    ++nerrs;
  }
}

static void
test_cmp (void)
{
  struct dn a, b;

  a = dn_make(1.0, numeric_limits<dn_native_t>::epsilon());
  b = dn_make(1.0);
  if (!(a > b)) {
    cout << "dn_gt failed\n";
    ++nerrs;
  }
  if (!(b < a)) {
    cout << "dn_lt failed\n";
    ++nerrs;
  }
  if (!(a != b)) {
    cout << "dn_ne failed\n";
    ++nerrs;
  }
  b += numeric_limits<dn_native_t>::epsilon();
  if (!(a == b)) {
    cout << "dn_eq failed\n";
    ++nerrs;
  }
}

int
main (void)
{
  test_add();
  test_mul();
  test_sqrt();
  test_div();
  test_sub();
  test_cmp();
  return nerrs;
}
