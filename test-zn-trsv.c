/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "zn-arith.h"
#include "zn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ident(void)
{
  const int N = 11;
  const int ldA = N+5, incX = 7, incY = 1;
  int j;
  struct zn *A, *X, *Y;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = calloc(ldA*N, sizeof(*A));
  X = malloc(N*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  srand48(3735);

  for (j = 0; j < N; ++j)
    AREF(A, j, j) = zn_one();

  for (j = 0; j < N; ++j)
    VREF(Y,j) = VREF(X, j) = zn_make(dn_make(drand48(), drand48()),
				     dn_make(drand48(), drand48()));

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	for (j = 0; j < N; ++j)
	  VREF(X, j) = VREF(Y,j);
	zn_trsv(&uplo[ui], &trans[ti], &diag[di], N, A, ldA, X, incX);

	for (j = 0; j < N; ++j)
	  if (zn_ne(VREF(X,j), VREF(Y,j))) {
	    printf("ident(%c%c%c)\\rand failed (%d)\n",
		   uplo[ui], trans[ti], diag[di], j);
	    ++nerrs;
	  }
      }

  free(Y);
  free(X);
  free(A);
}

static void
test_ones(void)
{
  const int N = 11;
  const int ldA = N+4, incX = 3;
  int i, j;
  struct zn *A, *X;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  const char diag[] = "Un";
  int ui, ti, di;

  A = malloc(ldA*N*sizeof(*A));
  X = malloc(N*incX*sizeof(*X));

  srand48(3735);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      AREF(A, i, j) = zn_one();

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti)
      for (di = 0; di < 2; ++di) {
	if (tolower(uplo[ui]) == 'u') {
	  if (tolower(trans[ti]) == 'n')
	    for (j = 0; j < N; ++j)
	      VREF(X, j) = zn_make_n_n(N-j, N-j);
	  else
	    for (j = 0; j < N; ++j)
	      VREF(X, j) = zn_make_n_n(j+1, j+1);
	}
	else {
	  if (tolower(trans[ti]) == 'n')
	    for (j = 0; j < N; ++j)
	      VREF(X, j) = zn_make_n_n(j+1, j+1);
	  else
	    for (j = 0; j < N; ++j)
	      VREF(X, j) = zn_make_n_n(N-j, N-j);
	}

	zn_trsv(&uplo[ui], &trans[ti], &diag[di], N, A, ldA, X, incX);

	for (j = 0; j < N; ++j)
	  if (zn_ne(VREF(X,j), zn_make_n_n(1,1))) {
	    printf("ones(%c%c%c)\\1:%d' failed (%d)\n",
		   uplo[ui], trans[ti], diag[di], N, j);
	    ++nerrs;
	  }
      }

  free(X);
  free(A);
}

static void
test_threes(void)
{
  const int N = 11;
  const int ldA = N+4, incX = 3;
  int i, j;
  struct zn *A, *X;
  const char uplo[] = "uL";
  const char trans[] = "NtC";
  int ui, ti;

  A = malloc(ldA*N*sizeof(*A));
  X = malloc(N*incX*sizeof(*X));

  srand48(3735);

  for (j = 0; j < N; ++j)
    for (i = 0; i < N; ++i)
      AREF(A, i, j) = zn_make_n(3.0);

  for (ui = 0; ui < 2; ++ui)
    for (ti = 0; ti < 3; ++ti) {
      if (tolower(uplo[ui]) == 'u') {
	if (tolower(trans[ti]) == 'n')
	  for (j = 0; j < N; ++j)
	    VREF(X, j) = zn_make_n_n(3.0*(N-j), 3.0*(N-j));
	else
	  for (j = 0; j < N; ++j)
	    VREF(X, j) = zn_make_n_n(3.0*(j+1), 3.0*(j+1));
      }
      else {
	if (tolower(trans[ti]) == 'n')
	  for (j = 0; j < N; ++j)
	    VREF(X, j) = zn_make_n_n(3.0*(j+1), 3.0*(j+1));
	else
	  for (j = 0; j < N; ++j)
	    VREF(X, j) = zn_make_n_n(3.0*(N-j), 3.0*(N-j));
      }

      zn_trsv(&uplo[ui], &trans[ti], "Non-unit", N, A, ldA, X, incX);

      for (j = 0; j < N; ++j)
	if (zn_ne(VREF(X,j), zn_make_n_n(1,1))) {
	  printf("threes(%c%c)\\1:%d' failed (%d)\n",
		 uplo[ui], trans[ti], N, j);
	  ++nerrs;
	}
    }

  free(X);
  free(A);
}

int
main (void)
{
  test_ident();
  test_ones();
  test_threes();
  return nerrs;
}
