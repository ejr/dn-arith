/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(ZN_ARITH_H_)
#define ZN_ARITH_H_

#include "dn-arith.h"

#if defined(__cplusplus)
extern "C" {
#endif

#if !defined(DN_DONT_USE__COMPLEX) \
  && !(defined(__cplusplus) && defined(DN_CPLUSPLUS_COMPILER_SUCKS))
#define DN_USE__COMPLEX
#endif

#if defined(DN_USE__COMPLEX)
#if defined(DN_HAVE__IMAGINARY)
typedef DN_NATIVE_T _Imaginary dn_native_imag_t;
#endif
typedef DN_NATIVE_T _Complex dn_native_complex_t;
#endif

struct in_ {
  struct dn im;
};

struct zn {
  struct dn re, im;
};

#if defined(DN_USE__COMPLEX)
#define zn_n_real_ DN_M(zn_n_real_)
DN_INLINE dn_native_t zn_n_real_(const dn_native_complex_t z)
  DN_ATTRS_CONST_;
DN_INLINE dn_native_t
zn_n_real_(const dn_native_complex_t z)
{
#if defined(__GNUC__)
  return (__extension__ __real__ z);
#else
  return ((dn_native_t*)&z)[0];
#endif
}

#define zn_n_imag_ DN_M(zn_n_imag)
DN_INLINE dn_native_t zn_n_imag_(const dn_native_complex_t z)
  DN_ATTRS_CONST_;
DN_INLINE dn_native_t
zn_n_imag_(const dn_native_complex_t z)
{
#if defined(__GNUC__)
  return (__extension__ __imag__ z);
#else
  return ((dn_native_t*)&z)[1];
#endif
}

#define zn_I_ DN_M(zn_I_)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE double _Imaginary zn_I_(void)
  DN_ATTRS_CONST_;
DN_INLINE double _Imaginary
zn_I_(void)
{
  return (double _Imaginary)1.0;
}
#else
DN_INLINE double _Complex zn_I_(void)
  DN_ATTRS_CONST_;
DN_INLINE double _Complex
zn_I_(void)
{
#if defined(__GNUC__)
  return (__extension__ 1.0iF);
#else
  static const double Idata[2] = {0.0,1.0};
  return *(double _Complex*)Idata;
#endif
}
#endif

#endif /* defined(DN_USE__COMPLEX) */

/* Constructors from hell.
   Support:
     {dn,d,n} => imaginary: in__make{,_d,_n}
     {(dn,dn),(d,d),(n,n)} => complex: zn_make{,_d_d,_n_n}
     {dn,d,n} => complex, real: zn_make{_dn,_d,_n}
     {dn,d,n} => complex, imag: zn_make_im{_dn,_d,_n}
     imag => complex, imag: zn_make_im
     d _Complex, n _Complex => complex: zn_make{_dz,_nz}
     d _Imaginary, n _Imaginary => imag: in__make{_di,_ni}
     d _Imaginary, n _Imaginary => complex: zn_make{_di,_ni}
*/

#define in__make DN_M(in__make)
DN_INLINE struct in_ in__make(const struct dn im)
  DN_ATTRS_CONST_;
struct in_
in__make(const struct dn im)
{
#if !defined(__cplusplus)
  return ((struct in_){.im = im});
#else
  struct in_ out;
  out.im = im;
  return out;
#endif
}

#define in__make_n DN_M(in__make_n)
DN_INLINE struct in_ in__make_n(const dn_native_t im)
  DN_ATTRS_CONST_;
struct in_
in__make_n(const dn_native_t im)
{
  return in__make(dn_make_n(im));
}

#define zn_make DN_M(zn_make)
DN_INLINE struct zn zn_make(const struct dn re, const struct dn im)
  DN_ATTRS_CONST_;
struct zn
zn_make(const struct dn re, const struct dn im)
{
#if !defined(__cplusplus)
  return ((struct zn){.re = re, .im = im});
#else
  struct zn out;
  out.re = re;
  out.im = im;
  return out;
#endif
}

#define zn_make_n_n DN_M(zn_make_n_n)
DN_INLINE struct zn zn_make_n_n(const dn_native_t re, const dn_native_t im)
  DN_ATTRS_CONST_;
struct zn
zn_make_n_n(const dn_native_t re, const dn_native_t im)
{
  return zn_make(dn_make_n(re), dn_make_n(im));
}

#define zn_make_dn DN_M(zn_make_dn)
DN_INLINE struct zn zn_make_dn(const struct dn re)
  DN_ATTRS_CONST_;
struct zn
zn_make_dn(const struct dn re)
{
  return zn_make(re, dn_zero());
}

#define zn_make_n DN_M(zn_make_n)
DN_INLINE struct zn zn_make_n(const dn_native_t re)
  DN_ATTRS_CONST_;
struct zn
zn_make_n(const dn_native_t re)
{
  return zn_make(dn_make_n(re), dn_zero());
}

#define zn_make_im_dn DN_M(zn_make_im_dn)
DN_INLINE struct zn zn_make_im_dn(const struct dn im)
  DN_ATTRS_CONST_;
struct zn
zn_make_im_dn(const struct dn im)
{
  return zn_make(dn_zero(), im);
}

#define zn_make_im_n DN_M(zn_make_im_n)
DN_INLINE struct zn zn_make_im_n(const dn_native_t im)
  DN_ATTRS_CONST_;
struct zn
zn_make_im_n(const dn_native_t im)
{
  return zn_make(dn_zero(), dn_make_n(im));
}

#define zn_make_in_ DN_M(zn_make_in_)
DN_INLINE struct zn zn_make_in_(const struct in_ im)
  DN_ATTRS_CONST_;
struct zn
zn_make_in_(const struct in_ im)
{
  return zn_make(dn_zero(), im.im);
}

#if defined(DN_USE__COMPLEX)
#define zn_make_nz DN_M(zn_make_nz)
DN_INLINE struct zn zn_make_nz(const dn_native_complex_t z)
  DN_ATTRS_CONST_;
struct zn
zn_make_nz(const dn_native_complex_t z)
{
  return zn_make(dn_make_n(zn_n_real_(z)), dn_make_n(zn_n_imag_(z)));
}

#define in__make_ni DN_M(in__make_ni)
#define zn_make_ni DN_M(zn_make_ni)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE struct in_ in__make_ni(const dn_native_imag_t z)
  DN_ATTRS_CONST_;
struct in_
in__make_ni(const dn_native_imag_t z)
{
  return in__make(dn_make_n((dn_native_t)z));
}

DN_INLINE struct zn zn_make_ni(const dn_native_imag_t z)
  DN_ATTRS_CONST_;
struct zn
zn_make_ni(const dn_native_imag_t z)
{
  return zn_make(dn_zero(), dn_make_n((dn_native_t)z));
}
#else
DN_INLINE struct in_ in__make_ni(const dn_native_t z)
  DN_ATTRS_CONST_;
struct in_
in__make_ni(const dn_native_t z)
{
  return in__make(dn_make_n(z));
}

DN_INLINE struct zn zn_make_ni(const dn_native_t z)
  DN_ATTRS_CONST_;
struct zn
zn_make_ni(const dn_native_t z)
{
  return zn_make(dn_zero(), dn_make_n(z));
}
#endif /* DN_HAVE__IMAGINARY */

/* Narrowing conversions.

   zn => double _Complex, dn _Complex: zn_to_{dz,nz}
   in_ => double _Imaginary, dn _Imaginary: zn_to_{di,ni}

*/

#define zn_to_nz DN_M(zn_to_nz)
DN_INLINE dn_native_complex_t zn_to_nz (const struct zn z)
  DN_ATTRS_CONST_;
dn_native_complex_t
zn_to_nz (const struct zn z)
{
  return dn_to_n(z.re) + zn_I_() * dn_to_n(z.im);
}

#if defined(DN_HAVE__IMAGINARY)
#define in__to_ni DN_M(in__to_ni)
DN_INLINE dn_native_imag_t in__to_ni (const struct in_ z)
  DN_ATTRS_CONST_;
dn_native_imag_t
in__to_di (const struct in_ z)
{
  return zn_I_() * dn_to_n(z.im);
}
#endif /* DN_HAVE__IMAGINARY */
#endif /* DN_USE__COMPLEX */

/* Accessors: real, imag */

#define zn_real DN_M(zn_real)
DN_INLINE struct dn zn_real(const struct zn z)
  DN_ATTRS_CONST_;
struct dn
zn_real(const struct zn z)
{
  return z.re;
}

#define zn_imag DN_M(zn_imag)
DN_INLINE struct dn zn_imag(const struct zn z)
  DN_ATTRS_CONST_;
struct dn
zn_imag(const struct zn z)
{
  return z.im;
}

#define in__real DN_M(in__real)
DN_INLINE struct dn in__real(const struct in_ z)
  DN_ATTRS_CONST_;
struct dn
in__real(const struct in_ z)
{
  return dn_zero();
}

#define in__imag DN_M(in__imag)
DN_INLINE struct dn in__imag(const struct in_ z)
  DN_ATTRS_CONST_;
struct dn
in__imag(const struct in_ z)
{
  return z.im;
}

/* Constants */

#define zn_zero DN_M(zn_zero)
DN_INLINE struct zn zn_zero(void) DN_ATTRS_CONST_;
struct zn
zn_zero(void)
{
  return zn_make(dn_zero(), dn_zero());
}

#define in__zero DN_M(in__zero)
DN_INLINE struct in_ in__zero(void) DN_ATTRS_CONST_;
struct in_
in__zero(void)
{
  return in__make(dn_zero());
}

#define zn_one DN_M(zn_one)
DN_INLINE struct zn zn_one(void) DN_ATTRS_CONST_;
struct zn
zn_one(void)
{
  return zn_make(dn_one(), dn_zero());
}

#define zn_negone DN_M(zn_negone)
DN_INLINE struct zn zn_negone(void) DN_ATTRS_CONST_;
struct zn
zn_negone(void)
{
  return zn_make(dn_negone(), dn_zero());
}

#define zn_I DN_M(zn_I)
DN_INLINE struct zn zn_I(void) DN_ATTRS_CONST_;
struct zn
zn_I(void)
{
  return zn_make(dn_zero(), dn_one());
}

#define in__I DN_M(in__I)
DN_INLINE struct in_ in__I(void) DN_ATTRS_CONST_;
struct in_
in__I(void)
{
  return in__make(dn_one());
}

#define zn_negI DN_M(zn_negI)
DN_INLINE struct zn zn_negI(void) DN_ATTRS_CONST_;
struct zn
zn_negI(void)
{
  return zn_make(dn_zero(), dn_negone());
}

#define in__negI DN_M(in__negI)
DN_INLINE struct in_ in__negI(void) DN_ATTRS_CONST_;
struct in_
in__negI(void)
{
  return in__make(dn_negone());
}

/* abs, abssqr, abs1, amax, amin */

#define zn_abs DN_M(zn_abs)
DN_INLINE struct dn zn_abs(const struct zn z) DN_ATTRS_CONST_;
struct dn
zn_abs(const struct zn z)
{
  if (dn_iszero(z.im)) return dn_abs(z.re);
  if (dn_iszero(z.re)) return dn_abs(z.im);
  /* The arithmetic is expensive enough that we might as
     well try to scale sanely. */
#if 0
  /* there is a bug lurking in sqracc.  blah. */
  if (dn_ge(z.re, z.im))
    return dn_mul(z.re,
		  dn_sqrt(dn_add(dn_one(),
				 dn_sqr(dn_div(z.im,
					       z.re)))));
  else
    return dn_mul(z.im,
		  dn_sqrt(dn_add(dn_one(),
				 dn_sqr(dn_div(z.re,
					       z.im)))));
#else
  return dn_sqrt(dn_add(dn_sqr(z.re), dn_sqr(z.im)));
#endif
}

#define in__abs DN_M(in__abs)
DN_INLINE struct dn in__abs(const struct in_ z) DN_ATTRS_CONST_;
struct dn
in__abs(const struct in_ z)
{
  return dn_abs(z.im);
}

#define zn_abs_n_ DN_M(zn_abs_n_)
DN_INLINE dn_native_t zn_abs_n_(const struct zn z) DN_ATTRS_CONST_;
dn_native_t
zn_abs_n_(const struct zn z)
{
  /* This is meant as an optimization, so no scaling. */
  const dn_native_t re = dn_to_n(z.re),
    im = dn_to_n(z.im);
  return DN_N_SQRT_(re*re + im*im);
}

#define zn_abssqr DN_M(zn_abssqr)
DN_INLINE struct dn zn_abssqr(const struct zn z) DN_ATTRS_CONST_;
struct dn
zn_abssqr(const struct zn z)
{
  return dn_add(dn_sqr(z.re), dn_sqr(z.im));
}

#define zn_abs1 DN_M(zn_abs1)
DN_INLINE struct dn zn_abs1(const struct zn z) DN_ATTRS_CONST_;
struct dn
zn_abs1(const struct zn z)
{
  return dn_add(dn_abs(z.re), dn_abs(z.im));
}

#define zn_abs1_n_ DN_M(zn_abs1_n_)
DN_INLINE dn_native_t zn_abs1_n_(const struct zn z) DN_ATTRS_CONST_;
dn_native_t
zn_abs1_n_(const struct zn z)
{
  return DN_N_ABS_(dn_to_n(z.re)) + DN_N_ABS_(dn_to_n(z.im));
}

#define zn_amax DN_M(zn_amax)
DN_INLINE struct zn zn_amax(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
struct zn
zn_amax(const struct zn z1, const struct zn z2)
{
  const dn_native_t cheap_1 = zn_abs_n_(z1),
    cheap_2 = zn_abs_n_(z2);
  if (cheap_1 > cheap_2 * (1.0 + 64.0*DN_N_EPS_))
    return z1;
  if (cheap_2 > cheap_1 * (1.0 + 64.0*DN_N_EPS_))
    return z2;
  if (dn_ge(zn_abs(z1), zn_abs(z2)))
    return z1;
  return z2;
}

#define zn_amax1 DN_M(zn_amax1)
DN_INLINE struct zn zn_amax1(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
struct zn
zn_amax1(const struct zn z1, const struct zn z2)
{
  const dn_native_t cheap_1 = zn_abs1_n_(z1),
    cheap_2 = zn_abs1_n_(z2);
  if (cheap_1 > cheap_2 * (1.0 + 64.0*DN_N_EPS_))
    return z1;
  if (cheap_2 > cheap_1 * (1.0 + 64.0*DN_N_EPS_))
    return z2;
  if (dn_ge(zn_abs1(z1), zn_abs1(z2)))
    return z1;
  return z2;
}

#define zn_amin DN_M(zn_amin)
DN_INLINE struct zn zn_amin(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
struct zn
zn_amin(const struct zn z1, const struct zn z2)
{
  const dn_native_t cheap_1 = zn_abs_n_(z1),
    cheap_2 = zn_abs_n_(z2);
  if (cheap_1 < cheap_2 * (1.0 + 64.0*DN_N_EPS_))
    return z1;
  if (cheap_2 < cheap_1 * (1.0 + 64.0*DN_N_EPS_))
    return z2;
  if (dn_le(zn_abs(z1), zn_abs(z2)))
    return z1;
  return z2;
}

#define zn_amin1 DN_M(zn_amin1)
DN_INLINE struct zn zn_amin1(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
struct zn
zn_amin1(const struct zn z1, const struct zn z2)
{
  const dn_native_t cheap_1 = zn_abs1_n_(z1),
    cheap_2 = zn_abs1_n_(z2);
  if (cheap_1 < cheap_2 * (1.0 + 64.0*DN_N_EPS_))
    return z1;
  if (cheap_2 < cheap_1 * (1.0 + 64.0*DN_N_EPS_))
    return z2;
  if (dn_le(zn_abs1(z1), zn_abs1(z2)))
    return z1;
  return z2;
}

/* Basic comparison. */

#define zn_eq DN_M(zn_eq)
DN_INLINE DN_BOOL zn_eq(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
DN_BOOL
zn_eq(const struct zn z1, const struct zn z2)
{
  return dn_eq(z1.re,z2.re) && dn_eq(z1.im,z2.im);
}

#define zn_ne DN_M(zn_ne)
DN_INLINE DN_BOOL zn_ne(const struct zn z1, const struct zn z2)
  DN_ATTRS_CONST_;
DN_BOOL
zn_ne(const struct zn z1, const struct zn z2)
{
  return dn_ne(z1.re,z2.re) || dn_ne(z1.im,z2.im);
}

#define in__eq DN_M(in__eq)
DN_INLINE DN_BOOL in__eq(const struct in_ z1, const struct in_ z2)
  DN_ATTRS_CONST_;
DN_BOOL
in__eq(const struct in_ z1, const struct in_ z2)
{
  return dn_eq(z1.im,z2.im);
}

#define in__ne DN_M(in__ne)
DN_INLINE DN_BOOL in__ne(const struct in_ z1, const struct in_ z2)
  DN_ATTRS_CONST_;
DN_BOOL
in__ne(const struct in_ z1, const struct in_ z2)
{
  return dn_ne(z1.im,z2.im);
}

#define zn_iszero DN_M(zn_iszero)
DN_INLINE DN_BOOL zn_iszero(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_iszero(const struct zn z)
{
  return dn_iszero(z.re) && dn_iszero(z.im);
}

#define zn_isone DN_M(zn_isone)
DN_INLINE DN_BOOL zn_isone(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isone(const struct zn z)
{
  return dn_isone(z.re) && dn_iszero(z.im);
}

#define zn_isnegone DN_M(zn_isnegone)
DN_INLINE DN_BOOL zn_isnegone(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isnegone(const struct zn z)
{
  return dn_isnegone(z.re) && dn_iszero(z.im);
}

#define zn_isI DN_M(zn_isI)
DN_INLINE DN_BOOL zn_isI(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isI(const struct zn z)
{
  return dn_isone(z.im) && dn_iszero(z.re);
}

#define zn_isnegI DN_M(zn_isnegI)
DN_INLINE DN_BOOL zn_isnegI(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isnegI(const struct zn z)
{
  return dn_isnegone(z.im) && dn_iszero(z.re);
}

#define in__iszero DN_M(in__iszero)
DN_INLINE DN_BOOL in__iszero(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__iszero(const struct in_ z)
{
  return dn_iszero(z.im);
}

#define in__isI DN_M(in__isI)
DN_INLINE DN_BOOL in__isI(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__isI(const struct in_ z)
{
  return dn_isone(z.im);
}

#define in__isnegI DN_M(in__isnegI)
DN_INLINE DN_BOOL in__isnegI(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__isnegI(const struct in_ z)
{
  return dn_isnegone(z.im);
}

#define zn_isnan DN_M(zn_isnan)
DN_INLINE DN_BOOL zn_isnan(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isnan(const struct zn z)
{
  return dn_isnan(z.im) || dn_isnan(z.re);
}

#define zn_isinf DN_M(zn_isinf)
DN_INLINE DN_BOOL zn_isinf(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isinf(const struct zn z)
{
  return dn_isinf(z.im) || dn_isinf(z.re);
}

#define zn_isfinite DN_M(zn_isfinite)
DN_INLINE DN_BOOL zn_isfinite(const struct zn z)
  DN_ATTRS_CONST_;
DN_BOOL
zn_isfinite(const struct zn z)
{
  return dn_isfinite(z.im) && dn_isfinite(z.re);
}

#define in__isnan DN_M(in__isnan)
DN_INLINE DN_BOOL in__isnan(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__isnan(const struct in_ z)
{
  return dn_isnan(z.im);
}

#define in__isinf DN_M(in__isinf)
DN_INLINE DN_BOOL in__isinf(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__isinf(const struct in_ z)
{
  return dn_isinf(z.im);
}

#define in__isfinite DN_M(in__isfinite)
DN_INLINE DN_BOOL in__isfinite(const struct in_ z)
  DN_ATTRS_CONST_;
DN_BOOL
in__isfinite(const struct in_ z)
{
  return dn_isfinite(z.im);
}

/* Simple manipulations */

#define zn_negate DN_M(zn_negate)
DN_INLINE struct zn zn_negate(const struct zn z)
  DN_ATTRS_CONST_;
struct zn
zn_negate(const struct zn z)
{
  return zn_make(dn_negate(z.re),dn_negate(z.im));
}

#define in__negate DN_M(in__negate)
DN_INLINE struct in_ in__negate(const struct in_ z)
  DN_ATTRS_CONST_;
struct in_
in__negate(const struct in_ z)
{
  return in__make(dn_negate(z.im));
}

#define zn_conj DN_M(zn_conj)
DN_INLINE struct zn zn_conj(const struct zn z)
  DN_ATTRS_CONST_;
struct zn
zn_conj(const struct zn z)
{
  return zn_make(z.re,dn_negate(z.im));
}

#define in__conj DN_M(in__conj)
DN_INLINE struct in_ in__conj(const struct in_ z)
  DN_ATTRS_CONST_;
struct in_
in__conj(const struct in_ z)
{
  return in__negate(z);
}

#define zn_mul_I DN_M(zn_mul_I)
DN_INLINE struct zn zn_mul_I(const struct zn z)
  DN_ATTRS_CONST_;
struct zn
zn_mul_I(const struct zn z)
{
  return zn_make(dn_negate(z.im), z.re);
}

#define zn_mul_negI DN_M(zn_mul_negI)
DN_INLINE struct zn zn_mul_negI(const struct zn z)
  DN_ATTRS_CONST_;
struct zn
zn_mul_negI(const struct zn z)
{
  return zn_make(z.im, dn_negate(z.re));
}

/* Basic arithmetic
   ops: sqr,csqr,+,-,*,/,macc,csqracc,msub,csqrsub
*/

/* Addition and subtraction */

#define zn_add DN_M(zn_add)
DN_INLINE struct zn zn_add(const struct zn a, const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_add(const struct zn a, const struct zn b)
{
  return zn_make(dn_add(a.re,b.re), dn_add(a.im,b.im));
}

#define zn_add_in_ DN_M(zn_add_in_)
DN_INLINE struct zn zn_add_in_(const struct zn a, const struct in_ b)
  DN_ATTRS_CONST_;
struct zn
zn_add_in_(const struct zn a, const struct in_ b)
{
  return zn_make(a.re, dn_add(a.im,b.im));
}

#define in__add DN_M(in__add)
DN_INLINE struct in_ in__add(const struct in_ a, const struct in_ b)
  DN_ATTRS_CONST_;
struct in_
in__add(const struct in_ a, const struct in_ b)
{
  return in__make(dn_add(a.im,b.im));
}

#define zn_add_n DN_M(zn_add_n)
DN_INLINE struct zn zn_add_n(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_add_n(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_add_n(a.re,b), a.im);
}

#define zn_add_dn DN_M(zn_add_dn)
DN_INLINE struct zn zn_add_dn(const struct zn a, const struct dn b)
  DN_ATTRS_CONST_;
struct zn
zn_add_dn(const struct zn a, const struct dn b)
{
  return zn_make(dn_add(a.re,b), a.im);
}

#if defined(DN_USE__COMPLEX)
#define zn_add_nz DN_M(zn_add_nz)
DN_INLINE struct zn zn_add_nz(const struct zn a, const dn_native_complex_t b)
  DN_ATTRS_CONST_;
struct zn
zn_add_nz(const struct zn a, const dn_native_complex_t b)
{
  return zn_make(dn_add_n(a.re,zn_n_real_(b)), dn_add_n(a.im,zn_n_imag(b)));
}

#define in__add_ni DN_M(in__add_ni)
#define zn_add_ni DN_M(zn_add_ni)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE struct in_ in__add_ni(const struct in_ a, const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct in_
in__add_ni(const struct in_ a, const dn_native_imag_t b)
{
  return in__make(dn_add_n(a.im, (dn_native_t)b));
}

DN_INLINE struct zn zn_add_ni(const struct zn a, dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct zn
zn_add_ni(const struct zn a, const dn_native_imag_t b)
{
  return zn_make(a.re, dn_add_n(a.im,(dn_native_t)b));
}
#else // This is probably an astonishingly poor idea, but...
DN_INLINE struct in_ in__add_ni(const struct in_ a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct in_
in__add_ni(const struct in_ a, const dn_native_t b)
{
  return in__make(dn_add_n(a.im, b));
}

DN_INLINE struct zn zn_add_ni(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_add_ni(const struct zn a, const dn_native_t b)
{
  return zn_make(a.re, dn_add_n(a.im,b));
}
#endif
#endif

#define zn_sub DN_M(zn_sub)
DN_INLINE struct zn zn_sub(const struct zn a, const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_sub(const struct zn a, const struct zn b)
{
  return zn_make(dn_sub(a.re,b.re), dn_sub(a.im,b.im));
}

#define in__sub DN_M(in__sub)
DN_INLINE struct in_ in__sub(const struct in_ a, const struct in_ b)
  DN_ATTRS_CONST_;
struct in_
in__sub(const struct in_ a, const struct in_ b)
{
  return in__make(dn_sub(a.im,b.im));
}

#define zn_sub_in_ DN_M(zn_sub_in_)
DN_INLINE struct zn zn_sub_in_(const struct zn a, const struct in_ b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_in_(const struct zn a, const struct in_ b)
{
  return zn_make(a.re, dn_sub(a.im,b.im));
}

#define zn_sub_n DN_M(zn_sub_n)
DN_INLINE struct zn zn_sub_n(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_n(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_sub_n(a.re,b), a.im);
}

#define zn_sub_dn DN_M(zn_sub_dn)
DN_INLINE struct zn zn_sub_dn(const struct zn a, const struct dn b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_dn(const struct zn a, const struct dn b)
{
  return zn_make(dn_sub(a.re,b), a.im);
}

#if defined(DN_USE__COMPLEX)
#define zn_sub_nz DN_M(zn_sub_nz)
DN_INLINE struct zn zn_sub_nz(const struct zn a, const dn_native_complex_t b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_nz(const struct zn a, const dn_native_complex_t b)
{
  return zn_make(dn_sub_n(a.re,zn_n_real_(b)), dn_sub_n(a.im,zn_n_imag(b)));
}

#define in__sub_ni DN_M(in__sub_ni)
#define zn_sub_ni DN_M(zn_sub_ni)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE struct in_ in__sub_ni(const struct in_ a, const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct in_
in__sub_ni(const struct in_ a, const dn_native_imag_t b)
{
  return in__make(dn_sub_n(a.im, (dn_native_t)b));
}

DN_INLINE struct zn zn_sub_ni(const struct zn a, dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_ni(const struct zn a, const dn_native_imag_t b)
{
  return zn_make(a.re, dn_sub_n(a.im,(dn_native_t)b));
}
#else
DN_INLINE struct in_ in__sub_ni(const struct in_ a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct in_
in__sub_ni(const struct in_ a, const dn_native_t b)
{
  return in__make(dn_sub_n(a.im, b));
}

DN_INLINE struct zn zn_sub_ni(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_sub_ni(const struct zn a, const dn_native_t b)
{
  return zn_make(a.re, dn_sub_n(a.im,b));
}
#endif
#endif

/* Multiplication, square */

#define zn_mul DN_M(zn_mul)
DN_INLINE struct zn zn_mul(const struct zn a, const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_mul(const struct zn a, const struct zn b)
{
  return zn_make(dn_sub(dn_mul(a.re,b.re),dn_mul(a.im,b.im)),
		 dn_add(dn_mul(a.re,b.im),dn_mul(a.im,b.re)));
}

#define in__mul DN_M(in__mul)
DN_INLINE struct dn in__mul(const struct in_ a, const struct in_ b)
  DN_ATTRS_CONST_;
struct dn
in__mul(const struct in_ a, const struct in_ b)
{
  return dn_negate(dn_mul(a.im,b.im));
}

#define zn_mul_in_ DN_M(zn_mul_in_)
DN_INLINE struct zn zn_mul_in_(const struct zn a, const struct in_ b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_in_(const struct zn a, const struct in_ b)
{
  return zn_make(dn_negate(dn_mul(a.im,b.im)),
		 dn_mul(a.re,b.im));
}

#define zn_mul_n DN_M(zn_mul_n)
DN_INLINE struct zn zn_mul_n(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_n(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_mul_n(a.re,b), dn_mul_n(a.im,b));
}

#define zn_mul_dn DN_M(zn_mul_dn)
DN_INLINE struct zn zn_mul_dn(const struct zn a, const struct dn b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_dn(const struct zn a, const struct dn b)
{
  return zn_make(dn_mul(a.re,b), dn_mul(a.im,b));
}

#define zn_mul_2 DN_M(zn_mul_2)
DN_INLINE struct zn zn_mul_2(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_2(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_mul_2(a.re,b), dn_mul_2(a.im,b));
}

#if defined(DN_USE__COMPLEX)
#define zn_mul_nz DN_M(zn_mul_nz)
DN_INLINE struct zn zn_mul_nz(const struct zn a, const dn_native_complex_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_nz(const struct zn a, const dn_native_complex_t b)
{
  const dn_native_t b_re = zn_n_real_(b);
  const dn_native_t b_im = zn_n_imag_(b);
  return zn_make(dn_sub(dn_mul_n(a.re,b_re),dn_mul_n(a.im,b_im)),
		 dn_add(dn_mul_n(a.re,b_im),dn_mul_n(a.im,b_re)));
}

#define in__mul_ni DN_M(in__mul_ni)
#define zn_mul_ni DN_M(zn_mul_ni)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE struct dn in__mul_ni(const struct in_ a, const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct dn
in__mul_ni(const struct in_ a, const dn_native_imag_t b)
{
  return dn_mul_n(a.im, -(dn_native_t)b);
}

DN_INLINE struct zn zn_mul_ni(const struct zn a, const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_ni(const struct zn a, const dn_native_imag_t b)
{
  return zn_make(dn_negate(dn_mul_n(a.im,(dn_native_t)b)),
		 dn_mul_n(a.re,(dn_native_t)b));
}
#else
DN_INLINE struct dn in__mul_ni(const struct in_ a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn
in__mul_ni(const struct in_ a, const dn_native_t b)
{
  return dn_mul_n(a.im, -b);
}

DN_INLINE struct zn zn_mul_ni(const struct zn a, const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_ni(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_negate(dn_mul_n(a.im,b)),
		 dn_mul_n(a.re,b));
}
#endif

#define zn_mul_nz_nz DN_M(zn_mul_nz_nz)
DN_INLINE struct zn zn_mul_nz_nz(const dn_native_complex_t a,
				 const dn_native_complex_t b)
  DN_ATTRS_CONST_;
struct zn
zn_mul_nz_nz(const dn_native_complex_t a, const dn_native_complex_t b)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  const dn_native_t b_re = zn_n_real_(b);
  const dn_native_t b_im = zn_n_imag_(b);
  return zn_make(dn_sub(dn_mul_n_n(a_re,b_re),dn_mul_n_n(a_im,b_im)),
		 dn_add(dn_mul_n_n(a_re,b_im),dn_mul_n_n(a_im,b_re)));
}
#endif /* DN_USE__COMPLEX */

#define zn_sqr DN_M(zn_sqr)
DN_INLINE struct zn zn_sqr(const struct zn a)
  DN_ATTRS_CONST_;
struct zn
zn_sqr(const struct zn a)
{
  return zn_make(dn_sub(dn_sqr(a.re),dn_sqr(a.im)),
		 dn_mul_2(dn_mul(a.im,a.re),2.0));
}

#define in__sqr DN_M(in__sqr)
DN_INLINE struct dn in__sqr(const struct in_ a)
  DN_ATTRS_CONST_;
struct dn
in__sqr(const struct in_ a)
{
  return dn_negate(dn_sqr(a.im));
}

#if defined(DN_USE__COMPLEX)
#define zn_sqr_nz DN_M(zn_sqr_nz)
DN_INLINE struct zn zn_sqr_nz(const dn_native_complex_t a)
  DN_ATTRS_CONST_;
struct zn
zn_sqr_nz(const dn_native_complex_t a)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  return zn_make(dn_sub(dn_sqr_n(a_re),dn_sqr_n(a_im)),
		 dn_mul_2(dn_mul_n_n(a_im,a_re),2.0));
}
#endif

/* multiply-accumulate and friends */

#define zn_macc DN_M(zn_macc)
DN_INLINE struct zn zn_macc(const struct zn a, const struct zn b,
			    const struct zn c)
  DN_ATTRS_CONST_;
struct zn
zn_macc(const struct zn a, const struct zn b, const struct zn c)
{
  /*
    c.re + i*c.im + (a.re + i*a.im) * (b.re + i*b.im)
    = c.re + a.re*b.re - a.im*b.im
      + i*( c.im + a.im*b.re + a.re*b.im)
  */
  return zn_make( dn_nmacc( a.im, b.im,
			    dn_macc( a.re, b.re,
				     c.re ) ),
		  dn_macc( a.re, b.im,
			   dn_macc( a.im, b.re,
				    c.im ) ) );
}

#if defined(DN_USE__COMPLEX)
#define zn_macc_nz DN_M(zn_macc_nz)
DN_INLINE struct zn zn_macc_nz(const dn_native_complex_t a,
			       const dn_native_complex_t b,
			       const struct zn c)
  DN_ATTRS_CONST_;
struct zn zn_macc_nz(const dn_native_complex_t a,
		     const dn_native_complex_t b,
		     const struct zn c)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  const dn_native_t b_re = zn_n_real_(b);
  const dn_native_t b_im = zn_n_imag_(b);
  return zn_make( dn_nmacc_n( a_im, b_im,
			      dn_macc_n( a_re, b_re,
					 c.re ) ),
		  dn_macc_n( a_re, b_im,
			     dn_macc_n( a_im, b_re,
					c.im ) ) );
}
#endif

#define zn_sqracc DN_M(zn_sqracc)
DN_INLINE struct zn zn_sqracc(const struct zn a,
			      const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_sqracc(const struct zn a, const struct zn b)
{
  /*
    b.re + i*b.im + (a.re + i*a.im) * (a.re + i*a.im)
    = b.re + a.re**2 - a.im**2
      + i*( b.im + 2.0*a.im*a.re )
  */
  return zn_make( dn_nsqracc(a.im, dn_sqracc(a.re, b.re)),
		  dn_macc(dn_mul_2(a.re,2.0), a.im, b.im) );
}

#if defined(DN_USE__COMPLEX)
#define zn_sqracc_nz DN_M(zn_sqracc_nz)
DN_INLINE struct zn zn_sqracc_nz(const dn_native_complex_t a,
				 const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_sqracc_nz(const dn_native_complex_t a, const struct zn b)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  return zn_make( dn_nsqracc_n(a_im, dn_sqracc_n(a_re, b.re)),
		  dn_macc_n(2.0*a_re, a_im, b.im) );
}
#endif

#define zn_csqracc DN_M(zn_csqracc)
DN_INLINE struct dn zn_csqracc(const struct zn a,
			       const struct dn b)
  DN_ATTRS_CONST_;
struct dn
zn_csqracc(const struct zn a, const struct dn b)
{
  return dn_sqracc(a.im, dn_sqracc(a.re, b));
}

#if defined(DN_USE__COMPLEX)
#define zn_csqracc_nz DN_M(zn_csqracc_nz)
DN_INLINE struct dn zn_csqracc_nz(const dn_native_complex_t a,
				  const struct dn b)
  DN_ATTRS_CONST_;
struct dn
zn_csqracc_nz(const dn_native_complex_t a, const struct dn b)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  return dn_sqracc_n(a_im, dn_sqracc_n(a_re, b));
}
#endif

#define zn_nmacc DN_M(zn_nmacc)
DN_INLINE struct zn zn_nmacc(const struct zn a,
			     const struct zn b, const struct zn c)
  DN_ATTRS_CONST_;
struct zn
zn_nmacc(const struct zn a, const struct zn b, const struct zn c)
{
  /*
    c.re + i*c.im - (a.re + i*a.im) * (b.re + i*b.im)
    = c.re - a.re*b.re + a.im*b.im
      + i*( c.im - a.im*b.re - a.re*b.im)
  */
  return zn_make( dn_nmacc(a.re, b.re,
			   dn_macc(a.im, b.im,
				   c.re)),
		  dn_nmacc(a.im, b.re,
			   dn_nmacc(a.re, b.im,
				    c.im)) );
}

#if defined(DN_USE__COMPLEX)
#define zn_nmacc_nz DN_M(zn_nmacc_nz)
DN_INLINE struct zn zn_nmacc_nz(const dn_native_complex_t a,
				const dn_native_complex_t b,
				const struct zn c)
  DN_ATTRS_CONST_;
struct zn zn_nmacc_nz(const dn_native_complex_t a,
		      const dn_native_complex_t b,
		      const struct zn c)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  const dn_native_t b_re = zn_n_real_(b);
  const dn_native_t b_im = zn_n_imag_(b);
  return zn_make( dn_nmacc_n(a_re, b_re,
			     dn_macc_n(a_im, b_im,
				       c.re)),
		  dn_nmacc_n(a_im, b_re,
			     dn_nmacc_n(a_re, b_im,
					c.im)) );
}
#endif

#define zn_nsqracc DN_M(zn_nsqracc)
DN_INLINE struct zn zn_nsqracc(const struct zn a,
			       const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_nsqracc(const struct zn a, const struct zn b)
{
  /*
    b.re + i*b.im - (a.re + i*a.im) * (a.re + i*a.im)
    = b.re - a.re**2 + a.im**2
      + i*( b.im - 2.0*a.im*a.re )
  */
  return zn_make( dn_sqracc(a.im, dn_nsqracc(a.re, b.re)),
		  dn_nmacc(dn_mul_2(a.re,2.0),a.im,b.im) );
}

#if defined(DN_USE__COMPLEX)
#define zn_nsqracc_nz DN_M(zn_nsqracc_nz)
DN_INLINE struct zn zn_nsqracc_nz(const dn_native_complex_t a,
				  const struct zn b)
  DN_ATTRS_CONST_;
struct zn
zn_nsqracc_nz(const dn_native_complex_t a, const struct zn b)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  return zn_make( dn_sqracc_n(a_im, dn_nsqracc_n(a_re, b.re)),
		  dn_nmacc_n(a_re*2.0,a_im,b.im) );
}
#endif

#define zn_ncsqracc DN_M(zn_ncsqracc)
DN_INLINE struct dn zn_ncsqracc(const struct zn a,
				const struct dn b)
  DN_ATTRS_CONST_;
struct dn
zn_ncsqracc(const struct zn a, const struct dn b)
{
  return dn_nsqracc(a.im, dn_nsqracc(a.re, b));
}

#if defined(DN_USE__COMPLEX)
#define zn_ncsqracc_nz DN_M(zn_ncsqracc_nz)
DN_INLINE struct dn zn_ncsqracc_nz(const dn_native_complex_t a,
				   const struct dn b)
  DN_ATTRS_CONST_;
struct dn
zn_ncsqracc_nz(const dn_native_complex_t a, const struct dn b)
{
  const dn_native_t a_re = zn_n_real_(a);
  const dn_native_t a_im = zn_n_imag_(a);
  return dn_nsqracc_n(a_im, dn_nsqracc_n(a_re, b));
}
#endif

/* Division, sqrt */

#define in__div DN_M(in__div)
DN_INLINE struct dn in__div(const struct in_ a,
			    const struct in_ b)
  DN_ATTRS_CONST_;
struct dn in__div(const struct in_ a, const struct in_ b)
{
  return dn_div(a.im,b.im);
}

#define zn_div_dn DN_M(zn_div_dn)
DN_INLINE struct zn zn_div_dn(const struct zn a,
			      const struct dn b)
  DN_ATTRS_CONST_;
struct zn zn_div_dn(const struct zn a, const struct dn b)
{
  return zn_make(dn_div(a.re, b), dn_div(a.im,b));
}

#define zn_div_n DN_M(zn_div_n)
DN_INLINE struct zn zn_div_n(const struct zn a,
			      const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn zn_div_n(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_div_n(a.re, b), dn_div_n(a.im,b));
}

#define zn_div_in_ DN_M(zn_div_in_)
DN_INLINE struct zn zn_div_in_(const struct zn a,
			       const struct in_ b)
  DN_ATTRS_CONST_;
struct zn zn_div_in_(const struct zn a, const struct in_ b)
{
  return zn_make(dn_div(a.im, b.im), dn_negate(dn_div(a.re,b.im)));
}

/* far too large ever to be inlined. */
#define zn_div DN_M(zn_div)
struct zn zn_div(struct zn a, struct zn b) DN_ATTRS_CONST_;

#define zn_div_n_n DN_M(zn_div_n_n)
struct zn zn_div_n_n(struct zn a,
		     dn_native_t breal, dn_native_t bimag)
  DN_ATTRS_CONST_;

#if defined(DN_USE__COMPLEX)
#define zn_div_nz DN_M(zn_div_nz)
DN_INLINE struct zn zn_div_nz(struct zn a, const dn_native_complex_t b)
  DN_ATTRS_CONST_;
struct zn
zn_div_nz(struct zn a, const dn_native_complex_t b)
{
  return zn_div_n_n(a, zn_n_real_(b), zn_n_imag_(b));
}

#define in__div_ni DN_M(in__div_ni)
#define zn_div_ni DN_M(zn_div_ni)
#if defined(DN_HAVE__IMAGINARY)
DN_INLINE struct dn in__div_ni(const struct in_ a,
			       const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct dn in__div_ni(const struct in_ a, const dn_native_imag_t b)
{
  return dn_div_n(a.im, (dn_native_t)b);
}

DN_INLINE struct zn zn_div_ni(const struct zn a,
			      const dn_native_imag_t b)
  DN_ATTRS_CONST_;
struct zn zn_div_ni(const struct zn a, const dn_native_imag_t b)
{
  return zn_make(dn_div_n(a.im, (dn_native_t)b),
		 dn_div_n(a.re, -(dn_native_t)b));
}
#else
DN_INLINE struct dn in__div_ni(const struct in_ a,
			       const dn_native_t b)
  DN_ATTRS_CONST_;
struct dn in__div_ni(const struct in_ a, const dn_native_t b)
{
  return dn_div_n(a.im, b);
}

DN_INLINE struct zn zn_div_ni(const struct zn a,
			      const dn_native_t b)
  DN_ATTRS_CONST_;
struct zn zn_div_ni(const struct zn a, const dn_native_t b)
{
  return zn_make(dn_div_n(a.im, b),
		 dn_div_n(a.re, -b));
}
#endif
#endif /* DN_USE__COMPLEX */

/* inverse could use optimization */
#define zn_inv DN_M(zn_inv)
DN_INLINE struct zn zn_inv(const struct zn a)
  DN_ATTRS_CONST_;
struct zn zn_inv(const struct zn a)
{
  return zn_div(zn_one(), a);
}

#define zn_sqrt DN_M(zn_sqrt)
DN_INLINE struct zn zn_sqrt(struct zn z) DN_ATTRS_CONST_;
struct zn
zn_sqrt(const struct zn z)
{
  if (DN_UNLIKELY_(dn_iszero(z.re))) { // just imaginary
    const struct dn t = dn_sqrt(dn_mul_2(dn_abs(z.im), 0.5));
    return zn_make(t, dn_isneg(z.im)? dn_negate(t) : t);
  }
  else if (DN_UNLIKELY_(dn_iszero(z.im))) { // actually real
    if (dn_isneg(z.re)) {
      return zn_make_im_dn(dn_sqrt(dn_abs(z.re)));
    }
    else {
      return zn_make_dn(dn_sqrt(z.re));
    }
  }
  else {  // yucktacular.
    const struct dn t = dn_sqrt(dn_mul_2(dn_add(zn_abs(z),
						dn_abs(z.re)),
					 2.0));
    const struct dn u = dn_mul_2(t, 0.5);
    if (dn_ispos(z.re)) // right half-plane
      return zn_make(u, dn_div(z.im, t));
    else // left
      return zn_make(dn_div(dn_abs(z.im), t),
		     dn_isneg(z.im)? dn_negate(u) : u);
  }
}

#define in__sqrt DN_M(in__sqrt)
DN_INLINE struct zn in__sqrt(struct in_ z) DN_ATTRS_CONST_;
struct zn
in__sqrt(const struct in_ z)
{
  const struct dn t = dn_sqrt(dn_mul_2(dn_abs(z.im), 0.5));
  return zn_make(t, dn_isneg(z.im)? dn_negate(t) : t);
}

/* Reciprocal sqrt, needs improvement */
#define zn_rsqrt DN_M(zn_rsqrt)
DN_INLINE struct zn zn_rsqrt(struct zn z) DN_ATTRS_CONST_;
struct zn
zn_rsqrt(const struct zn z)
{
  return zn_div(zn_one(), zn_sqrt(z));
}

#define in__rsqrt DN_M(in__rsqrt)
DN_INLINE struct zn in__rsqrt(struct in_ z) DN_ATTRS_CONST_;
struct zn
in__rsqrt(const struct in_ z)
{
  return zn_div(zn_one(), in__sqrt(z));
}

#define zn_sum_ DN_M(zn_sum_)
struct zn zn_sum_(const size_t, const struct zn*) DN_ATTRS_;

#define zn_asum_ DN_M(zn_asum_)
struct dn zn_asum_(const size_t, const struct zn*) DN_ATTRS_;

#define zn_sum_strided_ DN_M(zn_sum_strided_)
struct zn zn_sum_strided_(const size_t,
			  const struct zn*, const size_t) DN_ATTRS_;

#define zn_asum_strided_ DN_M(zn_asum_strided_)
struct dn zn_asum_strided_(const size_t,
			   const struct zn*, const size_t) DN_ATTRS_;

#define zn_sumprod_ DN_M(zn_sumprod_)
struct zn zn_sumprod_ (const size_t,
		       const struct zn *, const struct zn *)
  DN_ATTRS_;

#define zn_asumprod_ DN_M(zn_asumprod_)
struct dn zn_asumprod_ (const size_t,
			const struct zn *, const struct zn *)
  DN_ATTRS_;

/* (a+ib) * (c-id) = ac -iad +ibc +bd */
#define zn_sumprod_nc_ DN_M(zn_sumprod_nc_)
struct zn zn_sumprod_nc_ (const size_t,
			  const struct zn *, const struct zn *)
  DN_ATTRS_;

/* (a-ib) * (c-id) = ac -iad -ibc -bd */
#define zn_sumprod_cc_ DN_M(zn_sumprod_cc_)
struct zn zn_sumprod_cc_ (const size_t,
			  const struct zn *, const struct zn *)
  DN_ATTRS_;

#define zn_sumprod_strided_ DN_M(zn_sumprod_strided_)
struct zn zn_sumprod_strided_ (const size_t,
			       const struct zn *, const size_t,
			       const struct zn *, const size_t)
  DN_ATTRS_;

#define zn_asumprod_strided_ DN_M(zn_asumprod_strided_)
struct dn zn_asumprod_strided_ (const size_t,
				const struct zn *, const size_t,
				const struct zn *, const size_t)
  DN_ATTRS_;

#define zn_sumprod_strided_nc_ DN_M(zn_sumprod_strided_nc_)
struct zn zn_sumprod_strided_nc_ (const size_t,
				  const struct zn *, const size_t,
				  const struct zn *, const size_t)
  DN_ATTRS_;

#define zn_sumprod_strided_cc_ DN_M(zn_sumprod_strided_cc_)
struct zn zn_sumprod_strided_cc_ (const size_t,
				  const struct zn *, const size_t,
				  const struct zn *, const size_t)
  DN_ATTRS_;

#if defined(__cplusplus)
} // extern "C"
#endif

#endif /* ZN_ARITH_H_ */
