/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

struct dn
DN_M(dn_dot)(const int N,
	     const struct dn * restrict X, const int incX,
	     const struct dn * restrict Y, const int incY)
{
  if (incX == 1 && incY == 1)
    return dn_sumprod_(N, X, Y);
  else
    return dn_sumprod_strided_(N, X, incX, Y, incY);
}

struct dn
DN_M(dn_dot_f)(const int *N,
	       const struct dn *X, const int *incX,
	       const struct dn *Y, const int *incY)
{
  return DN_M(dn_dot)(*N, X, *incX, Y, *incY);
}
