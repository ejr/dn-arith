/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

static int nerrs = 0;

static void
test_ger (void)
{
  const int M = 11, N = 38;
  const int ldA = M + 4, incX = 3, incY = 8;
  struct dn *A, *X, *Y;
  int i, j;
  struct dn small = dn_make_n(DBL_EPSILON/4096.0);

  A = malloc(ldA*N*sizeof(*A));
  X = malloc(M*incX*sizeof(*X));
  Y = malloc(N*incY*sizeof(*Y));

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      AREF(A,i,j) = small;

  for (i = 0; i < M; ++i)
    VREF(X,i) = dn_one();
  for (j = 0; j < N; ++j)
    VREF(Y,j) = dn_make_n(2.0);

  dn_ger(M, N, dn_one(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (dn_ne(small, dn_sub_n(AREF(A,i,j), 2.0))) {
	printf("test_ger failed: (%d %d)\n", i, j);
	++nerrs;
      }

  dn_ger(M, N, dn_negone(), X, incX, Y, incY, A, ldA);

  for (j = 0; j < N; ++j)
    for (i = 0; i < M; ++i)
      if (dn_ne(small, AREF(A,i,j))) {
	printf("test_ger - old failed: (%d %d)\n", i, j);
	++nerrs;
      }

  free(Y);
  free(X);
  free(A);
}

int
main (void)
{
  test_ger();
  return nerrs;
}
