/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(DN_BLAS_H_)
#define DN_BLAS_H_

#if defined(__cplusplus)
extern "C" {
#endif

#define dn_dot DN_M(dn_dot)
struct dn dn_dot(const int,
		 const struct dn * DN_RESTRICT, const int,
		 const struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_dot_f DN_M(dn_dot_f)
struct dn dn_dot_f(const int *,
		   const struct dn * DN_RESTRICT, const int *,
		   const struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_scal DN_M(dn_scal)
void dn_scal(const int,
	     const struct dn,
	     struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_scal_f DN_M(dn_scal_f)
void dn_scal_f(const int *,
	       const struct dn *,
	       struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_iamax DN_M(dn_iamax)
int dn_iamax(const int,
	     const struct dn * DN_RESTRICT,
	     const int)
  DN_ATTRS_;

#define dn_iamax_f DN_M(dn_iamax_f)
int dn_iamax_f(const int *,
	       const struct dn * DN_RESTRICT,
	       const int *)
  DN_ATTRS_;

#define dn_gemv DN_M(dn_gemv)
void dn_gemv(const char *, const int, const int,
	     const struct dn,
	     const struct dn * DN_RESTRICT, const int,
	     const struct dn * DN_RESTRICT, const int,
	     const struct dn,
	     struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_gemv_f DN_M(dn_gemv_f)
void dn_gemv_f(const char *, const int *, const int *,
	       const struct dn *,
	       const struct dn * DN_RESTRICT, const int *,
	       const struct dn * DN_RESTRICT, const int *,
	       const struct dn *,
	       struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_geamv DN_M(dn_geamv)
void dn_geamv(const char *, const int, const int,
	      const struct dn,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_geamv_f DN_M(dn_geamv_f)
void dn_geamv_f(const char *, const int *, const int *,
		const struct dn *,
		const struct dn * DN_RESTRICT, const int *,
		const struct dn * DN_RESTRICT, const int *,
		const struct dn *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_gemm DN_M(dn_gemm)
void dn_gemm (const char *, const char *,
	      const int, const int, const int,
	      const struct dn,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_gemm_f DN_M(dn_gemm_f)
void dn_gemm_f (const char *, const char *,
		const int *, const int *, const int *,
		const struct dn *,
		const struct dn * DN_RESTRICT, const int *,
		const struct dn * DN_RESTRICT, const int *,
		const struct dn *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_geamm DN_M(dn_geamm)
void dn_geamm (const char *, const char *,
	       const int, const int, const int,
	       const struct dn,
	       const struct dn * DN_RESTRICT, const int,
	       const struct dn * DN_RESTRICT, const int,
	       const struct dn,
	       struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_geamm_f DN_M(dn_geamm_f)
void dn_geamm_f (const char *, const char *,
		 const int *, const int *, const int *,
		 const struct dn *,
		 const struct dn * DN_RESTRICT, const int *,
		 const struct dn * DN_RESTRICT, const int *,
		 const struct dn *,
		 struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_ger DN_M(dn_ger)
void dn_ger(const int, const int,
	    const struct dn,
	    const struct dn * DN_RESTRICT, const int,
	    const struct dn * DN_RESTRICT, const int,
	    struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_ger_f DN_M(dn_ger_f)
void dn_ger_f(const int *, const int *,
	      const struct dn *,
	      const struct dn * DN_RESTRICT, const int *,
	      const struct dn * DN_RESTRICT, const int *,
	      struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_syrk DN_M(dn_syrk)
void dn_syrk (const char *, const char *, const int,
	      const int,
	      const struct dn,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_syrk_f DN_M(dn_syrk_f)
void dn_syrk_f (const char *, const char *, const int *,
		const int *,
		const struct dn *,
		const struct dn * DN_RESTRICT, const int *,
		const struct dn *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_trsv DN_M(dn_trsv)
void dn_trsv (const char *, const char *, const char *,
	      const int,
	      const struct dn * DN_RESTRICT, const int,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_trsv_f DN_M(dn_trsv_f)
void dn_trsv_f (const char *, const char *, const char *,
		const int *,
		const struct dn * DN_RESTRICT, const int *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#define dn_trsm DN_M(dn_trsm)
void dn_trsm (const char *, const char *,
	      const char *, const char *,
	      const int,
	      const int,
	      const struct dn,
	      const struct dn * DN_RESTRICT, const int,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

#define dn_trsm_f DN_M(dn_trsm_f)
void dn_trsm_f (const char *, const char *,
		const char *, const char *,
		const int *,
		const int *,
		const struct dn *,
		const struct dn * DN_RESTRICT, const int *,
		struct dn * DN_RESTRICT, const int *)
  DN_ATTRS_;

#if defined(__cplusplus)
}
#endif

#endif /* DN_BLAS_H_ */
