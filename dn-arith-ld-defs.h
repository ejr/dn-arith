/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
typedef double dn_native_t;
#define DN_NATIVE_T double

struct dn {
  long double x;
};

#if defined(FP_FAST_FMAL)
#define DN_N_FMA_ fmal
#endif

#define DN_N_FLOOR_ floor
#define DN_N_SQRT_ sqrt
#define DN_N_ABS_ fabs
#define DN_N_EPS_ DBL_EPSILON
#define DN_N_OV_ DBL_MAX
#define DN_N_UN_ DBL_MIN
