/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#if !defined(DN_LAPACK_H_)
#define DN_LAPACK_H_

#if defined(__cplusplus)
extern "C" {
#endif

#define dn_potf2 DN_M(dn_potf2)
void dn_potf2(const char *, const int,
	      struct dn * DN_RESTRICT, const int,
	      int *)
  DN_ATTRS_;

#define dn_potf2_f DN_M(dn_potf2_f)
void dn_potf2_f(const char *, const int *,
		struct dn * DN_RESTRICT, const int *,
		int *)
  DN_ATTRS_;

#define dn_potrf DN_M(dn_potrf)
void dn_potrf(const char *, const int,
	      struct dn * DN_RESTRICT, const int,
	      int *)
  DN_ATTRS_;

#define dn_potrf_f DN_M(dn_potrf_f)
void dn_potrf_f(const char *, const int *,
		struct dn * DN_RESTRICT, const int *,
		int *)
  DN_ATTRS_;

#define dn_laswp DN_M(dn_laswp)
void dn_laswp (int, struct dn * DN_RESTRICT, int, int, int,
	       const int * DN_RESTRICT, int);

#define dn_laswp_f DN_M(dn_laswp_f)
void dn_laswp_f (int *, struct dn * DN_RESTRICT, int, int, int,
		 int * DN_RESTRICT, int);

#define dn_getrf DN_M(dn_getrf)
void dn_getrf(const int, const int,
	      struct dn * DN_RESTRICT, const int,
	      int * DN_RESTRICT, int *)
  DN_ATTRS_;

#define dn_getrf_f DN_M(dn_getrf_f)
void dn_getrf_f(const int *, const int *,
		struct dn * DN_RESTRICT, const int *,
		int * DN_RESTRICT, int *)
  DN_ATTRS_;

#define dn_getrs DN_M(dn_getrs)
void dn_getrs (const char *, const int, const int,
	       const struct dn * DN_RESTRICT, const int,
	       const int * DN_RESTRICT,
	       struct dn * DN_RESTRICT, const int,
	       int *)
  DN_ATTRS_;

#define dn_getrs_f DN_M(dn_getrs_f)
void dn_getrs_f (const char *, const int *, const int *,
		 const struct dn * DN_RESTRICT, const int *,
		 const int * DN_RESTRICT,
		 struct dn * DN_RESTRICT, const int *,
		 int *)
  DN_ATTRS_;

#define dn_getri DN_M(dn_getri)
void dn_getri (const int,
	       struct dn * DN_RESTRICT, const int,
	       const int * DN_RESTRICT,
	       int *)
  DN_ATTRS_;

#define dn_getri_f DN_M(dn_getri_f)
void dn_getri_f (const int *,
		 struct dn * DN_RESTRICT, const int *,
		 const int * DN_RESTRICT,
		 int *)
  DN_ATTRS_;

#if defined(__cplusplus)
}
#endif

#endif /* DN_LAPACK_H_ */
