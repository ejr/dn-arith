/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdio.h>
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

#define dn_gemm DN_M(dn_gemm)
void dn_gemm (const char *, const char *,
	      const int, const int, const int,
	      const struct dn,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn * DN_RESTRICT, const int,
	      const struct dn,
	      struct dn * DN_RESTRICT, const int)
  DN_ATTRS_;

enum diag { UNIT, DEFINED };

static void
trsm_LUN (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = M;

  //printf("in trsm_LUN\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,K-1,K-1));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,K-1,0)), dn_to_n(invdg));
    for (int j = 0; j < N; ++j)
      AREF(B,K-1,j) = dn_mul(AREF(B,K-1,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,K-1-k,K-k);
#if 0
      printf("single pull A(%d, %d) (%Lg) * B(%d, %d:%d) to B(%d, %d:%d)\n",
	     K-1-k, K-k, dn_to_n(offdg),
	     K-k, 0, N-1,
	     K-1-k, 0, N-1);
#endif
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_mul( dn_nmacc(offdg, AREF(B,K-k,j),
					     AREF(B,K-1-k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_nmacc(offdg, AREF(B,K-k,j),
				     AREF(B,K-1-k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
#if 0
	printf("pull from A(%d:%d, %d:%d) and B(%d:%d, %d:%d) to B(%d:%d, %d:%d)\n",
	       K-k-n_to_update, K-k-n_to_update+n_to_update-1, K-k, K-k+n_to_pull-1,
	       K-k, K-k+n_to_pull-1, 0, N-1,
	       K-k-n_to_update, K-k-n_to_update+n_to_update-1, 0, N-1);
#endif
	dn_gemm("Notrans", "Notrans", n_to_update, N, n_to_pull,
		dn_negone(), &AREF(A,K-k-n_to_update,K-k), ldA,
		&AREF(B,K-k,0), ldB,
		dn_one(), &AREF(B,K-k-n_to_update,0), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_mul(AREF(B,K-1-k,j), invdg);
      }
    }
  }
  //printf("out trsm_LUN\n");
}

static void
trsm_RUN (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = N;

  //printf("in trsm_RUN\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,0,0));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,0,0)), dn_to_n(invdg));
    for (int j = 0; j < M; ++j)
      AREF(B,j,0) = dn_mul(AREF(B,j,0), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,k-1,k);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_mul( dn_nmacc(offdg, AREF(B,j,k-1),
					 AREF(B,j,k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_nmacc(offdg, AREF(B,j,k-1),
				 AREF(B,j,k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	dn_gemm("Notrans", "Notrans", M, n_to_update, n_to_pull,
		dn_negone(),
		&AREF(B,0,k-n_to_pull), ldB,
		&AREF(A,k-n_to_pull,k), ldA,
		dn_one(), &AREF(B,0,k), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_mul(AREF(B,j,k), invdg);
      }
    }
  }
  //printf("out trsm_RUN\n");
}

static void
trsm_LLN (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = M;

  //printf("in trsm_LLN\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,0,0));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,0,0)), dn_to_n(invdg));
    for (int j = 0; j < N; ++j)
      AREF(B,0,j) = dn_mul(AREF(B,0,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,k,k-1);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_mul( dn_nmacc(offdg, AREF(B,k-1,j),
					 AREF(B,k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_nmacc(offdg, AREF(B,k-1,j),
				 AREF(B,k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
#if 0
	printf("pull from A(%d:%d, %d:%d) and B(%d:%d, %d:%d) to B(%d:%d, %d:%d)\n",
	       k, k+n_to_update-1, k-n_to_pull, k-n_to_pull+n_to_pull-1,
	       k-n_to_pull, k-n_to_pull+n_to_pull-1, 0, N-1,
	       k, k+n_to_update-1, 0, N-1);
#endif
	dn_gemm("Notrans", "Notrans", n_to_update, N, n_to_pull,
		dn_negone(), &AREF(A,k,k-n_to_pull), ldA,
		&AREF(B,k-n_to_pull,0), ldB,
		dn_one(), &AREF(B,k,0), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_mul(AREF(B,k,j), invdg);
      }
    }
  }
  //printf("out trsm_LLN\n");
}

static void
trsm_RLN (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = N;

  //printf("in trsm_RLN\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,K-1,K-1));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,K-1,0)), dn_to_n(invdg));
    for (int j = 0; j < M; ++j)
      AREF(B,j,K-1) = dn_mul(AREF(B,j,K-1), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,K-k,K-1-k);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_mul( dn_nmacc(offdg, AREF(B,j,K-k),
					     AREF(B,j,K-1-k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_nmacc(offdg, AREF(B,j,K-k),
				     AREF(B,j,K-1-k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	dn_gemm("Notrans", "Notrans", M, n_to_update, n_to_pull,
		dn_negone(),
		&AREF(B,0,K-k), ldB,
		&AREF(A,K-k,K-k-n_to_update), ldA,
		dn_one(), &AREF(B,0,K-k-n_to_update), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_mul(AREF(B,j,K-1-k), invdg);
      }
    }
  }
  //printf("out trsm_RLN\n");
}

static void
trsm_LUT (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = M;

  //printf("in trsm_LUT\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,0,0));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,0,0)), dn_to_n(invdg));
    for (int j = 0; j < N; ++j)
      AREF(B,0,j) = dn_mul(AREF(B,0,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,k-1,k);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_mul( dn_nmacc(offdg, AREF(B,k-1,j),
					 AREF(B,k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_nmacc(offdg, AREF(B,k-1,j),
				 AREF(B,k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
#if 0
	printf("pull from A(%d:%d, %d:%d); and B(%d:%d, %d:%d) to B(%d:%d, %d:%d)\n",
	       k+n_to_update-1, k, k-n_to_pull, k-n_to_pull+n_to_pull-1,
	       k-n_to_pull, k-n_to_pull+n_to_pull-1, 0, N-1,
	       k, k+n_to_update-1, 0, N-1);
#endif
	dn_gemm("Trans", "Notrans", n_to_update, N, n_to_pull,
		dn_negone(), &AREF(A,k-n_to_pull,k), ldA,
		&AREF(B,k-n_to_pull,0), ldB,
		dn_one(), &AREF(B,k,0), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < N; ++j)
	  AREF(B,k,j) = dn_mul(AREF(B,k,j), invdg);
      }
    }
  }
  //printf("out trsm_LUT\n");
}

static void
trsm_RUT (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = N;

  //printf("in trsm_RUT\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,K-1,K-1));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,K-1,0)), dn_to_n(invdg));
    for (int j = 0; j < M; ++j)
      AREF(B,j,K-1) = dn_mul(AREF(B,j,K-1), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,K-1-k,K-k);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_mul( dn_nmacc(offdg, AREF(B,j,K-k),
					     AREF(B,j,K-1-k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_nmacc(offdg, AREF(B,j,K-k),
				     AREF(B,j,K-1-k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	dn_gemm("Notrans", "Trans", M, n_to_update, n_to_pull,
		dn_negone(),
		&AREF(B,0,K-k), ldB,
		&AREF(A,K-k-n_to_update,K-k), ldA,
		dn_one(), &AREF(B,0,K-k-n_to_update), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,K-1-k) = dn_mul(AREF(B,j,K-1-k), invdg);
      }
    }
  }
  //printf("out trsm_RUT\n");
}

static void
trsm_LLT (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = M;

  //printf("in trsm_LLT\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,K-1,K-1));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,K-1,0)), dn_to_n(invdg));
    for (int j = 0; j < N; ++j)
      AREF(B,K-1,j) = dn_mul(AREF(B,K-1,j), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,K-k,K-1-k);
#if 0
      printf("single pull A(%d, %d) (%Lg) * B(%d, %d:%d) to B(%d, %d:%d)\n",
	     K-1-k, K-k, dn_to_n(offdg),
	     K-k, 0, N-1,
	     K-1-k, 0, N-1);
#endif
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_mul( dn_nmacc(offdg, AREF(B,K-k,j),
					     AREF(B,K-1-k,j)),
				invdg);
      }
      else {
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_nmacc(offdg, AREF(B,K-k,j),
				     AREF(B,K-1-k,j));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
#if 0
	printf("pull from A(%d:%d, %d:%d) and B(%d:%d, %d:%d) to B(%d:%d, %d:%d)\n",
	       K-k-n_to_update+n_to_update-1, K-k-n_to_update, K-k, K-k+n_to_pull-1,
	       K-k, K-k+n_to_pull-1, 0, N-1,
	       K-k-n_to_update, K-k-n_to_update+n_to_update-1, 0, N-1);
#endif
	dn_gemm("Trans", "Notrans", n_to_update, N, n_to_pull,
		dn_negone(), &AREF(A,K-k,K-k-n_to_update), ldA,
		&AREF(B,K-k,0), ldB,
		dn_one(), &AREF(B,K-k-n_to_update,0), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,K-1-k,K-1-k));
	for (int j = 0; j < N; ++j)
	  AREF(B,K-1-k,j) = dn_mul(AREF(B,K-1-k,j), invdg);
      }
    }
  }
  //printf("out trsm_LLT\n");
}

static void
trsm_RLT (const enum diag diag, const int M, const int N,
	  const struct dn * restrict A, const int ldA,
	  struct dn * restrict B, const int ldB)
{
  const int K = N;

  //printf("in trsm_RLT\n");
  if (K == 0) return;
  if (diag != UNIT) {
    const struct dn invdg = dn_inv(AREF(A,0,0));
    //printf("mul %Lg by invdg %Lg\n", dn_to_n(AREF(B,0,0)), dn_to_n(invdg));
    for (int j = 0; j < M; ++j)
      AREF(B,j,0) = dn_mul(AREF(B,j,0), invdg);
  }
  if (K == 1) return;
  for (int k = 1; k < K; ++k) {
    const int n_to_pull = k & (1+~k);
    if (1 == n_to_pull) {
      // premature optimization
      const struct dn offdg = AREF(A,k,k-1);
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_mul( dn_nmacc(offdg, AREF(B,j,k-1),
					 AREF(B,j,k)),
				invdg);
      }
      else {
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_nmacc(offdg, AREF(B,j,k-1),
				 AREF(B,j,k));
      }
    }
    else {
      const int n_remaining = K - k;
      const int n_to_update = (n_to_pull < n_remaining? n_to_pull :
			       n_remaining);
      if (n_to_update) {
	dn_gemm("Notrans", "Trans", M, n_to_update, n_to_pull,
		dn_negone(),
		&AREF(B,0,k-n_to_pull), ldB,
		&AREF(A,k,k-n_to_pull), ldA,
		dn_one(), &AREF(B,0,k), ldB);
      }
      if (diag != UNIT) {
	const struct dn invdg = dn_inv(AREF(A,k,k));
	for (int j = 0; j < M; ++j)
	  AREF(B,j,k) = dn_mul(AREF(B,j,k), invdg);
      }
    }
  }
  //printf("out trsm_RLT\n");
}

void
DN_M(dn_trsm)(const char *side, const char *uplo,
	      const char *transa, const char *diag,
	      const int M,
	      const int N,
	      const struct dn alpha,
	      const struct dn * restrict A, const int ldA,
	      struct dn * restrict B, const int ldB)
{
  int i, j;
  enum diag d;

  if (dn_iszero(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = dn_zero();
    return;
  }
  if (dn_isnegone(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = dn_negate(AREF(B,i,j));
  }
  else if (!dn_isone(alpha)) {
    for (j = 0; j < N; ++j)
      for (i = 0; i < M; ++i)
	AREF(B,i,j) = dn_mul(alpha, AREF(B,i,j));
  }

  if (tolower(*diag) == 'u')
    d = UNIT;
  else
    d = DEFINED;

  if (tolower(*side) == 'l') {
    if (tolower(*uplo) == 'u') {
      if (tolower(*transa) == 'n')
	trsm_LUN(d, M, N, A, ldA, B, ldB);
      else
	trsm_LUT(d, M, N, A, ldA, B, ldB);
    }
    else {
      if (tolower(*transa) == 'n')
	trsm_LLN(d, M, N, A, ldA, B, ldB);
      else
	trsm_LLT(d, M, N, A, ldA, B, ldB);
    }
  }
  else { /* Right */
    if (tolower(*uplo) == 'u') {
      if (tolower(*transa) == 'n')
	trsm_RUN(d, M, N, A, ldA, B, ldB);
      else
	trsm_RUT(d, M, N, A, ldA, B, ldB);
    }
    else {
      if (tolower(*transa) == 'n')
	trsm_RLN(d, M, N, A, ldA, B, ldB);
      else
	trsm_RLT(d, M, N, A, ldA, B, ldB);
    }
  }

}

void
DN_M(dn_trsm_f)(const char *side, const char *uplo,
		const char *transa, const char *diag,
		const int *M,
		const int *N,
		const struct dn *alpha,
		const struct dn *A, const int *ldA,
		struct dn *B, const int *ldB)
{
  DN_M(dn_trsm)(side, uplo, transa, diag, *M, *N, *alpha, A, *ldA,
		B, *ldB);
}
