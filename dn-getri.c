/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include <string.h>

#include <assert.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

void
DN_M(dn_getri) (const int n,
		struct dn * restrict LU, const int ldLU,
		const int * restrict ipiv,
		int *info)
{
    int i, j;
    struct dn *Ainv = NULL;
    const int ldAinv = n;

    *info = 0; /* XXX: should check args. */

    Ainv = calloc (n * n, sizeof(*Ainv));
    if (!Ainv) { *info = -1; return; }

    for (i = 0; i < n; ++i)
	AREF(Ainv, i, i) = dn_one();

    /* Invert U by solving U Ainv = I*/
    dn_trsm("Left", "Upper", "No trans", "Non-unit", n, n,
	    dn_one(), LU, ldLU, Ainv, n);

    /* Solve Ainv L = inv(U) */
    dn_trsm("Right", "Lower", "No trans", "Unit", n, n,
	    dn_one(), LU, ldLU, Ainv, n);

    /* Permute columns */
    for (j = n-2; j >= 0; --j) {
	const int jp = ipiv[j];
	if (jp != j) {
	    for (i = 0; i < n; ++i) {
		const struct dn tmp = AREF(Ainv,i,j);
		AREF(Ainv,i,j) = AREF(Ainv,i,jp);
		AREF(Ainv,i,jp) = tmp;
	    }
	}
    }

    /* Copy back */
    for (j = 0; j < n; ++j)
	memcpy (&AREF(LU,0,j), &AREF(Ainv,0,j), n*sizeof(*Ainv));

    free (Ainv);
}

void
DN_M(dn_getri_f) (const int *n,
		  struct dn * restrict LU, const int *ldLU,
		  const int * restrict ipiv,
		  int *info)
{
    dn_getri(*n, LU, *ldLU, ipiv, info);
}
