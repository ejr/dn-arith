/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
typedef long double dn_native_t;
#define DN_NATIVE_T long double
#define DN_NATIVE_IS_NOT_DOUBLE

struct dn {
  dn_native_t x[2];
};

#if defined(FP_FAST_FMAL)
#define DN_N_FMA_ fmal
#else
#define DN_SPLITTER_ (4294967297.0L)
/* = 2**32 + 1 */
#define DN_SPLIT_THRESH_ (2.77005949839e4922L)
/* \approx 2**(2**14-32) */
#define DN_SPLIT_SCALE_ (4294967296.0L)
/* = 2**32 */
#define DN_SPLIT_INV_SCALE_ (2.32830643653870e-10L)
/* = 2**-32 */
#endif

#define DN_N_ABS_ fabsl
#define DN_N_FLOOR_ floorl
#define DN_N_CEIL_ ceill
#define DN_N_SQRT_ sqrtl
#define DN_N_REM_ remainderl
#define DN_N_ISNAN_ isnan
#define DN_N_ISINF_ isinf
#define DN_N_ISFINITE_ isfinite
#define DN_N_COPYSIGN_ copysignl
#define DN_N_EPS_ LDBL_EPSILON
#define DN_N_OV_ LDBL_MAX
#define DN_N_UN_ LDBL_MIN
