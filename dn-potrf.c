/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <stdio.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"
#include "dn-blas.h"

#include "dn-helpers.h"

void
DN_M(dn_potf2)(const char *uplo, const int N,
	       struct dn * restrict A, const int ldA,
	       int *info)
{
  _Bool upper = tolower(*uplo) == 'u';
  int j = -1;

  *info = 0;

  if (!upper && tolower(*uplo) != 'l') {
    *info = -1;
    return; /* XXX: XERBLA */
  }

  if (N < 0) {
    *info = -2;
    return;
  }

  if (0 == N) return;

  if (ldA < N) {
    *info = -4;
    return;
  }

  if (!dn_ispos(*A)) goto not_posdef;

  if (upper) {
    for (j = 0; j < N; ++j) {
      int k;
      struct dn tmp = dn_sumsqr_(j, &AREF(A,0,j));
      struct dn Ajj = AREF(A,j,j);
      Ajj = dn_sub(Ajj, tmp);
      if (!dn_ispos(Ajj)) {
	AREF(A,j,j) = Ajj;
	goto not_posdef;
      }
      Ajj = dn_sqrt(Ajj);
      AREF(A,j,j) = Ajj;
      if (j < N-1) {
	dn_gemv("Transpose", j, N-j-1,
		dn_negone(), &AREF(A,0,j+1), ldA, &AREF(A,0,j), 1,
		dn_one(), &AREF(A,j,j+1), ldA);
	Ajj = dn_inv(Ajj);
	for (k = j+1; k < N; k++)
	  AREF(A,j,k) = dn_mul(AREF(A,j,k), Ajj);
      }
    }
  }
  else { /* lower */
    for (j = 0; j < N; ++j) {
      struct dn tmp = dn_sumsqr_strided_(j, &AREF(A,j,0), ldA);
      struct dn Ajj = AREF(A,j,j);
      Ajj = dn_sub(Ajj, tmp);
      if (!dn_ispos(Ajj)) {
	AREF(A,j,j) = Ajj;
	goto not_posdef;
      }
      Ajj = dn_sqrt(Ajj);
      AREF(A,j,j) = Ajj;
      if (j < N-1) {
	int k;
	dn_gemv("No transpose", N-j-1, j,
		dn_negone(), &AREF(A,j+1,0), ldA, &AREF(A,j,0), ldA,
		dn_one(), &AREF(A,j+1,j), 1);
	Ajj = dn_inv(Ajj);
	for (k = j+1; k < N; k++)
	  AREF(A,k,j) = dn_mul(AREF(A,k,j), Ajj);
      }
    }
  }
  return;

 not_posdef:
  *info = j;
  return;
}

void
DN_M(dn_potrf)(const char *uplo, const int N,
	       struct dn * restrict A, const int ldA,
	       int *info)
{
  DN_M(dn_potf2)(uplo, N, A, ldA, info);
}

void
DN_M(dn_potf2_f)(const char *uplo, const int *N,
		 struct dn *A, const int *ldA,
		 int *info)
{
  DN_M(dn_potf2)(uplo, *N, A, *ldA, info);
}

void
DN_M(dn_potrf_f)(const char *uplo, const int *N,
		 struct dn *A, const int *ldA,
		 int *info)
{
  DN_M(dn_potrf)(uplo, *N, A, *ldA, info);
}
