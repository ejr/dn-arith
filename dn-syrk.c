/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

/*
  uplo == U
    trans = N : C := alpha * A * A' + beta * C
    trans = T|C : C := alpha * A' * A + beta * C
  uplo == L
    trans = N : C := alpha * A * A' + beta * C
    trans = T|C : C := alpha * A' * A + beta * C
*/

static void
syrk_UN (const int N, const int K,
	 const struct dn alpha,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict C, const int ldC)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i <= j; ++i) {
      struct dn tmp;
      tmp = dn_sumprod_strided_(K, &AREF(A,i,0), ldA, &AREF(A,j,0), ldA);
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(C,i,j) = dn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
syrk_UT (const int N, const int K,
	 const struct dn alpha,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict C, const int ldC)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = 0; i <= j; ++i) {
      struct dn tmp;
      tmp = dn_sumprod_(K, &AREF(A,0,i), &AREF(A,0,j));
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(C,i,j) = dn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
syrk_LN (const int N, const int K,
	 const struct dn alpha,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict C, const int ldC)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = j; i < N; ++i) {
      struct dn tmp;
      tmp = dn_sumprod_strided_(K, &AREF(A,i,0), ldA, &AREF(A,j,0), ldA);
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(C,i,j) = dn_add(AREF(C,i,j), tmp);
    }
  }
}


static void
syrk_LT (const int N, const int K,
	 const struct dn alpha,
	 const struct dn * restrict A, const int ldA,
	 struct dn * restrict C, const int ldC)
{
  const _Bool one_alpha = dn_isone(alpha);
  const _Bool negone_alpha = dn_isnegone(alpha);

#pragma omp parallel for
  for (int j = 0; j < N; ++j) {
    for (int i = j; i < N; ++i) {
      struct dn tmp;
      tmp = dn_sumprod_(K, &AREF(A,0,i), &AREF(A,0,j));
      if (negone_alpha)
	tmp = dn_negate(tmp);
      else if (!one_alpha)
	tmp = dn_mul(tmp, alpha);
      AREF(C,i,j) = dn_add(AREF(C,i,j), tmp);
    }
  }
}


void
DN_M(dn_syrk)(const char *uplo, const char *trans, const int N,
	      const int K,
	      const struct dn alpha,
	      const struct dn * restrict A, const int ldA,
	      const struct dn beta,
	      struct dn * restrict C, const int ldC)
{
#pragma omp parallel
  if (!dn_isone(beta)) {
    if (dn_iszero(beta)) {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = dn_zero();
    } else if (dn_isnegone(beta)) {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = dn_negate(AREF(C,i,j));
    } else {
#pragma omp for
      for (int j = 0; j < N; ++j)
	for (int i = 0; i < N; ++i)
	  AREF(C,i,j) = dn_mul(AREF(C,i,j), beta);
    }
  }

  if (dn_iszero(alpha)) return;

  if (tolower(*uplo) == 'u') {
    if (tolower(*trans) == 'n')
      syrk_UN(N, K, alpha, A, ldA, C, ldC);
    else
      syrk_UT(N, K, alpha, A, ldA, C, ldC);
  }
  else {
    if (tolower(*trans) == 'n')
      syrk_LN(N, K, alpha, A, ldA, C, ldC);
    else
      syrk_LT(N, K, alpha, A, ldA, C, ldC);
  }
}

void
DN_M(dn_syrk_f)(const char *uplo, const char *trans, const int *N,
		const int *K,
		const struct dn *alpha,
		const struct dn *A, const int *ldA,
		const struct dn *beta,
		struct dn *C, const int *ldC)
{
  DN_M(dn_syrk)(uplo, trans, *N, *K, *alpha, A, *ldA, *beta, C, *ldC);
}
