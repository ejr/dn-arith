/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#define DN_EXT_DEFS
#include "zn-arith.h"
#include "dn-ops-internal.h"

static const dn_native_t ovthresh = DN_N_OV_/16.0;
static const dn_native_t unthresh = (DN_N_UN_*2.0)/DN_N_EPS_;
static const dn_native_t scale_up_fact = 2.0/(DN_N_EPS_*DN_N_EPS_);

struct zn
zn_div(struct zn a, struct zn b)
{
  const dn_native_t amag = zn_abs1_n_(a);
  const dn_native_t bmag = zn_abs1_n_(a);
  dn_native_t S = 1.0;
  struct zn out;

  if (amag > ovthresh) {
    a = zn_div_n(a, 16.0);
    S *= 16.0;
  } else if (amag < unthresh) {
    a = zn_mul_n(a, scale_up_fact);
    S /= scale_up_fact;
  }
  if (bmag > ovthresh) {
    b = zn_div_n(b, 16.0);
    S /= 16.0;
  } else if (bmag < unthresh) {
    b = zn_mul_n(b, scale_up_fact);
    S *= scale_up_fact;
  }

  if (dn_gt(dn_abs(b.re),dn_abs(b.im))) {
    const struct dn r = dn_div(b.im, b.re);
    const struct dn t = dn_inv(dn_macc(r,b.im, b.re));
    out = zn_mul_dn(zn_make(dn_macc(r,a.im, a.re),
			    dn_nmacc(r,a.re, a.im)),
		    t);
  } else {
    const struct dn r = dn_div(b.re, b.im);
    const struct dn t = dn_inv(dn_macc(r,b.re, b.im));
    out = zn_mul_dn(zn_make(dn_macc(r,a.re, a.im),
			    dn_macc(r,a.im, dn_negate(a.re))),
		    t);
  }

  return zn_mul_2(out, S);
}

struct zn
zn_div_n_n(struct zn a, dn_native_t breal, dn_native_t bimag)
{
  const dn_native_t amag = zn_abs1_n_(a);
  const dn_native_t bmag = DN_N_ABS_(breal) + DN_N_ABS_(bimag);
  dn_native_t S = 1.0;
  struct zn out;

  if (amag > ovthresh) {
    a = zn_div_n(a, 16.0);
    S *= 16.0;
  } else if (amag < unthresh) {
    a = zn_mul_n(a, scale_up_fact);
    S /= scale_up_fact;
  }
  if (bmag > ovthresh) {
    breal /= 16.0;
    bimag /= 16.0;
    S /= 16.0;
  } else if (bmag < unthresh) {
    breal *= scale_up_fact;
    bimag *= scale_up_fact;
    S *= scale_up_fact;
  }

  if (DN_N_ABS_(breal) > DN_N_ABS_(bimag)) {
    const struct dn r = dn_div_n_n(bimag, breal);
    const struct dn t = dn_inv(dn_add_n(dn_mul_n(r, bimag), breal));
    out = zn_mul_dn(zn_make(dn_macc(r,a.im, a.re),
			    dn_nmacc(r,a.re, a.im)),
		    t);
  } else {
    const struct dn r = dn_div_n_n(breal, bimag);
    const struct dn t = dn_inv(dn_add_n(dn_mul_n(r, breal), bimag));
    out = zn_mul_dn(zn_make(dn_macc(r,a.re, a.im),
			    dn_macc(r,a.im, dn_negate(a.re))),
		    t);
  }

  return zn_mul_2(out, S);
}

struct zn
zn_sum_ (const size_t n, const struct zn *x)
{
    struct dn sum_re, sum_im;
    sum_re = dn_sum_strided_np_ (n, &x[0].re.x[0], 4);
    sum_im = dn_sum_strided_np_ (n, &x[0].im.x[0], 4);
    return zn_make(sum_re, sum_im);
}

struct dn
zn_asum_ (const size_t n, const struct zn *x)
{
  struct dn asum = dn_zero();
  size_t i;
  for (i = 0; i < n; ++i)
    asum = dn_running_sum_wrapped_ (asum, zn_abs (x[i]));
  return dn_renormalize_ (asum);
}

struct zn
zn_sum_strided_ (const size_t n, const struct zn *x, const size_t stride)
{
    struct dn sum_re, sum_im;
    sum_re = dn_sum_strided_np_ (n, &x[0].re.x[0], 4*stride);
    sum_im = dn_sum_strided_np_ (n, &x[0].im.x[0], 4*stride);
    return zn_make(sum_re, sum_im);
}

struct dn
zn_asum_strided_ (const size_t n, const struct zn *x, const size_t stride)
{
  struct dn asum = dn_zero();
  size_t i;
  for (i = 0; i < n; ++i)
    asum = dn_running_sum_wrapped_ (asum, zn_abs (x[i*stride]));
  return dn_renormalize_ (asum);
}

struct zn
zn_sumprod_ (const size_t n, const struct zn *x, const struct zn *y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].re, 2);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].im, 2);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].im, 2);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].re, 2);
    return zn_make(dn_sub(sum_rere, sum_imim),
		   dn_add(sum_reim, sum_imre));
}

struct dn
zn_asumprod_ (const size_t n, const struct zn *x, const struct zn *y)
{
  struct dn asum = dn_zero ();
  size_t i;
  for (i = 0; i < n; ++i)
    asum = dn_running_sum_wrapped_ (asum, dn_mul (zn_abs (x[i]), zn_abs (y[i])));
  return dn_renormalize_ (asum);
}

struct zn
zn_sumprod_nc_ (const size_t n, const struct zn *x, const struct zn *y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].re, 2);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].im, 2);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].im, 2);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].re, 2);
    return zn_make(dn_add(sum_rere, sum_imim),
		   dn_sub(sum_imre, sum_reim));
}

struct zn
zn_sumprod_cc_ (const size_t n, const struct zn *x, const struct zn *y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].re, 2);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].im, 2);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2, &y[0].im, 2);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2, &y[0].re, 2);
    return zn_make(dn_sub(sum_rere, sum_imim),
		   dn_negate(dn_add(sum_reim, sum_imre)));
}

struct zn
zn_sumprod_strided_ (const size_t n,
		     const struct zn *x, const size_t stride_x,
		     const struct zn *y, const size_t stride_y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].re, 2*stride_y);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].re, 2*stride_y);
    return zn_make(dn_add(sum_rere, dn_negate(sum_imim)),
		   dn_add(sum_reim, sum_imre));
}

struct dn
zn_asumprod_strided_ (const size_t n,
		      const struct zn *x, const size_t xstride,
		      const struct zn *y, const size_t ystride)
{
  struct dn asum = dn_zero ();
  size_t i;
  for (i = 0; i < n; ++i)
    asum = dn_running_sum_wrapped_ (asum, dn_mul (zn_abs (x[i*xstride]),
						  zn_abs (y[i*ystride])));
  return dn_renormalize_ (asum);
}

struct zn
zn_sumprod_strided_nc_ (const size_t n,
			const struct zn *x, const size_t stride_x,
			const struct zn *y, const size_t stride_y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].re, 2*stride_y);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].re, 2*stride_y);
    return zn_make(dn_add(sum_rere, sum_imim),
		   dn_sub(sum_imre, sum_reim));
}

struct zn
zn_sumprod_strided_cc_ (const size_t n,
			const struct zn *x, const size_t stride_x,
			const struct zn *y, const size_t stride_y)
{
    struct dn sum_rere, sum_imre, sum_reim, sum_imim;
    sum_rere = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].re, 2*stride_y);
    sum_imim = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_reim = dn_sumprod_strided_ (n, &x[0].re, 2*stride_x,
				    &y[0].im, 2*stride_y);
    sum_imre = dn_sumprod_strided_ (n, &x[0].im, 2*stride_x,
				    &y[0].re, 2*stride_y);
    return zn_make(dn_sub(sum_rere, sum_imim),
		   dn_negate(dn_add(sum_reim, sum_imre)));
}
