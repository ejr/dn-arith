/* -*- C -*- Copyright (c) 2007 The Regents of the University of California.
   All rights reserved.  See COPYING for license.
*/
#include <ctype.h>
#include <math.h>

#include <stdio.h>

#define DN_RESTRICT restrict
#include "dn-arith.h"

#include "dn-helpers.h"

void
DN_M(dn_gemv)(const char *trans, const int M, const int N,
	      const struct dn alpha,
	      const struct dn * restrict A, const int ldA,
	      const struct dn * restrict X, const int incX,
	      const struct dn beta,
	      struct dn * restrict Y, const int incY)
{
  const _Bool do_trans = tolower(*trans) != 'n';
  const int lenY = (do_trans? M : N);

#pragma omp parallel
  {
    if (dn_iszero(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = dn_zero();
    else if (dn_isnegone(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = dn_negate(VREF(Y,j));
    else if (!dn_isone(beta))
#pragma omp for
      for (int j = 0; j < lenY; ++j)
	VREF(Y,j) = dn_mul(beta, VREF(Y,j));

    if (!do_trans) {
#pragma omp for nowait
      for (int i = 0; i < M; ++i) {
	struct dn tmp = dn_sumprod_strided_(N, &AREF(A,i,0), ldA, X, incX);
	VREF(Y,i) = dn_macc(alpha, tmp, VREF(Y,i));
      }
    }
    else {
#pragma omp for nowait
      for (int j = 0; j < N; ++j) {
	struct dn tmp;
	if (incX == 1)
	  tmp = dn_sumprod_(M, &AREF(A,0,j), X);
	else
	  tmp = dn_sumprod_strided_(M, &AREF(A,0,j), 1, X, incX);
	VREF(Y,j) = dn_macc(alpha, tmp, VREF(Y,j));
      }
    }
  }
}

void
DN_M(dn_gemv_f)(const char *trans, const int *M, const int *N,
		const struct dn *alpha,
		const struct dn *A, const int *ldA,
		const struct dn *X, const int *incX,
		const struct dn *beta,
		struct dn *Y, const int *incY)
{
  DN_M(dn_gemv)(trans, *M, *N, *alpha, A, *ldA, X, *incX,
		*beta, Y, *incY);
}
